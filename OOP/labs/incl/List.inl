/**
 * @file include/List.inl
 * @memberof manual containers collection
 * @category list
 * @namespace containers
 *
 * @brief Circled List, which can store elements of any type (temple class).
 * @todo: faster methods, reserving memory.
 *
 * @author d_pch
 * @version 1.5
 * @date 09-Oct-16 14:48
 */

#ifndef CONTAINERS_LIST_INL
#define CONTAINERS_LIST_INL

#include "BaseObject.inl"
#include <iosfwd>

namespace containers {

    template<class T>
    class List : public basic::BaseObject {
        template<class T1>
        friend std::ostream& operator>>(std::ostream& os, const List<T1>& list); //send List to output
        template<class T1>
        friend std::ostream& operator<<(std::ostream& os, const List<T1>& list); //fill List from input
    public:
        List(void); //default constructor
        List(const unsigned int size,
             T objects[]); //initialize list with elements from array (+err:err)
        virtual ~List(void); //destructor (free allocated memory)
        List(const List& list); //coping constructor (+err:err)

        unsigned int size(void) const; //return number of elements
        bool empty(void) const; //return true if list is empty
        //@todo: unsigned int reserved_memory(void) const; //show amount of reserved free memory

        virtual bool append(const T& object); //add element to the end of list
        virtual int insert(int position,
                           const T& object); //add element in posinion in list
        virtual int remove(int position); //delete element from list
        virtual bool remove_all_same(const T& object); //delete all elements == object from list
        virtual void clear(void); //remove all elements from list (call destructor)
        virtual void sort(bool (* compare)(const T& object1, const T& object2)); //(+Ex:err)
        virtual int find(const T& object, bool (* compare)(const T& object1, const T& object2)); //(+Ex:err)
        //@todo: bool compress(void); //delete all duplicates and free elements from List
        //@todo: bool free_memory(void); //delete elements without data from List
        //@todo: int reserve(const unsigned int amount); //reserve needed amount of memory in list

        virtual bool operator+(const T& object); //add element to the end of list
        virtual bool operator-(const T& object); //delete all elements == object from list

        virtual List& operator=(const List& list); //(+Ex:err)/(+Ex:n)
        virtual List& operator+(const List& list); //add elements of list to this::list (+Ex:err)

        void spoil_everything() const {

        }

        virtual bool operator==(const List& list) const;
        virtual bool operator!=(const List& list) const;
        virtual bool operator>(const List& list) const;
        virtual bool operator>=(const List& list) const;
        virtual bool operator<(const List& list) const;
        virtual bool operator<=(const List& list) const;

        virtual T &operator[](int position) const; //return element of list (+Ex:err)
        virtual bool operator()(void (*do_stuff)(T* element)); //do smth with each element of list
    protected:
        //Structure is private as it contains links to other structures and data
        //Wrong usage may cause unexpected class behaviour
        //@WRAN: coping constructor of data should exist
        typedef struct _Element {
        private:
            T *p_data; //pointer to stored data
        public:
            _Element *next;
            _Element *previous;

            _Element(void) {
                p_data = nullptr;
                previous = next = nullptr;
            }

            _Element(const T &object) {
                p_data = new T(object); //copy value of object to element
                /* try to get memory */
                for (int i = 0; i < 10 and !p_data; i++) {
                    p_data = new T(object);
                }
                /* generate exception in case of failure */
                if (!p_data) {
                    throw NALLOC;
                }
            } //initialization of Element (+Ex:err)
            ~_Element(void) {
                if (p_data) {
                    delete p_data;
                    p_data = nullptr;
                }
                next = previous = nullptr;
            } //destructor
            _Element(const _Element &element) {
                if (p_data) {
                    delete this; //delete old data if exists
                }

                /* copy links */
                next = element.next;
                previous = element.previous;

                try {
                    this = new _Element(element.p_data); //create new copy of data
                } catch (char err) {
                    throw err; //regenerate exception
                }
            } //coping constructor (+Ex:err)

            bool set_data(const T &object) {
                if (p_data) {
                    delete p_data; //free allocated memory
                }
                try {
                    p_data = new T(object); //copy value of object to element
                } catch (int err) {
                    throw err;
                }

                return p_data != nullptr; //check memory was allocated
            } //(+Ex:err)
            T get_data(void) const {
                if (!p_data) {
                    throw PTRNLL;
                }
                return (p_data) ? *p_data : *(new T); //if data exist
            } //return a copy of object if exists (+Ex:err)
            inline T *get_access_to_data(void) const {
                return p_data;
            } //return a pointer to data (data can be modified)
            void remove_data(void) {
                if (p_data) {
                    delete p_data;
                    p_data = nullptr;
                }
            }

            void swap_data(T *external_data) {
                T *temp = p_data;
                p_data = external_data;
                external_data = p_data;
            }

            const T &operator=(const T &object) {
                if (p_data) {
                    delete p_data; //free allocated memory
                }
                p_data = new T(object); //copy value of object to element
                /* generate exception in case of failure */
                if (!p_data) {
                    throw NALLOC;
                }

                return (p_data) ? *p_data : *(new T);
            } //(+Ex:err)
            const T *operator=(T *p_object) {
                p_data = p_object;
            }

            const _Element &operator=(const _Element &element) {
                if (this != &element) {
                    if (this->p_data) {
                        delete this->p_data; //delete stored data
                    }
                    this->p_data = new T(element.p_data); //save new data
                    this->next = element.next;
                    this->previous = element.previous;
                }

                return this;
            }

            inline bool operator==(const T &object) {
                return *p_data == object;
            } //Compare data in structure with object of same type
            inline bool operator==(const _Element &element) {
                return *(this->p_data) == *(element.p_data);
            } //Compare data in structures
            inline bool operator!=(const _Element &element) {
                return !(*(this->p_data) == *(element.p_data));
            }

            inline bool operator<(const _Element &element) {
                return *(element.p_data) > *(this->p_data);
            }

            inline bool operator>(const _Element &element) {
                return *(this->p_data) > *(element.p_data);
            }

            inline bool operator<=(const _Element &element) {
                return !(*(this->p_data) > *(element.p_data));
            }

            inline bool operator>=(const _Element &element) {
                return !(*(element.p_data) > *(this->p_data));
            }
        } Element;

        Element* m_start; //pointer to one half on list
        mutable unsigned int m_size;
        //@todo: Element* m_middle; //pointer to another half of list
        //@todo: unsigned int m_free_space; //amount of free space in list

        virtual int set_position(int& position,
                                 unsigned int& size,
                                 Element* pointer) const;
        void add(Element* pointer,
                 Element* new_el); //add element next to pointer
    };

    template<class T>
    List<T>::List(void) {
        /* List is empty */
        m_size = 0;
        m_start = nullptr;
    } //@tested

    template<class T>
    List<T>::List(const unsigned int size, T *objects): List() {
        for (int i = 0; i < size; ++i) {
            //@fixme: strong-linked code: operator+ should exists and works properly!
            if (!(*this + objects[i])) {
                set_error(NALLOC);
            }
        }
    } //@tested

    template<class T>
    List<T>::~List(void) {
        for (; m_size; --m_size) {
            _Element *tmp_ptr = m_start;
            m_start = m_start->next; //shift start
            delete tmp_ptr; //delete structure
        }

        m_start = nullptr;
    } //@tested

    template<class T>
    List<T>::List(const List &list): List() {
        Element *tmp_ptr = list.m_start;

        for (int i = 0; i < list.m_size; ++i) {
            if (!(*this + tmp_ptr->get_data())) {
                set_error(NALLOC);
            }

            if (tmp_ptr) {
                tmp_ptr = tmp_ptr->next;
            } else {
                set_error(PTRNLL);
            }
        }
    } //@tested

    template<class T>
    inline unsigned int List<T>::size(void) const {
        return this->m_size;
    } //@tested

    template<class T>
    inline bool List<T>::empty(void) const {
        return this->m_start == nullptr && m_size == 0;
    } //@tested

    template<class T>
    inline bool List<T>::append(const T &object) {
        return *this + object;
    } //@tested

    template<class T>
    int List<T>::insert(int position, const T &object) {
        Element *new_el = new Element(object);
        if (!new_el) {
            return NALLOC1;
        }
        if (position == 0 || position + m_size == 0) {
            add(m_start, new_el);
            m_start = m_start->previous;
        } else {
            Element *tmp_ptr = m_start;
            if (auto err = set_position(position, m_size, tmp_ptr)) {
                return err;
            }

            add(tmp_ptr, new_el);
        }

        return EXIT_SUCCESS;
    } //@tested

    template<class T>
    int List<T>::remove(int position) {
        if (m_start == nullptr) {
            return EMPVAL; //list is empty
        }

        Element *tmp_ptr = m_start;
        if (position < 0) {
            position += m_size; //reverse position
        }
        if (position >= m_size) {
            return INVPOS;
        }

        /* search for position */
        if (position <= (m_size - 1) / 2) {
            for (tmp_ptr = m_start; position--; tmp_ptr = tmp_ptr->next); //search upwards, 'cause position is positive (and valid)
        } else {
            for (tmp_ptr = m_start->previous; position++ < m_size - 1;)
                tmp_ptr = tmp_ptr->previous; //search backwards, 'cause position is negative (and valid)
        }

        if (m_size == 1) {
            delete m_start; //delete structure
            m_start = nullptr;
        } else {
            tmp_ptr->previous->next = tmp_ptr->next;
            tmp_ptr->next->previous = tmp_ptr->previous;
            delete tmp_ptr;
        }

        m_size--;
        return EXIT_SUCCESS;
    } //@tested

    template<class T>
    inline bool List<T>::remove_all_same(const T &object) {
        return *this - object;
    } //@tested

    template<class T>
    void List<T>::clear(void) {
        delete this;
    } //@tested

    template<class T>
    void List<T>::sort(bool (*compare)(const T &object1, const T &object2)) {
        for (int i = 1; i < m_size; ++i) {
            Element *tmp_ptr = m_start; //to store Element temporary

            for (int j = 0; j < i - 1; ++j, tmp_ptr = tmp_ptr->next);
            T *tmp_p_data = tmp_ptr->next->get_access_to_data();

            try {
                for (int j = i; j > 0 && compare(tmp_ptr->get_data(), *tmp_p_data); --j) {
                    *tmp_ptr->next = tmp_ptr->get_access_to_data();
                    *tmp_ptr = tmp_p_data;
                    tmp_ptr = tmp_ptr->previous;
                }
            } catch (int err) {
                throw err; //regenerate error
            }
        }
    } //tested

    template <class T>
    int List<T>::find(const T& object, bool (* compare)(const T& object1, const T& object2)) {
        Element *tmp_ptr = m_start; //to store Element temporary
        for (int i = 0; i < m_size; ++i) {
            try {
                if (compare(object, tmp_ptr->get_data())) {
                    return i;
                }
            } catch (int err) {
                throw err; //regenerate error
            }
            tmp_ptr = tmp_ptr->next;
        }

        return NFOUND;
    } //@tested

    template<class T>
    bool List<T>::operator+(const T &object) {
        Element *new_el = new Element(object);
        if (!new_el || !(new_el->get_access_to_data())) {
            return false; //can't allocate memory
        }

        add(m_start, new_el);

        return true;
    } //@tested

    template<class T>
    bool List<T>::operator-(const T &object) {
        if (m_start == nullptr) {
            return false; //list are empty
        }

        Element *current_el = m_start; //temp pointer - iterator
        int limit = m_size; //amount of elements in original list

        for (int i = 0; i < limit; ++i) {
            if (*(current_el->next) == object) {
                if (current_el->next == current_el) /*only one element in list*/ {
                    delete current_el; //delete structure
                    m_start = current_el = nullptr;
                } else {
                    current_el->next = current_el->next->next; //shift pointer to next
                    delete current_el->next->previous; //delete structure
                    current_el->next->previous = current_el; //shift pointer to previous
                }
                m_size--; //decrease size of list
            }
            if (current_el) /* to avoid NULLptr */ {
                current_el = current_el->next; //go to the next element
            }
        }

        return limit != m_size; //check whether removal
    } //@tested

    template<class T>
    List<T> &List<T>::operator=(const List &list) {
        if (this != &list) {
            delete this;

            try {
                Element *tmp_ptr = list.m_start;
                for (int i = 0; i < list.m_size; ++i) {
                    if (!(*this + tmp_ptr->get_data())) {
                        throw i; //number of added elements
                    }

                    if (tmp_ptr) {
                        tmp_ptr = tmp_ptr->next;
                    } else {
                        throw PTRNLL;
                    }
                }
            } catch (int err) {
                throw err; //regenerate exception
            }
        }
        return *this;
    } //@tested

    template<class T>
    List<T> &List<T>::operator+(const List &list) {
        Element *tmp_ptr = list.m_start;
        for (int i = 0; i < list.m_size; ++i) {
            if (!(*this + tmp_ptr->get_data())) {
                throw NALLOC;
            }
            tmp_ptr = tmp_ptr->next;
        }
        return *this;
    } //@tested

    template<class T>
    bool List<T>::operator==(const List& list) const {
        if (list.m_size != this->m_size) {
            return false;
        }

        Element* tmp_ptr = this->m_start;
        Element* tmp_lptr = list.m_start;

        for (int i = 0; i < this->m_size; ++i, tmp_ptr = tmp_ptr->next, tmp_lptr = tmp_lptr->next) {
            if (!(*(tmp_ptr) == *(tmp_lptr))) {
                return false;
            }
        }

        return true;
    } //@tested

    template<class T>
    bool List<T>::operator!=(const List& list) const {
        if (list.m_size != this->m_size) {
            return true;
        }

        Element* tmp_ptr = this->m_start;
        Element* tmp_lptr = list.m_start;

        for (int i = 0; i < this->m_size; ++i, tmp_ptr = tmp_ptr->next, tmp_lptr = tmp_lptr->next) {
            if (!(*(tmp_ptr) == *(tmp_lptr))) {
                return true;
            }
        }

        return false;
    } //@tested

    template<class T>
    bool List<T>::operator>(const List& list) const {
        if (this->m_size > list.m_size) {
            return true;
        }

        Element* tmp_ptr = this->m_start;
        Element* tmp_lptr = list.m_start;

        for (int i = 0; i < this->m_size; ++i, tmp_ptr = tmp_ptr->next, tmp_lptr = tmp_lptr->next) {
            if (*(tmp_ptr) > *(tmp_lptr)) {
                return true;
            }
        }

        return false;
    } //@tested

    template<class T>
    bool List<T>::operator>=(const List& list) const {
        if (list.m_size > this->m_size) {
            return false;
        }

        Element* tmp_ptr = this->m_start;
        Element* tmp_lptr = list.m_start;

        for (int i = 0; i < this->m_size; ++i, tmp_ptr = tmp_ptr->next, tmp_lptr = tmp_lptr->next) {
            if (*(tmp_lptr) > *(tmp_ptr)) {
                return false;
            }
        }

        return true;
    } //@tested

    template<class T>
    bool List<T>::operator<(const List& list) const {
        if (list.m_size > this->m_size) {
            return true;
        }

        Element* tmp_ptr = this->m_start;
        Element* tmp_lptr = list.m_start;

        for (int i = 0; i < this->m_size; ++i, tmp_ptr = tmp_ptr->next, tmp_lptr = tmp_lptr->next) {
            if (*(tmp_lptr) > *(tmp_ptr)) {
                return true;
            }
        }

        return false;
    } //@tested

    template<class T>
    bool List<T>::operator<=(const List& list) const {
        if (this->m_size > list.m_size) {
            return false;
        }

        Element* tmp_ptr = this->m_start;
        Element* tmp_lptr = list.m_start;

        for (int i = 0; i < this->m_size; ++i, tmp_ptr = tmp_ptr->next, tmp_lptr = tmp_lptr->next) {
            if (*(tmp_ptr) > *(tmp_lptr)) {
                return false;
            }
        }

        return true;
    } //@tested

    template<class T>
    T& List<T>::operator[](int position) const {
        if (!m_start) {
            throw PTRNLL;
            return *(new T);
        }

        Element *tmp_ptr;
        if (position < 0) {
            position += m_size; //reverse position
        }
        if (position >= m_size) {
            throw INVPOS;
            return *(new T);
        }

        /* search for position */
        if (position <= (m_size - 1) / 2) {
            for (tmp_ptr = m_start; position--; tmp_ptr = tmp_ptr->next); //search upwards, 'cause position is positive (and valid)
        } else {
            for (tmp_ptr = m_start->previous; position++ < m_size - 1;)
                tmp_ptr = tmp_ptr->previous; //search backwards, 'cause position is negative (and valid)
        }

        return *(tmp_ptr->get_access_to_data());
    } //@tested

    template<class T>
    bool List<T>::operator()(void (*do_stuff)(T *element)) {
        if (!m_start) {
            return false;
        }

        Element *tmp_ptr = m_start;

        for (int i = 0; i < m_size; ++i, tmp_ptr = tmp_ptr->next) {
            if (tmp_ptr && tmp_ptr->get_access_to_data()) {
                do_stuff(tmp_ptr->get_access_to_data()); //change element somehow
            }
        }

        return true;
    } //@tested

    template <class T>
    int List<T>::set_position(int& position, unsigned int& size, Element* pointer) const {
        if (position < 0) {
            position += size; //reverse position
        }
        if (position >= size) {
            return INVPOS;
        }

        /* search for position */
        if (position <= (size - 1) / 2) {
            for (pointer = m_start; position--; pointer = pointer->next); //search upwards, 'cause position is positive (and valid)
        } else {
            for (pointer = m_start->previous; position++ < size - 1;)
                pointer = pointer->previous; //search backwards, 'cause position is negative (and valid)
        }

        return EXIT_SUCCESS;
    } //@tested

    template <class T>
    void List<T>::add(Element* pointer, Element* new_el) {
        if (m_start == nullptr) {
            /* add first element */
            m_start = new_el; //add first element
            m_start->next = m_start->previous = m_start; //element links with itself
        } else {
            new_el->previous = pointer->previous;
            new_el->next = pointer; //list is cycled
            pointer->previous = new_el;
            new_el->previous->next = new_el;
        }

        m_size++; //increase list size
    } //@tested
}

///* both functions should be declared in place of usage */
//template<class T>
//std::ostream& operator>>(std::ostream& os, const containers::List<T>& list) {
//    /* @undone - specific for each type */
//}

//template<class T>
//std::ostream& operator<<(std::ostream& os, const containers::List<T>& list) {
//    if (list.empty()) {
//        os << "List is empty!";
//    }
//    for (int i = 0; i < list.size(); ++i) {
//        os << i << " - " << list[i] << std::endl;
//    }
//    os << std::endl;

//    return os;
//} //@untested

#endif //CONTAINERS_LIST_INL
