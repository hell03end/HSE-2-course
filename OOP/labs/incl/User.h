/**
 * @file User.h
 * @memberof authication system
 * @category account
 * @namespace auth
 *
 * @brief Class of simple user.
 *
 * @details
 * Contains login(+id)/password, key to crypt password and methods to manipulate them.
 * To configure min/max valid lengths of login/password redefine "LENBRD".
 * To crypt password simple xor-method is used.
 * @todo: type cast operator.
 *
 * @author d_pch
 * @version 0.1
 * @date 07-Oct-16 21:42
 */

#ifndef USER_H
#define USER_H

#ifndef AUTOKEY
#define AUTOKEY "-a"
#endif //AUTOKEY
#ifndef DEFUSR //default data for User if they aren't set manually
#define DEFNAM "Noname"
#define DEFSNM "Nosurname"
#define DEFLOG "no_username" //default login
#define DEFPAS "dick123!!!" //default password
#endif //DEFUSR

#ifndef LENBRDR
#define MINNAM 2
#define MINLOG 3
#define MINPAS 7
#define MAXNAM 20
#define MAXLOG 20
#define MAXPAS 30
#endif //LENBRDR

#include "BaseObject.inl"
#include <string>
#include <cctype>

namespace auth {

    class User : public basic::BaseObject {
    public:
        User(void);
        User(const std::string& name,
             const std::string& surname,
             const std::string& password,
             const std::string& login = DEFLOG,
             const std::string& key = AUTOKEY);
        virtual ~User(void); //empty
        User(const User& user);
        // User(const Player& player); //converting constructor (type cast)
        // operator bool(?) const; //?

        virtual bool set_login(const std::string& login); //check login to be correct, then assign in to m_login
        const std::string& get_login(void) const;

        virtual bool set_name(const std::string& name); //check name to be correct, then assign in to m_name
        const std::string& get_name(void) const;

        virtual bool set_surname(const std::string& surname); //check surname to be correct, then assign in to m_surname
        const std::string& get_surname(void) const;

        bool set_password(const std::string& password,
                          const std::string& key = AUTOKEY);
        const std::string& get_password(const std::string& key = AUTOKEY) const; //get password if key is right (+Ex:err)
        void set_key(const std::string& key = AUTOKEY); //reset key and re-crypt password in correct way

        virtual User& operator=(const User& user);

        static bool correct_name(const std::string& name); //check (surname)name format is correct
        static bool correct_login(const std::string& login); //check login format is correct
        static bool correct_password(const std::string& password); //check password format is correct
    protected:
        std::string m_login;
        std::string m_name;
        std::string m_surname;
        std::string m_password;
        std::string m_key;

        static std::string crypt(const std::string& message,
                                 const std::string& key);
        static std::string decrypt(const std::string& message,
                                   const std::string& key);
//        static unsigned long int get_hash(const std::string& message); //return hash from string
        static std::string to_standard_login(const std::string& login); //get login suit standards
        static std::string to_standard_name(const std::string& name); //get (sur)name suit standards
    };

}

#endif //USER_H
