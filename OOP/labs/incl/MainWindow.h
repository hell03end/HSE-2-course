#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ErrMessages.h"
#include "List.inl"
#include "Player.h"
#include <QMessageBox>
#include <QMainWindow>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    enum GameMode {SNGL, CMND, PVP};

    explicit MainWindow(QWidget *parent = NULL);
    ~MainWindow(void);

    void set_offline_settings(void);
    void set_online_settings(const std::string& nickname, unsigned long score);
    void set_geometry(const QRect& size);
    void update_score(unsigned long score);
    void update_nickname(const std::string& nickname = "");
    void show_players(const containers::List<auth::Player>& list);

private:
    Ui::MainWindow* ui; //GUI for main window
    GameMode m_mode;

    void close_app(void);
    void show_message(int icon, const std::string& title,
                   const std::string& body = "", const std::string& detals = "");
    void set_sounds(void);
    void request_players(void);
    void rest_score(void);
    void call_about(void);
    void log_out(void);
    void delete_user(void);
    void change_nickname(void);
    void save_settings(void);

    void play(void);
    void go_to_main_window(void);
    void go_to_scores(void);
    void go_to_settings(void);

    void update_game_mode(void);
    void button_enable_save(void);
    void button_enable_scores(void);

signals:
    void KillApp(void);
    void Play(GameMode, bool);
    void Sound(bool);
    void RestScore(void);
    void DeleteUser(void);
    void LogOut(void);
    void ShowPlayers(GameMode);
    void ChangeNickname(const std::string&);
    void CallAbout(void); //show info window
    void Geometry(const QRect&); //return geometry of window
};

#endif // MAINWINDOW_H
