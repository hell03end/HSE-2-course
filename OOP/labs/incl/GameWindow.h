#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QDialog>

namespace Ui {
    class GameWindow;
}

class GameWindow : public QDialog {
    Q_OBJECT

public:
    explicit GameWindow(QWidget *parent = 0);
    ~GameWindow();

    void set_geometry(const QRect& size);

private:
    Ui::GameWindow* ui;

    void close_app(void);

signals:
    void KillApp(void);
    void Geometry(const QRect&);
};

#endif // GAMEWINDOW_H
