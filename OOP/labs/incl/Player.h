/**
 * @file Player.h
 * @memberof authication system
 * @category account
 * @namespace auth
 *
 * @brief Class of simple player.
 *
 * @details
 *
 * @todo: type cast operator.
 *
 * @author d_pch
 * @version 0.01
 * @date 07-Oct-16 21:46
 */

#ifndef PLAYER_H
#define PLAYER_H

#ifndef DEFPLR //default data for Player if they aren't set manually
#define DEFNNM "=nagibator="
#endif //DEFPLR

#include "User.h"

namespace auth {

    class Player : public User {
    public:
        Player(void);
        Player(const std::string& nickname,
               const std::string& password,
               const std::string& name = DEFNAM,
               const std::string& surname = DEFSNM,
               const std::string& login = DEFLOG,
               const std::string& key = AUTOKEY); //(+err::err)
        virtual ~Player(void);
        Player(const Player& player);

        virtual bool set_nickname(const std::string& nickname);
        virtual std::string get_nickname(void) const;

        void set_score(const unsigned long int score);
        unsigned long int get_score(void) const;

        void add_game_won(void);
        void add_game_lost(void);
        unsigned int get_num_games_won(void);
        unsigned int get_num_games_lost(void);
        unsigned long int get_num_of_games(void); //both won and lost

        void set_level(const unsigned short int level);
        unsigned short int get_level(void);

        virtual Player& operator=(const Player& player);

        virtual unsigned long int operator+(unsigned long int score);
        virtual unsigned long int operator-(unsigned long int score);
        virtual unsigned long int operator*(unsigned long int score);

        virtual Player& operator+(const Player& player);
        virtual Player& operator-(const Player& player);

        virtual unsigned int operator++(void); //add won game
        virtual unsigned int operator++(int tmp); //add won game
        virtual unsigned int operator--(void); //add lost game
        virtual unsigned int operator--(int tmp); //add lost game

        virtual bool operator==(const Player& player) const;
        virtual bool operator!=(const Player& player) const;
        virtual bool operator>(const Player& player) const;
        virtual bool operator>=(const Player& player) const;
        virtual bool operator<(const Player& player) const;
        virtual bool operator<=(const Player& player) const;

        static bool correct_nickname(const std::string& nickname); //check nickname format is correct
    protected:
        std::string m_nickname; //nickname of player
        unsigned long int m_score; //max score of player
        unsigned short int m_level; //user level
        unsigned int m_num_games_won;
        unsigned int m_num_games_lost;
    };

}

#endif //PLAYER_H
