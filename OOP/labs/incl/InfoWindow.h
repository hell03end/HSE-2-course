#ifndef INFOWINDOW_H
#define INFOWINDOW_H

#include <QDialog>

namespace Ui {
    class InfoWindow;
}

class InfoWindow : public QDialog {
    Q_OBJECT

public:
    explicit InfoWindow(QWidget *parent = 0);
    ~InfoWindow();

private:
    Ui::InfoWindow* ui;

    void close_app(void);
};

#endif // INFOWINDOW_H
