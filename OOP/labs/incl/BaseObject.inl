/**
 * @file Object.inl
 * @category basic object class
 * @namespace base
 *
 * @brief Class of object, which is basic to all other classes.
 *
 * @details
 * Contains fields and their methods to avoid exception generation in constructors.
 * @todo: interface.
 *
 * @author d_pch
 * @version 0.1
 * @date 11-Oct-16
 */

#ifndef BASE_OBJECT_INL
#define BASE_OBJECT_INL

#include "ErrMessages.h"

namespace basic {

    class BaseObject {
    public:
        BaseObject(void) {
            m_err_amount = 0;
            m_err = EXIT_SUCCESS; //means no errors
        }
        virtual ~BaseObject(void) {
            /* nothing to delete */
        }

        void set_error(const int errnum) {
            m_err = errnum;
            m_err_amount++;
        }
        int get_error(void) const {
            return m_err;
        }
        unsigned int get_err_amount(void) const {
            unsigned int result = m_err_amount;

            /* restore normal values */
            m_err_amount = 0;
            m_err = EXIT_SUCCESS;

            return result;
        }
        unsigned int show_err_amount(void) const {
            return m_err_amount;
        }
    private:
        mutable unsigned int m_err_amount;
        mutable int m_err; //store type of error
    };

}

#endif //BASE_OBJECT_INL
