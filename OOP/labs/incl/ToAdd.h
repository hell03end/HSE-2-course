//Remember to delete this trash later!

#ifndef THREADTOADD_H
#define THREADTOADD_H

#include <QObject>
#include <random>
#include <ctime>
#include <unistd.h> //to use sleep()
#include <QDebug>

class ToAdd : public QObject {
    Q_OBJECT
public:
    start_add() {
        srand(static_cast<unsigned int>(time(0)));
        qRegisterMetaType<std::string>("std::string"); //¯\_(ツ)_/¯
        /* infinite cycle, which emits signals for adding new users: */
        while (true) {
            sleep(rand()%10); //sleep for some time
            std::string name = names[rand()%10]; //get rand name
            std::string password = passwords[rand()%8]; //get rand password
            qDebug() << QString::fromStdString("adding new user...");
            std::string info = "Name: "+name+"  Password: "+password;
            qDebug() << QString::fromStdString(info);
            emit(AddUser(name, password, name, name, name));
        }
    }
private:
    std::string names [10] = {
        static_cast<std::string>("Stan"), static_cast<std::string>("Eric"),
        static_cast<std::string>("Kenny"), static_cast<std::string>("Kyle"),
        static_cast<std::string>("Butters"), static_cast<std::string>("Token"),
        static_cast<std::string>("Trump"), static_cast<std::string>("skankhunt42"),
        static_cast<std::string>("DeLarge"), static_cast<std::string>("¯\_(ツ)_/¯")
    }; //Merde
    std::string passwords [8] = {
        static_cast<std::string>("someCanadianPassword,budy"), static_cast<std::string>("slim!shady"),
        static_cast<std::string>("1488 ss"), static_cast<std::string>("I'm singing in the rain..."),
        static_cast<std::string>("c=d1ck==3"), static_cast<std::string>("qwerty1"),
        static_cast<std::string>("Make America Gread Again!"), static_cast<std::string>("Ludwig_Van")
    };
signals:
    void AddUser(const std::string &nickname, const std::string &password,
                 const std::string &name, const std::string &surname,
                 const std::string &login);
};

#endif // THREADTOADD_H
