/**
 * @file include/ErrTypes.h
 * @category errors
 * @namespace err
 *
 * @brief Declare the types of errors.
 *
 * @author d_pch
 * @version 1.02
 * @date 07-Oct-16
 */

#ifndef ERRMSSG_H
#define ERRMSSG_H

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif //EXIT_SUCCESS
#ifndef SUCCESS
#define SUCCESS 0
#endif //SUCCESS
#define RTRNSCCSS 0

#define NFOUND -1 //    element wasn't found

#define PTRNLL -18 //   NULL-pointer dereference
#define NALLOC1 -17 //  can't allocate memory (1)
#define NALLOC -16 //   can't allocate memory
#define INVPOS -15 //   invalid position
#define POSOUT -14 //   out of borders of container
#define EMPVAL -13 //   no elements in container

#define ACCDEN -100 //access denied
#define WRGDAT -101 //  can't assign: wrong data
#define WRGDAT1 -102 //  can't assign: wrong data (1)
#define CNTCRT -103 //can't create
#define CNTUPD -104 //can't update data

/* server */
#define SRVRNAVLBL -200 //server isn't available
#define SRVRRNTERR -205 //server runtime error
#define SRVROVERLD -206 //server overload

#endif //ERRMSSG_H
