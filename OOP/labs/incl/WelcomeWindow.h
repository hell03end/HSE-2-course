#ifndef WELCOMEWINDOW_H
#define WELCOMEWINDOW_H

#include "ErrMessages.h"
#include <QMessageBox>
#include <QWidget>

namespace Ui {
    class WelcomeWindow; //why the fuck is done in such way?
}

class WelcomeWindow : public QWidget {
    Q_OBJECT
public:
    explicit WelcomeWindow(QWidget* parent = NULL);
    ~WelcomeWindow(void);

    void signed_in(int code, int key = 0); //check sign-in' process result
    void signed_up(int code, int key = 0); //check registration' process result
    void set_geometry(const QRect& size);

private:
    Ui::WelcomeWindow* ui; //GUI for welcome window

    void close_app(void); //sent KillApp signal
    void sign_up(void); //try to create new user with such data
    void sign_in(void); //try to sign in with login and password
    void call_about(void); //call window with info
    void play_online(bool online = false); //go to mainwindow

    void show_message(int icon, const std::string& title,
                   const std::string& body = "", const std::string& detals = "");

    /* navigation */
    void go_to_registration(void); //+call_about()
    void go_to_welcome(void);
    void restart(void); //clea  all fields and go to welcome

    void button_enable_sign_in(void); //check all fields needed to enable sign_in button
    void button_enable_register(void); //check all fields needed to enable register button

signals:
    void KillApp(void); //call app closing
    void PlayOnline(bool); //return game status
    void SignIn(const std::string&, const std::string&); //sent user credentials
    void SignUp(const std::string&, const std::string&, const std::string&,
                const std::string&, const std::string&); //sent credentials for new user
    void CallAbout(void); //show info window
    void Geometry(const QRect&); //return geometry of window
};

#endif // WELCOMEWINDOW_H
