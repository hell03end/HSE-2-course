#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "MainWindow.h"
#include "WelcomeWindow.h"
#include "GameWindow.h"
#include "InfoWindow.h"
#include "Player.h"
#include <fstream>
#include <QObject>
#include <QMutex>
#include <string>
/* TEMP: */
#include "List.inl"
#include "ToAdd.h"
#include "ToDel.h"
#include <QDebug>
#include <QThread>
#include <QProgressBar>

namespace controllers {

    class ClientUIController : public QObject {
        Q_OBJECT
    public:
        explicit ClientUIController(void);
        ~ClientUIController(void);

    private:
        MainWindow* main_w_ui; //graphical user interface
        GameWindow* game_w_ui;
        InfoWindow* info_w_ui;
        WelcomeWindow* welcome_w_ui;
        containers::List<auth::Player>* players; //TEMP
        auth::Player* player; //current player
        QRect m_curr_geom;
        bool m_sounds;
        mutable QMutex mutex;
        ToAdd* to_add;
        QThread* add_th;
        ToDel* to_del;
        QThread* del_th;
        QProgressBar* prb;

        void close_app(void);
        void show_about(void);
        void remember_geom(const QRect& size);

        /* welcome window */
        void open_main_win(bool);
        void user_add(const std::string& nickname, const std::string& password,
                      const std::string& name, const std::string& surname,
                      const std::string& login);
        void user_log_in(const std::string& login, const std::string& password);

        /* main window */
        void open_game_w(MainWindow::GameMode mode, bool online);
        void set_sound(bool on);
        void rest_score(void);
        void user_delete(void);
        void open_welcome_w(void);
        void players_show(MainWindow::GameMode mode);
        void change_nickname(const std::string& nickname);

        void add_user(const std::string& nickname, const std::string& password,
                      const std::string& name, const std::string& surname,
                      const std::string& login);
        void delete_user(void);
    signals:
        void Occupied(int n); //Show how many free space is occupied
    };

}

#endif // CONTROLLER_H
