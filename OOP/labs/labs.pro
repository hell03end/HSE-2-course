#-------------------------------------------------
#
# Project created by QtCreator 2016-10-10T23:43:48
# Updated by d_pch 2016-15-10 all files included + folders added
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = labs
TEMPLATE = app


SOURCES += src/main.cpp\
    src/MainWindow.cpp \
    src/WelcomeWindow.cpp \
    src/GameWindow.cpp \
    src/InfoWindow.cpp \
    src/User.cpp \
    src/Player.cpp \
    src/ClientUIController.cpp

HEADERS  += \
    incl/MainWindow.h \
    incl/WelcomeWindow.h \
    incl/GameWindow.h \
    incl/InfoWindow.h \
    incl/BaseObject.inl \
    incl/User.h \ #class for user
    incl/Player.h \ #class for player
    incl/List.inl \  #basic manual list
    incl/ClientUIController.h \
    incl/ErrMessages.h \  #handling with errors
    incl/ToAdd.h \
    incl/ToDel.h

FORMS    += \
    ui/MainWindow.ui \
    ui/WelcomeWindow.ui \
    ui/GameWindow.ui \
    ui/InfoWindow.ui

CONFIG += c++17 #why not?:)
