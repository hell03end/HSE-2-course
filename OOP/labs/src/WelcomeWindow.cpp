//#include "WelcomeWindow.h" // <-- the fuck?!
#include "../incl/WelcomeWindow.h"
#include "ui_welcomewindow.h"

WelcomeWindow::WelcomeWindow(QWidget* parent) : QWidget(parent) {
    ui = new Ui::WelcomeWindow; //create ui
    ui->setupUi(this);

    /* create regular expressions to check input */
    QRegExp is_input_name("[a-zA-Zа-яА-Я]{2,20}");
    QRegExp is_input_login("[a-zA-Zа-яА-Я0-9.]{3,20}");
    QRegExp is_input_pass("[a-zA-Zа-яА-Я0-9.]{7,30}");

    /* create input validators */
    ui->lineEdit_login->setValidator(new QRegExpValidator(is_input_login, this));
    ui->lineEdit_password->setValidator(new QRegExpValidator(is_input_pass, this));
    ui->lineEdit_name_reg->setValidator(new QRegExpValidator(is_input_name, this));
    ui->lineEdit_surname_reg->setValidator(new QRegExpValidator(is_input_name, this));
    ui->lineEdit_login_reg->setValidator(new QRegExpValidator(is_input_login, this));
    ui->lineEdit_password_reg->setValidator(new QRegExpValidator(is_input_pass, this));
    ui->lineEdit_confirm_password_reg->setValidator(new QRegExpValidator(is_input_pass, this));
    ui->lineEdit_nickname_reg->setValidator(new QRegExpValidator(is_input_login, this));

    /* connect each action with input fields with inspection */
    connect(ui->lineEdit_login, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_sign_in);
    connect(ui->lineEdit_password, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_sign_in);
    connect(ui->lineEdit_login_reg, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_register);
    connect(ui->lineEdit_password_reg, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_register);
    connect(ui->lineEdit_confirm_password_reg, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_register);
    connect(ui->lineEdit_name_reg, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_register);
    connect(ui->lineEdit_surname_reg, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_register);
    connect(ui->lineEdit_nickname_reg, &QLineEdit::textChanged,
            this, &WelcomeWindow::button_enable_register);
    connect(ui->checkBox_accept, &QCheckBox::clicked,
            this, &WelcomeWindow::button_enable_register);

    /* buttons' actions */
    connect(ui->pushButton_exit, &QPushButton::clicked,
            this, &WelcomeWindow::close_app);
    connect(ui->pushButton_sign_up, &QPushButton::clicked,
            this, &WelcomeWindow::go_to_registration);
    connect(ui->pushButton_back, &QPushButton::clicked,
            this, &WelcomeWindow::go_to_welcome);
    connect(ui->pushButton_play_offline, &QPushButton::clicked,
            this, &WelcomeWindow::play_online);
    connect(ui->pushButton_register, &QPushButton::clicked,
            this, &WelcomeWindow::sign_up);
    connect(ui->pushButton_sign_in, &QPushButton::clicked,
            this, &WelcomeWindow::sign_in);
    connect(ui->pushButton_about, &QPushButton::clicked,
            this, &WelcomeWindow::call_about);
}

WelcomeWindow::~WelcomeWindow() {
    delete ui;
}

void WelcomeWindow::signed_in(int code, int key) {
    if (code == RTRNSCCSS) {
        restart();
        play_online(false);
    } else {
        if (code == ACCDEN) {
            show_message(4, "Wrong login/password!",
                       "Try another credentials or Sign Up first. You also can play offline.");
        } else if (code == SRVRNAVLBL) {
            show_message(4, "Server isn't available!", "But you can play offline instead.");
        } else if (code == SRVROVERLD) {
            show_message(4, "Server is overloaded!",
                       "You can wait for a few minutes or play offline instead.");
        }
    }
}

void WelcomeWindow::signed_up(int code, int key) {
    if (code == RTRNSCCSS) {
        restart();
        play_online(true);
    } else {
        if (code == WRGDAT) {
            if (key == 0) {
                show_message(4, "User with such credentials exists!",
                           "Please, enter another credentials.");
                ui->lineEdit_login_reg->clear();
                ui->lineEdit_login_reg->setPlaceholderText("LOGIN");
                ui->lineEdit_nickname_reg->clear();
                ui->lineEdit_nickname_reg->setPlaceholderText("USERNAME");
            } if (key == 1) {
                show_message(4, "User with such login exists!", "Please, enter another login.");
                ui->lineEdit_login_reg->clear();
                ui->lineEdit_login_reg->setPlaceholderText("LOGIN");
            } if (key == 2) {
                show_message(4, "User with such username exists!", "Please, enter another username.");
                ui->lineEdit_nickname_reg->clear();
                ui->lineEdit_nickname_reg->setPlaceholderText("USERNAME");
            }
        } else if (code == WRGDAT1) {
            if (key == 0) {
                show_message(4, "Incorrect format of user data!", "Please, enter data again.",
                           "Login, nickname >= 3 & <= 20 symbols, without spaces.\n"
                           "Password >=7 & <=30 symbols, at least 1 digit or symbol and 1 letter.\n"
                           "Name, surname >=2 & <= 20 symbols, only letters (one spave available).");
                ui->lineEdit_login_reg->clear();
                ui->lineEdit_nickname_reg->clear();
                ui->lineEdit_password_reg->clear();
                ui->lineEdit_confirm_password_reg->clear();
                ui->lineEdit_name_reg->clear();
                ui->lineEdit_surname_reg->clear();
            } //@undone
        } else if (code == SRVRNAVLBL) {
            show_message(4, "Server isn't available!", "But you can play offline instead.");
        } else if (code == SRVROVERLD) {
            show_message(4, "Server is overloaded!", "You can wait for a few minutes or play offline instead.");
        }
    }
}

void WelcomeWindow::set_geometry(const QRect& size) {
    this->setGeometry(size);
}

void WelcomeWindow::close_app() {
    /* do smth */
    emit KillApp();
    this->close();
}

void WelcomeWindow::sign_up() {
    if (ui->lineEdit_password_reg->text() !=
        ui->lineEdit_confirm_password_reg->text()) {
        show_message(4, "Passwords don't match!", "Enter password again.");
        ui->lineEdit_confirm_password_reg->clear();
        ui->lineEdit_password_reg->clear(); //forget passwords
    } else {
        /* send signal with player data trying to register */
        emit Geometry(this->geometry());
        emit SignUp(ui->lineEdit_nickname_reg->text().toStdString(),
                    ui->lineEdit_password_reg->text().toStdString(),
                    ui->lineEdit_name_reg->text().toStdString(),
                    ui->lineEdit_surname_reg->text().toStdString(),
                    ui->lineEdit_login_reg->text().toStdString());
    }
}

void WelcomeWindow::sign_in() {
    /* send signal with player data trying to log in */
    emit Geometry(this->geometry());
    emit SignIn(ui->lineEdit_login->text().toStdString(),
                ui->lineEdit_password->text().toStdString());
}

void WelcomeWindow::call_about() {
    emit CallAbout();
}

void WelcomeWindow::play_online(bool online) {
    this->setEnabled(false);
    this->hide();
    emit Geometry(this->geometry());
    emit PlayOnline(online);
}

void WelcomeWindow::show_message(int type,
                                 const std::__cxx11::string& title,
                                 const std::__cxx11::string& body,
                                 const std::__cxx11::string& detals) {
    /* show error with some message */
    QMessageBox message_w;
    if (type == 1) {
        message_w.setIcon(QMessageBox::Question);
    } else if (type == 2) {
        message_w.setIcon(QMessageBox::Information);
    } else if (type == 3) {
        message_w.setIcon(QMessageBox::Warning);
    } else if (type == 4) {
        message_w.setIcon(QMessageBox::Critical);
    }
    message_w.setDefaultButton(QMessageBox::Ok); //create only one button
    message_w.setText("<b>"+QString::fromStdString(title)+"</b>"); //show title message
    if (body != "") {
        message_w.setInformativeText(QString::fromStdString(body)); //add informative text
    }
    if (detals != "") {
        message_w.setDetailedText(QString::fromStdString(detals)); //add detailed text
    }
    message_w.exec();
}

void WelcomeWindow::go_to_registration() {
    ui->lineEdit_password->clear(); //forget password
    if (ui->lineEdit_login_reg->text().toStdString().empty() &&
        ui->lineEdit_name_reg->text().toStdString().empty() &&
        ui->lineEdit_nickname_reg->text().toStdString().empty() &&
        ui->lineEdit_surname_reg->text().toStdString().empty()) {
        /* show only if visit registration page first time */
        emit CallAbout(); //show info window
    }
    if (!ui->lineEdit_login->text().toStdString().empty() &&
            ui->lineEdit_login_reg->text().toStdString().empty()) {
        ui->lineEdit_login_reg->setText(ui->lineEdit_login->text());
        ui->lineEdit_nickname_reg->setText(ui->lineEdit_login->text());
    }
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->indexOf(ui->page_registration));
}

void WelcomeWindow::go_to_welcome() {
    ui->lineEdit_password_reg->clear(); //forget passwords:
    ui->lineEdit_confirm_password_reg->clear();
    if (!ui->lineEdit_login_reg->text().toStdString().empty() &&
            ui->lineEdit_login->text().toStdString().empty()) {
        ui->lineEdit_login->setText(ui->lineEdit_login_reg->text());
    }
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->indexOf(ui->page_welcome));
}

void WelcomeWindow::restart() {
    ui->lineEdit_login_reg->clear();
    ui->lineEdit_name_reg->clear();
    ui->lineEdit_nickname_reg->clear();
    ui->lineEdit_surname_reg->clear();
    ui->lineEdit_login->clear();
    ui->lineEdit_password->clear();
    ui->lineEdit_password_reg->clear();
    ui->lineEdit_confirm_password_reg->clear();
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->indexOf(ui->page_welcome));
}

void WelcomeWindow::button_enable_sign_in() {
    ui->pushButton_sign_in->setEnabled(ui->lineEdit_login->hasAcceptableInput() &&
                                       ui->lineEdit_password->hasAcceptableInput());
}

void WelcomeWindow::button_enable_register() {
    ui->pushButton_register->setEnabled(ui->lineEdit_login_reg->hasAcceptableInput() &&
                                        ui->lineEdit_password_reg->hasAcceptableInput() &&
                                        ui->lineEdit_confirm_password_reg->hasAcceptableInput() &&
                                        ui->lineEdit_name_reg->hasAcceptableInput() &&
                                        ui->lineEdit_surname_reg->hasAcceptableInput() &&
                                        ui->lineEdit_nickname_reg->hasAcceptableInput() &&
                                        ui->checkBox_accept->isChecked());
}
