#include "../incl/ClientUIController.h"

namespace controllers {

    ClientUIController::ClientUIController() {
        m_sounds = true;
        player = nullptr;
        players = new containers::List<auth::Player>();

        main_w_ui = new MainWindow();
        info_w_ui = new InfoWindow();
        game_w_ui = new GameWindow();
        welcome_w_ui = new WelcomeWindow();

        welcome_w_ui->show(); //show welcome window

        connect(welcome_w_ui, &WelcomeWindow::CallAbout,
                this, &ClientUIController::show_about);
        connect(welcome_w_ui, &WelcomeWindow::Geometry,
                this, &ClientUIController::remember_geom);
        connect(main_w_ui, &MainWindow::CallAbout,
                this, &ClientUIController::show_about);
        connect(main_w_ui, &MainWindow::Geometry,
                this, &ClientUIController::remember_geom);

        connect(welcome_w_ui, &WelcomeWindow::PlayOnline,
                this, &ClientUIController::open_main_win);
        connect(welcome_w_ui, &WelcomeWindow::SignUp,
                this, &ClientUIController::user_add);
        connect(welcome_w_ui, &WelcomeWindow::SignIn,
                this, &ClientUIController::user_log_in);

        connect(main_w_ui, &MainWindow::Play,
                this, &ClientUIController::open_game_w);
        connect(main_w_ui, &MainWindow::Sound,
                this, &ClientUIController::set_sound);
        connect(main_w_ui, &MainWindow::RestScore,
                this, &ClientUIController::rest_score);
        connect(main_w_ui, &MainWindow::DeleteUser,
                this, &ClientUIController::user_delete);
        connect(main_w_ui, &MainWindow::LogOut,
                this, &ClientUIController::open_welcome_w);
        connect(main_w_ui, &MainWindow::ShowPlayers,
                this, &ClientUIController::players_show);
        connect(main_w_ui, &MainWindow::ChangeNickname,
                this, &ClientUIController::change_nickname);

        connect(main_w_ui, &MainWindow::KillApp,
                this, &ClientUIController::close_app);
        connect(game_w_ui, &GameWindow::KillApp,
                this, &ClientUIController::close_app);
        connect(welcome_w_ui, &WelcomeWindow::KillApp,
                this, &ClientUIController::close_app);

        /***************************************************
         *                                                 *
         *                      !!!!!                      *
         *                     Threads                     *
         *                      !!!!!                      *
         *                                                 *
         * *************************************************/
         to_add = new ToAdd(); //create new object which will add 'Canadians' to 'server'
         add_th = new QThread(); //create new thread, in which code upstairs will run
         connect(add_th, &QThread::started, to_add, &ToAdd::start_add); //connect theads start with adding cycle
         connect(to_add, &ToAdd::AddUser, this, &ClientUIController::add_user);
         to_add->moveToThread(add_th); //move object from current thread to it's own
         add_th->start(); //run thread

         to_del  = new ToDel(); //create new object which will f*ck 'Canadians' to death
         del_th = new QThread(); //create new thread, in which code upstairs will run
         connect(del_th, &QThread::started, to_del, &ToDel::start_del); //connect theads start with adding cycle
         connect(to_del, &ToDel::FuckThemAll2Death, this, &ClientUIController::delete_user);
         to_del->moveToThread(del_th); //move object from current thread to it's own
         del_th->start(); //run thread

         prb = new QProgressBar; //create progress bar
         connect(this, &ClientUIController::Occupied, prb, &QProgressBar::setValue);
         prb->setValue(0); //initial value
         prb->resize(473, 30); //prb size
         prb->show();
    }

    ClientUIController::~ClientUIController() {
        delete welcome_w_ui;
        delete game_w_ui;
        delete info_w_ui;
        delete main_w_ui;
        delete players;
        delete player;
        delete to_add;
        delete to_del;
        delete add_th;
        delete del_th;
        delete prb;
    }

    void ClientUIController::close_app() {
        exit(0);
    }

    void ClientUIController::show_about() {
//        QMutexLocker locker(&mutex);
        info_w_ui->show();
    }

    void ClientUIController::remember_geom(const QRect& size) {
        m_curr_geom = size;
    }

    void ClientUIController::open_main_win(bool key) {
        if (!main_w_ui) {
            main_w_ui = new MainWindow();
        }
        if (key) {
            main_w_ui->set_online_settings(player->get_nickname(), player->get_score());
        } else {
            main_w_ui->set_offline_settings();
        }
        main_w_ui->setEnabled(true);
        main_w_ui->set_geometry(m_curr_geom);
        main_w_ui->show();
    }

    void ClientUIController::user_add(const std::__cxx11::string& nickname,
                                      const std::__cxx11::string& password,
                                      const std::__cxx11::string& name,
                                      const std::__cxx11::string& surname,
                                      const std::__cxx11::string& login) {
        mutex.lock();
        if (players->size() == 10) {
            welcome_w_ui->signed_up(SRVROVERLD);
            return;
        }
        try {
            if (player == nullptr) {
                player = new auth::Player();
            }
            if (!player->set_nickname(nickname) || !player->set_login(login) ||
                !player->set_name(name) || !player->set_password(password) ||
                !player->set_surname(surname)) {
                delete player; //free memory
                player = nullptr;
                welcome_w_ui->signed_up(WRGDAT);
            } else {
//                players->append(*player); //add player to players
                std::string filename = player->get_nickname() + ".txt";
                std::ofstream fl(filename);
                if (fl.is_open()) {
                    fl << player->get_login() << std::endl;
//                    fl << player->get_password() << std::endl;
//                    fl << player->get_name() << std::endl;
//                    fl << player->get_surname() << std::endl;
//                    fl << std::to_string(player->get_score()) << std::endl;
//                    fl << std::to_string(player->get_num_games_won()) << std::endl;
//                    fl << std::to_string(player->get_num_games_lost()) << std::endl;
//                    fl << std::to_string(player->get_level()) << std::endl;
                    fl.close();
                }
                welcome_w_ui->signed_up(SUCCESS);
            }
        } catch (int err) {
            welcome_w_ui->signed_up(WRGDAT1);
        }
        mutex.unlock(); //release mutex before lock it again in next method
        players_show(MainWindow::SNGL);
    }

    void ClientUIController::user_log_in(const std::__cxx11::string& login,
                                         const std::__cxx11::string& password) {
        QMutexLocker locker(&mutex);
        try {
            std::ifstream fl(login + ".txt");
            if (fl.is_open()) {
                std::string temp;
                getline(fl, temp);
                if (temp == login) {
                    fl.close();
                    welcome_w_ui->signed_in(SUCCESS);
                } else {
                    fl.close();
                    welcome_w_ui->signed_in(ACCDEN);
                }
//                player = new auth::Player();
//                std::string temp;
//                getline(fl, temp);
//                player->set_nickname(nickname);
//                player->set_login(login);
//                player->set_name(name);
//                player->set_password(password);
//                player->set_surname(surname);
//                std::ofstream fl(player->get_nickname() + ".txt");
//                fl << player->get_login() << std::endl;
//                fl << player->get_password() << std::endl;
//                fl << player->get_name() << std::endl;
//                fl << player->get_surname() << std::endl;
//                fl << player->get_score() << std::endl;
//                fl << player->get_num_games_won() << std::endl;
//                fl << player->get_num_games_lost() << std::endl;
//                fl << player->get_level() << std::endl;
                fl.close();
            }
            else if (players->empty()) {
                welcome_w_ui->signed_in(SRVRNAVLBL);
            } else {
                auth::Player t_player;
                t_player.set_login(login);
                t_player.set_password(password);
                int index = players->find(t_player, [](const auth::Player& pl1, const auth::Player& pl2) {
                    return pl1.get_login() == pl2.get_login() && pl1.get_password() == pl2.get_password();
                });
                if (index == NFOUND) {
                    welcome_w_ui->signed_in(ACCDEN);
                } else {
//                    t_player = players[index];
//                    player = new auth::Player(t_player);
                    welcome_w_ui->signed_in(SUCCESS);
                }
            }
        } catch (int err) {
            welcome_w_ui->signed_in(ACCDEN);
        }
    }

    void ClientUIController::open_game_w(MainWindow::GameMode mode, bool online) {
        if (!game_w_ui) {
            game_w_ui = new GameWindow();
        }
        if (mode == MainWindow::SNGL) {
            /* select mode */
        } else if (mode == MainWindow::CMND) {
            /* select mode */
        } else {
            /* select mode */
        }
        game_w_ui->setEnabled(true);
        game_w_ui->set_geometry(m_curr_geom);
        game_w_ui->show();
    }

    void ClientUIController::set_sound(bool on) {
        m_sounds = on;
        /* do smth */
    }

    void ClientUIController::rest_score() {
        player->set_score(0);
    }

    void ClientUIController::user_delete() {
        mutex.lock();
        players->remove_all_same(*player);
        delete player;
        player = nullptr;
        open_welcome_w();
        mutex.unlock(); //release mutex before lock it again in next method
        players_show(MainWindow::SNGL);
    }

    void ClientUIController::open_welcome_w() {
        delete player;
        player = nullptr;

        if (!welcome_w_ui) {
            welcome_w_ui = new WelcomeWindow();
        }
        welcome_w_ui->setEnabled(true);
        welcome_w_ui->set_geometry(m_curr_geom);
        welcome_w_ui->show();
    }

    void ClientUIController::players_show(MainWindow::GameMode mode) {
        QMutexLocker locker(&mutex);
        /* send players to output */
        main_w_ui->show_players(*players);
        int n = (player == nullptr) ? 0 : 10;
        emit(Occupied(players->size()*10+n));
    }

    void ClientUIController::change_nickname(const std::__cxx11::string &nickname) {
        QMutexLocker locker(&mutex);
        if (!player->set_nickname(nickname)) {
            main_w_ui->update_nickname("");
        } else {
            main_w_ui->update_nickname(nickname);
        }
    }

    void ClientUIController::add_user(const std::__cxx11::string &nickname,
                                      const std::__cxx11::string &password,
                                      const std::__cxx11::string &name,
                                      const std::__cxx11::string &surname,
                                      const std::__cxx11::string &login) {
        if (players->size() < 10) {
            try {
                auth::Player temp_player(nickname, password, name, surname, login);
                mutex.lock();
                if (players->find(temp_player, [](const auth::Player& p1, const auth::Player& p2)
                { return p1.get_nickname() == p2.get_nickname() && p1.get_login() == p2.get_login(); })
                        == NFOUND) {
                    players->append(temp_player); //add player to players
                    mutex.unlock(); //release mutex before lock it again in next method
                    std::string filename = temp_player.get_nickname() + ".txt";
                    /* write User to file: */
                    std::ofstream fl(filename);
                    if (fl.is_open()) {
                        fl << temp_player.get_login() << std::endl;
                        fl.close();
                    }
                    qDebug() << QString::fromStdString("New User have added!");
                } else {
                    mutex.unlock(); //release mutex before lock it again in next method
                    qDebug() << QString::fromStdString("I'm not your buddy, gay!"); // (c) average canadian.
                }
            } catch (int err) {
                qDebug() << QString::fromStdString("err");
            }
        } else {
            qDebug() << QString::fromStdString("Can't add new User, Trump has built the wall!");
        }
        players_show(MainWindow::SNGL);
    }

    void ClientUIController::delete_user() {
        mutex.lock();
        int n = players->size();
        if (n > 0) {
            players->remove(players->size()-1);
            qDebug() << QString::fromStdString("Make America Great Again!"); //(c) Donald Trump (Home Along 2)
        } else {
            qDebug() << QString::fromStdString("There are no *Canadians* on server!:(");
        }
        mutex.unlock(); //release mutex before lock it again in next method
        players_show(MainWindow::SNGL);
    }
}
