//#include "GameWindow.h" // <-- the fuck?!
#include "../incl/GameWindow.h"
#include "ui_GameWindow.h"

GameWindow::GameWindow(QWidget *parent) : QDialog(parent) {
    ui = new Ui::GameWindow;
    ui->setupUi(this);

    connect(ui->pushButton_exit, &QPushButton::clicked,
            this, &GameWindow::close_app);
}

GameWindow::~GameWindow() {
    delete ui;
}

void GameWindow::set_geometry(const QRect& size) {
    this->setGeometry(size);
}

void GameWindow::close_app() {
    emit KillApp();
    this->close();
}
