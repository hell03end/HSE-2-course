//#include "User.h" // <-- the fuck?!
#include "../incl/User.h"

namespace auth {

    User::User() {
        m_key = AUTOKEY;
        m_login = DEFLOG;
        m_password = crypt(DEFPAS, AUTOKEY);
        m_name = DEFNAM;
        m_surname = DEFSNM;
    } //@tested

    User::User(const std::string& name,
               const std::string& surname,
               const std::string& password,
               const std::string& login,
               const std::string& key) {
        if (!set_login(login)) {
            m_login = DEFLOG;
            set_error(WRGDAT);
        }
        if (!set_name(name)) {
            m_name = DEFNAM;
            set_error(WRGDAT1);
        }
        if (!set_surname(surname)) {
            m_surname = DEFSNM;
            set_error(WRGDAT);
        }
        if (!set_password(password, key)) {
            m_password = crypt(DEFPAS, key);
            set_error(WRGDAT1);
        }
    } //@tested

    User::~User(void) {
        /* nothing to delete */
    } //@tested

    User::User(const User& user) {
        /* imitation of coping. Nothing to copy if seriously */
        m_key = user.m_key;
        m_login = user.m_login;
        m_name = user.m_name;
        m_surname = user.m_surname;
        m_password = user.m_password;
    } //@tested

    bool User::set_login(const std::string& login) {
        if (!correct_login(login)) {
            return false;
        }
        m_login = to_standard_login(login);

        return true;
    } //@tested

    const std::string& User::get_login(void) const {
        return m_login;
    } //@tested

    bool User::set_name(const std::string& name) {
        if (!correct_name(name)) {
            return false;
        }
        m_name = to_standard_name(name);

        return true;
    } //@tested

    const std::string& User::get_name(void) const {
        return m_name;
    } //@tested

    bool User::set_surname(const std::string& surname) {
        if (!correct_name(surname)) {
            return false;
        }
        m_surname = to_standard_name(surname);

        return true;
    } //@tested

    const std::string& User::get_surname(void) const {
        return m_surname;
    } //@tested

    bool User::set_password(const std::string& password, const std::string& key) {
        if (!correct_password(password)) {
            return false;
        }
        m_key = key; //remember key
        m_password = crypt(password, key); //crypt password with key

        return true;
    } //@tested

    const std::string& User::get_password(const std::string& key) const {
        if (key != m_key) /* wrong key */ {
            throw ACCDEN;
            return key;
        }
        return decrypt(m_password, key);
    } //@tested

    void User::set_key(const std::string& key) {
        m_password = decrypt(m_password, m_key); //return original password
        m_password = crypt(m_password, key); //crypt password again with new key
        m_key = key; //remember new key
    } //@tested

    User& User::operator=(const User& user) {
        if (this != &user) {
            m_key = user.m_key;
            m_login = user.m_login;
            m_name = user.m_name;
            m_surname = user.m_surname;
            m_password = user.m_password;
        }

        return *this;
    } //@tested

    bool User::correct_name(const std::string& name) {
        /* check length */
        if (name.size() < MINNAM || name.size() > MAXNAM) {
            return false;
        }
        bool space = false;
        for (auto c : name) {
            if (iscntrl(c) || ispunct(c) || isdigit(c)) {
                return false;
            } else if (isspace(c) && !space) {
                space = true;
            } else if (isspace(c) && space) {
                return false; //only one space is available
            }
        }

        return !(space && name.size() < 2*MINNAM); //name with space should be a bit longer
    } //@tested

    bool User::correct_login(const std::string& login) {
        /* check length */
        if (login.size() < MINLOG || login.size() > MAXLOG) {
            return false;
        }
        /* check symbols */
        for (auto c : login) {
            if (iscntrl(c) || isspace(c)) {
                return false;
            }
        }

        return true;
    } //@tested

    bool User::correct_password(const std::string& password) {
        /* check length */
        if (password.size() < MINPAS || password.size() > MAXPAS) {
            return false;
        }
        /* check complexity */
        bool digit = false;
        bool alpha = false;
        for (auto c : password) {
            if (isdigit(c) || ispunct(c)) {
                digit = true;
            } else if (isalpha(c)) {
                alpha = true;
            }
            if (iscntrl(c)) {
                return false;
            }
        }
        return (digit && alpha);
    } //@tested

//@undone crypt message in some way
    std::string User::crypt(const std::string& message, const std::string& key) {
        return message;
    } //@tested

//@undone crypt message in some way
    std::string User::decrypt(const std::string& message, const std::string& key) {
        return message;
    } //@tested

//@undone get rael hash
//    unsigned long int User::get_hash(const std::string& message) {
//        unsigned long int hash = 0;
//        for (auto c : message) {
//            hash += static_cast<unsigned long int>(c);
//        }
//
//        return hash;
//    }

    std::string User::to_standard_login(const std::string& login) {
        //@undone
//        std::string result = "";
//
//        for (auto c : login) {
//            int tmp = toupper(c);
//            result + static_cast<char>(tmp);
//        }

        return login;
    } //@untested

    std::string User::to_standard_name(const std::string& name) {
        //@undone
//        std::string result = "";
//
//        unsigned short int upper_char = 0;
//        for (auto c : name) {
//            int tmp;
//            if (upper_char != 2 || isupper(c)) {
//                upper_char++;
//                tmp = c;
//            } else {
//                tmp = tolower(c); //no more than two upper symbols
//            }
//
//            result + static_cast<char>(tmp);
//        }

        return name;
    } //@untested

}
