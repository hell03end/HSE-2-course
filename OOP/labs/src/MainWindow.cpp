//#include "MainWindow.h" // <-- the fuck?!
#include "../incl/MainWindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
    ui = new Ui::MainWindow;
    ui->setupUi(this);
    m_mode = SNGL;

    connect(ui->pushButton_exit, &QPushButton::clicked,
            this, &MainWindow::close_app);
    connect(ui->pushButton_back_scores, &QPushButton::clicked,
            this, &MainWindow::go_to_main_window);
    connect(ui->pushButton_back_settings, &QPushButton::clicked,
            this, &MainWindow::go_to_main_window);
    connect(ui->pushButton_scores, &QPushButton::clicked,
            this, &MainWindow::go_to_scores);
    connect(ui->pushButton_scores, &QPushButton::clicked,
            this, &MainWindow::request_players);
    connect(ui->pushButton_settings, &QPushButton::clicked,
            this, &MainWindow::go_to_settings);
    connect(ui->pushButton_play, &QPushButton::clicked,
            this, &MainWindow::play);
    connect(ui->pushButton_about, &QPushButton::clicked,
            this, &MainWindow::call_about);
    connect(ui->pushButton_delete, &QPushButton::clicked,
            this, &MainWindow::delete_user);
    connect(ui->pushButton_logout, &QPushButton::clicked,
            this, &MainWindow::log_out);
    connect(ui->pushButton_about, &QPushButton::clicked,
            this, &MainWindow::call_about);
    connect(ui->pushButton_save, &QPushButton::clicked,
            this, &MainWindow::save_settings);
    connect(ui->pushButton_rest_score, &QPushButton::clicked,
            this, &MainWindow::rest_score);

    connect(ui->radioButton_mode_command, &QRadioButton::clicked,
            this, &MainWindow::update_game_mode);
    connect(ui->radioButton_mode_pvp, &QRadioButton::clicked,
            this, &MainWindow::update_game_mode);
    connect(ui->radioButton_mode_single, &QRadioButton::clicked,
            this, &MainWindow::update_game_mode);
    connect(ui->radioButton_mode_command_scores, &QRadioButton::clicked,
            this, &MainWindow::update_game_mode);
    connect(ui->radioButton_mode_command_scores, &QRadioButton::clicked,
            this, &MainWindow::request_players);
    connect(ui->radioButton_mode_pVp_scores, &QRadioButton::clicked,
            this, &MainWindow::update_game_mode);
    connect(ui->radioButton_mode_pVp_scores, &QRadioButton::clicked,
            this, &MainWindow::request_players);
    connect(ui->radioButton_mode_single_scores, &QRadioButton::clicked,
            this, &MainWindow::update_game_mode);
    connect(ui->radioButton_mode_single_scores, &QRadioButton::clicked,
            this, &MainWindow::request_players);

    connect(ui->checkBox_sounds, &QCheckBox::clicked,
            this, &MainWindow::button_enable_save);
    connect(ui->lineEdit_username, &QLineEdit::textChanged,
            this, &MainWindow::button_enable_save);
    connect(ui->checkBox_offline, &QCheckBox::clicked,
            this, &MainWindow::button_enable_scores);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::set_offline_settings() {
    ui->pushButton_scores->setEnabled(false);
    ui->pushButton_delete->setEnabled(false);
    ui->lineEdit_username->setEnabled(false);
    ui->checkBox_offline->setChecked(true);
    ui->checkBox_offline->setEnabled(false);
    ui->radioButton_mode_command->setEnabled(false);
    ui->radioButton_mode_pvp->setEnabled(false);
    ui->radioButton_mode_single->setEnabled(false);
    ui->label_user_greetings->setText("Hello, User!");
    ui->lineEdit_username->setText(QString::fromStdString("NoLoggedIn"));
    ui->label_score->setText(QString::fromStdString("###"));
}

void MainWindow::set_online_settings(const std::string& nickname, unsigned long score) {
    ui->pushButton_scores->setEnabled(true);
    ui->pushButton_delete->setEnabled(true);
    ui->lineEdit_username->setEnabled(true);
    ui->checkBox_offline->setChecked(false);
    ui->checkBox_offline->setEnabled(true);
    ui->radioButton_mode_command->setEnabled(true);
    ui->radioButton_mode_pvp->setEnabled(true);
    ui->radioButton_mode_single->setEnabled(true);
    ui->label_user_greetings->setText("Hello, "+QString::fromStdString(nickname)+"!");
    ui->label_your_score->setText(QString::fromStdString(nickname)+" score:");
    ui->lineEdit_username->setText(QString::fromStdString(nickname));
    ui->label_score->setText(QString::fromStdString(std::to_string(score)));
}

void MainWindow::set_geometry(const QRect& size) {
    this->setGeometry(size);
}

void MainWindow::update_score(unsigned long score) {
    ui->label_score->setText(QString::fromStdString(std::to_string(score)));
}

void MainWindow::update_nickname(const std::__cxx11::string &nickname) {
    if (nickname == "") {
        show_message(4, "Can't change nickname!", "Are you shure suggested nickname was correct?");
    } else {
        ui->lineEdit_username->setText(QString::fromStdString(nickname));
    }
}

void MainWindow::show_players(const containers::List<auth::Player>& list) {
    /* undone: sent players to output */
    ui->textEdit_scores->clear();
    for (int i = 0; i < list.size(); ++i) {
        ui->textEdit_scores->append(QString::fromStdString(list[i].get_login()));
    }
}

void MainWindow::close_app() {
    emit KillApp();
    this->close();
}

void MainWindow::show_message(int icon, const std::__cxx11::string &title,
                           const std::__cxx11::string &body,
                           const std::__cxx11::string &detals) {
    /* show error with some message */
    QMessageBox message_w;
    if (icon == 1) {
        message_w.setIcon(QMessageBox::Question);
    } else if (icon == 2) {
        message_w.setIcon(QMessageBox::Information);
    } else if (icon == 3) {
        message_w.setIcon(QMessageBox::Warning);
    } else if (icon == 4) {
        message_w.setIcon(QMessageBox::Critical);
    }
    message_w.setDefaultButton(QMessageBox::Ok); //create only one button
    message_w.setText("<b>"+QString::fromStdString(title)+"</b>"); //show title message
    if (body != "") {
        message_w.setInformativeText(QString::fromStdString(body)); //add informative text
    }
    if (detals != "") {
        message_w.setDetailedText(QString::fromStdString(detals)); //add detailed text
    }
    message_w.exec();
}

void MainWindow::set_sounds() {
    emit Sound(ui->checkBox_sounds->isChecked());
}

void MainWindow::request_players() {
    emit ShowPlayers(m_mode);
}

void MainWindow::rest_score() {
    ui->label_score->setText(QString::fromStdString(std::to_string(0)));
    emit RestScore();
}

void MainWindow::call_about() {
    emit CallAbout();
}

void MainWindow::log_out() {
    go_to_main_window();
    emit Geometry(this->geometry());
    emit LogOut();
    this->close();
}

void MainWindow::delete_user() {
    emit Geometry(this->geometry());
    emit DeleteUser();
    this->close();
}

void MainWindow::change_nickname() {
    emit ChangeNickname(ui->lineEdit_username->text().toStdString());
}

void MainWindow::save_settings() {
    set_sounds();
    change_nickname();

    show_message(2, "Changes were successfully saved!");
}

void MainWindow::play() {
    emit Play(m_mode, !ui->checkBox_offline->isChecked());
}

void MainWindow::go_to_main_window() {
    this->ui->stackedWidget->setCurrentIndex(this->ui->stackedWidget->indexOf(this->ui->page_main));
    if (ui->radioButton_mode_command_scores->isChecked()) {
        ui->radioButton_mode_command->setChecked(true);
    } else if (ui->radioButton_mode_pVp_scores->isChecked()) {
        ui->radioButton_mode_pvp->setChecked(true);
    } else {
        ui->radioButton_mode_single->setChecked(true);
    }
}

void MainWindow::go_to_scores() {
    request_players();

    this->ui->stackedWidget->setCurrentIndex(this->ui->stackedWidget->indexOf(this->ui->page_scores));
    if (ui->radioButton_mode_command->isChecked()) {
        ui->radioButton_mode_command_scores->setChecked(true);
    } else if (ui->radioButton_mode_pvp->isChecked()) {
        ui->radioButton_mode_pVp_scores->setChecked(true);
    } else {
        ui->radioButton_mode_single_scores->setChecked(true);
    }
}

void MainWindow::go_to_settings() {
    this->ui->stackedWidget->setCurrentIndex(this->ui->stackedWidget->indexOf(this->ui->page_settings));
}

void MainWindow::update_game_mode() {
    if (ui->radioButton_mode_command->isChecked()) {
        m_mode = CMND;
    } else if (ui->radioButton_mode_pvp->isChecked()) {
        m_mode = PVP;
    } else {
        m_mode = SNGL;
    }
}

void MainWindow::button_enable_save() {
    ui->pushButton_save->setEnabled(ui->lineEdit_username->text().toStdString().size() >= 3);
}

void MainWindow::button_enable_scores() {
    ui->pushButton_scores->setEnabled(!ui->checkBox_offline->isChecked());
}
