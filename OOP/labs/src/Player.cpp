//#include "Player.h" // <-- the fuck?!
#include "../incl/Player.h"

namespace auth {

    Player::Player() : User() {
        m_num_games_lost = 0;
        m_num_games_won = 0;
        m_score = 0;
        m_level = 0;
        m_nickname = DEFNNM;
    } //@tested

    Player::Player(const std::string& nickname,
                   const std::string& password,
                   const std::string& name,
                   const std::string& surname,
                   const std::string& login,
                   const std::string& key) :
            User(name, surname, password, login, key) {
        if (!set_nickname(nickname)) {
            m_nickname = DEFNNM;
            set_error(WRGDAT);
        }
        m_num_games_lost = m_num_games_won = 0;
        m_score = 0;
        m_level = 0;
    } //@tested

    Player::~Player(void) {
        /* nothing to delete */
    } //@tested

    Player::Player(const Player& player) {
        m_key = player.m_key;
        m_password = player.m_password;
        m_name = player.m_name;
        m_login = player.m_login;
        m_surname = player.m_surname;
        m_nickname = player.m_nickname;
        m_score = player.m_score;
        m_level = player.m_level;
        m_num_games_won = player.m_num_games_won;
        m_num_games_lost = player.m_num_games_lost;
    } //@tested

    bool Player::set_nickname(const std::string& nickname) {
        if (!correct_nickname(nickname)) {
            return false;
        }
        m_nickname = to_standard_login(nickname);

        return true;
    } //@tested

    std::string Player::get_nickname() const {
        return m_nickname;
    } //@tested

    void Player::set_score(const unsigned long int score) {
        m_score = score;
    } //@tested

    unsigned long int Player::get_score(void) const {
        return m_score;
    } //@tested

    unsigned long int Player::get_num_of_games(void) {
        return m_num_games_lost + m_num_games_won;
    } //@tested

    void Player::add_game_won(void) {
        ++m_num_games_won;
    } //@tested

    void Player::add_game_lost(void) {
        ++m_num_games_lost;
    } //@tested

    unsigned int Player::get_num_games_won(void) {
        return m_num_games_won;
    } //@tested

    unsigned int Player::get_num_games_lost(void) {
        return m_num_games_lost;
    } //@tested

    void Player::set_level(const unsigned short int level) {
        m_level = level;
    } //@tested

    unsigned short int Player::get_level(void) {
        return m_level;
    } //@tested

    Player& Player::operator=(const Player& player) {
        if (this != &player) {
            m_key = player.m_key;
            m_password = player.m_password;
            m_name = player.m_name;
            m_login = player.m_login;
            m_surname = player.m_surname;
            m_nickname = player.m_nickname;
            m_score = player.m_score;
            m_level = player.m_level;
            m_num_games_won = player.m_num_games_won;
            m_num_games_lost = player.m_num_games_lost;
        }

        return *this;
    } //@tested

    unsigned long int Player::operator+(unsigned long int score) {
        return m_score += score;
    } //@tested

    unsigned long int Player::operator-(unsigned long int score) {
        return m_score -= score;
    } //@tested

    unsigned long int Player::operator*(unsigned long int score) {
        return m_score *= score;
    } //@tested

    Player& Player::operator+(const Player& player) {
        m_score += player.m_score;
        return *this;
    } //@tested

    Player& Player::operator-(const Player& player) {
        m_score -= player.m_score;
        return *this;
    } //@tested

    unsigned int Player::operator++(void) {
        return ++m_num_games_won;
    } //@tested

    unsigned int Player::operator++(int tmp) {
        tmp = m_num_games_won;
        ++m_num_games_won;
        return static_cast<unsigned int>(tmp);
    } //@tested

    unsigned int Player::operator--(void) {
        return ++m_num_games_lost;
    } //@tested

    unsigned int Player::operator--(int tmp) {
        tmp = m_num_games_lost;
        ++m_num_games_lost;
        return static_cast<unsigned int>(tmp);
    } //@tested

    bool Player::operator==(const Player& player) const {
        return m_score == player.m_score;
    } //@tested

    bool Player::operator!=(const Player& player) const {
        return m_score != player.m_score;
    } //@tested

    bool Player::operator>(const Player& player) const {
        return m_score > player.m_score;
    } //@tested

    bool Player::operator>=(const Player& player) const {
        return m_score >= player.m_score;
    } //@tested

    bool Player::operator<(const Player& player) const {
        return m_score < player.m_score;
    } //@tested

    bool Player::operator<=(const Player& player) const {
        return m_score <= player.m_score;
    } //@tested

    //@undone:
    bool Player::correct_nickname(const std::string& nickname) {
        return correct_login(nickname);
    } //@tested

}
