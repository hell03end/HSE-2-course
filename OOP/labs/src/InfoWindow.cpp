//#include "InfoWindow.h" // <-- the fuck?!
#include "../incl/InfoWindow.h"
#include "ui_InfoWindow.h"

InfoWindow::InfoWindow(QWidget *parent) : QDialog(parent) {
    ui = new Ui::InfoWindow();
    ui->setupUi(this);

    connect(ui->pushButton, &QPushButton::clicked,
            this, &InfoWindow::close_app);
}

InfoWindow::~InfoWindow()
{
    delete ui;
}

void InfoWindow::close_app() {
    this->close();
}
