/* Created by Pchelkin Dmitriy Alexeevich, BIV-151, Variant 11.
 *   2 threads was created - one to add new players, second to delete them from list.
 *   As both threads plus GUI interuct with the same list, their methods lock the mutex in the controller
 *-- while executing to prevent collision. The indicator of server load is temprory made with progress bar.
 *   Actions of user processes in separate thread - main flow of the program, 'cause it's kinda hard to move
 *-- GUI from main thred of a program. All threads' actions are reported to qDebug().
 *   To communicate throw threads signals and slots are used. Then new Players creates they are syncs with files.
 * version 6.0: 03-Dec-16
 */

#include "../incl/ClientUIController.h"
#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication application(argc, argv); //Start point of application
    controllers::ClientUIController client_controller; //create controller
    return application.exec(); //return value from QApplication
}

/* Conclusion:
 * Results for all labs:
 * Lab1: List which stores Users created - begining to learn OOP, classes, incapsulation.
 *       Also learn how to create simple unit tests.
 * Lab2: GUI created - learning the basics of Qt and creating user interfaces.
 * Lab3: Player class, which is child to User created - inheritance, abstract classes, polymorphism.
 *       Working with files.
 * Lab4: Operator overloading for classes.
 * Lab5: Container remaked with use of templetes - learn, how templates work and where to use them. 
 * Lab6: Threads created to add and delete Players and show information about threm - learn about threads.
 * This project will be used as a base for mini-course-work.
 */
