#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), //создаем новый объект интерфейса пользователя
    changes(false), //устанавливаем индикатор изменений
    lastUser(NULL), //последний пользователь, изначально, пуст
    line(new Line<User*>()) //создаем пустую очередь
{
    ui->setupUi(this);

    // проверка на то, что данные введены в форму:
    QRegExp vvedeno("[a-zA-Zа-яА-Я0-9]{3,30}"); //регулярное выражение, для осуществления проверки
    ui->lineEdit_login->setValidator(new QRegExpValidator(vvedeno, this)); //устанавливаем проверку в поле
    connect(ui->lineEdit_login, SIGNAL(textChanged(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием
    ui->lineEdit_parol->setValidator(new QRegExpValidator(vvedeno, this)); //устанавливаем проверку в поле
    connect(ui->lineEdit_parol, SIGNAL(textChanged(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием
    QRegExp chisla("([0-9])*([\.])?([0-9])*"); //проверка на то, что строка является числом
    ui->lineEdit_schet->setValidator(new QRegExpValidator(chisla, this));
    connect(ui->lineEdit_schet, SIGNAL(textChanged(QString)), this, SLOT(checkAddButton()));

    printInfo(); //вывод информаци о состоянии очереди в начальный момент времени

    //соединение кнопок с действиями:
    connect(ui->action, SIGNAL(clicked(bool)), this, SLOT(on_action_triggered()));
    connect(ui->pushButton_cancel, SIGNAL(clicked(bool)), this, SLOT(on_action_triggered()));
    connect(ui->action_2, SIGNAL(clicked(bool)), this, SLOT(on_action_2_triggered()));
    connect(ui->pushButton_add, SIGNAL(clicked(bool)), this, SLOT(addUser()));
    connect(ui->pushButton_delete, SIGNAL(clicked(bool)), this, SLOT(deleteUser()));
    connect(ui->pushButton_save, SIGNAL(clicked(bool)), this, SLOT(saveToFile()));
    connect(ui->pushButton_load, SIGNAL(clicked(bool)), this, SLOT(loadFromFile()));
    connect(ui->pushButton_refresh, SIGNAL(clicked(bool)), this, SLOT(printInfo()));
}

MainWindow::~MainWindow()
{
    delete line; //удаляем очередь
    if (lastUser != NULL) {
        delete lastUser; //удаляем последнего пользователя (если он не был удален прежде)
    }
    delete ui; //удаляем объект графического интерфейса
}

void MainWindow::on_action_triggered()
{
    this->close(); //закрытие приложения
}

void MainWindow::on_action_2_triggered()
{
    this->ui->toolBox->setCurrentIndex(0); //переход во вкладку справки
}

void MainWindow::checkAddButton()
{
    // проверяет, введено ли что-то в поля ввода логина/пароля,
    // устанавливает доступность кнопки "добавить пользоватлеля"
    ui->pushButton_add->setEnabled(ui->lineEdit_login->hasAcceptableInput() &&
                                   ui->lineEdit_parol->hasAcceptableInput() &&
                                   ui->lineEdit_schet->hasAcceptableInput() &&
                                   ui->lineEdit_schet->text().toStdString().size() <= 15);
    //устанавливает кнопку "добавить пользователя" выбранной по умолчанию
    ui->pushButton_add->setDefault(ui->lineEdit_login->hasAcceptableInput() &&
                                   ui->lineEdit_parol->hasAcceptableInput() &&
                                   ui->lineEdit_schet->hasAcceptableInput() &&
                                   ui->lineEdit_schet->text().toStdString().size() <= 15);
}

void MainWindow::deleteUser()
{
    if (line->size() != 0) {
        User **user = line->pop(); //сохранение удаляемого элемента очереди и удаление из очереди
        if (line->size() == 0 && lastUser != NULL) {
            delete lastUser; //удаление последнего добавленного пользователя
            lastUser = NULL;
        }
        if (*user != NULL) {
            ui->textBrowser_oknoSostoyaniya->append("<b>\nПользователь удален</b>");
            printInfo(); //вывод состояния очереди
        }
    } else {
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
        error.setText("<b>Очередь пуста!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Невозможно произвести удаление пользователя из пустой очереди");
        error.exec(); //отображаем окно
    }
}

void MainWindow::addUser()
{
    string login = ui->lineEdit_login->text().toStdString(); //получение информации, введенной в поле логина
    string parol = ui->lineEdit_parol->text().toStdString(); //получение информации, введенной в поле пароля
    bool key = ui->lineEdit_schet->text().toStdString().empty();
    double schet = ui->lineEdit_schet->text().toDouble(); //получение информации, введенной в поле счета
    if (parol.size() < 5) {
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
        error.setText("<b>Слишком короткий пароль!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Пароль должен содержать не меньше 5 символов.");
        error.exec(); //отображаем окно
        ui->lineEdit_parol->clear(); //удаляем введеный пароль
        return; //выход
    }
    if (key) {
        //создаем нового пользователя
        User *user = new User();
        user->setLogin(login);
        user->setPassword(parol);
        lastUser = user; //сохраняем последнего пользователя
        line->push(user); //добавляем нового пользователя в конец очереди
    } else {
        //создаем нового клиента
        Client *client = new Client();
        client->setLogin(login);
        client->setPassword(parol);
        client->setBill(schet);
        lastUser = client; //сохраняем последнего клиента
        line->push(client); //добавляем нового клиента в конец очереди
    }

    ui->textBrowser_oknoSostoyaniya->append("<b>\nНовый пользователь добавлен в очередь</b>");

    //очистка полей ввода информации о пользователе:
    ui->lineEdit_login->clear();
    ui->lineEdit_parol->clear();
    ui->lineEdit_schet->clear();

    printInfo(); //вывод текущего состояния очереди
}

void MainWindow::printInfo()
{
    int size = line->size(); //получаем размер очереди
    //вывод информации:
    if (size != 0) {
        User **user = line->show(); //получаем данные о первом в очереди пользователе (без его удаления из очереди)
        if (user != NULL) /* если данные есть */ {
            ui->label->setText(QString::fromStdString(to_string(size)));
            ui->textBrowser_oknoSostoyaniya->append("Первый пользователь в очереди:");
            ui->textBrowser_oknoSostoyaniya->append(QString::fromStdString((*user)->getLogin() + " " + (*user)->getPassword() + " (" + (*user)->getPassword(true) + ")"));
            if (typeid(**user) == typeid(Client)) {
                ui->textBrowser_oknoSostoyaniya->append(QString::fromStdString("Счет клиента: " + to_string(static_cast<Client*>(*user)->getBill())));
            }
            ui->textBrowser_oknoSostoyaniya->append("Последний пользователь в очереди:");
            ui->textBrowser_oknoSostoyaniya->append(QString::fromStdString(lastUser->getLogin() + " " + lastUser->getPassword() + " (" + lastUser->getPassword(true) + ")"));
            if (typeid(*lastUser) == typeid(Client)) {
                ui->textBrowser_oknoSostoyaniya->append(QString::fromStdString("Счет клиента: " + to_string(static_cast<Client*>(lastUser)->getBill())));
            }
        } else {
            QMessageBox error; //создаем всплывающее окно ошибки
            error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
            error.setText("<b>Ошибка чтения пользовательской информации!<\b>"); //задаем сообщение окна ошибки
            error.exec(); //отображаем окно
            ui->textBrowser_oknoSostoyaniya->append("<b>Ошибка чтения пользовательской информации</b>\n");
        }
    } else {
        ui->label->setText(QString::fromStdString(to_string(size)));
        ui->textBrowser_oknoSostoyaniya->append("<b>Очередь пуста</b>\n");
    }
}

void MainWindow::saveToFile()
{
//    ofstream file("result.txt", ios::binary|ios::out);
    ofstream file("file.txt"); //открытие файла для записи
    if (file.is_open()) {
        User crypt; //создаем объект пользователя, для того, чтобы зашифровать данные
        //если файл был успешно открыт, записываем данные. Все записываемые данные шифруются:
        file << crypt(to_string(line->size())) << endl; //первой строкой записываем размер очереди
        for (int i = 0; i < line->size(); i++) {
            User* user = static_cast<User*>((*line)[i]);
            if (typeid(*user) == typeid(Client)) {
                file << "#!" << endl;
                file << crypt(to_string(static_cast<Client*>(user)->getBill())) << endl; //если хранится клиент, записываем его счет
            }
            //запись информации о пользователи в файл
            file << crypt((user)->getLogin()) << endl;
            file << (user)->getPassword() << endl;
        }
        file.close(); //закрываем файл
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Information); //добавляем значок ошибки
        error.setText("<b>Изменения успешно сохранены!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Файл с сохраненной очередью (file.txt) находится в одном каталоге с программой.");
        error.exec(); //отображаем окно
    } else {
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
        error.setText("<b>Не удается открыть файл для записи!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Возможно, создание файла в данном разделе запрещено или файл защищен.");
        error.exec(); //отображаем окно
    }
}

void MainWindow::loadFromFile()
{
    ifstream file("file.txt"); //открытие файла для записи
    if (file.is_open()) {
        //если файл был успешно открыт:
        User decrypt; //создаем объект пользователя, для того, чтобы расшифровать данные
        int n; //переменная для хранения размера считываемых данных
        string tmp; //переменная для временного хранения считанных данных
        //для обработки возможных ошибок при чтении воспользуемся обработкой исключений:
        try {
            //перед считыванием каждого поля расшифровывам его
            getline(file, tmp); //считываем первую строку файла
            n = stoi(decrypt(tmp)); //запоминаем количество считываемых данных
            for (int i = 0; i < n; i++) {
                getline(file, tmp); //считываем следующую строку
                if (tmp == "#!") {
                    Client* client = new Client();
                    getline(file, tmp); //считываем следующую строку
                    client->setBill(stod(decrypt(tmp)));
                    getline(file, tmp); //считываем следующую строку
                    client->setLogin(decrypt(tmp));
                    getline(file, tmp); //считываем следующую строку
                    client->setPassword(decrypt(tmp));
                    lastUser = client; //запоминаем последнего клиента
                    line->push(client); //добавляем считанного клиента в очередь
                } else {
                    User* user = new User();
                    user->setLogin(decrypt(tmp));
                    getline(file, tmp); //считываем следующую строку
                    user->setPassword(decrypt(tmp));
                    lastUser = user; //запоминаем последнего клиента
                    line->push(user); //добавляем считанного клиента в очередь
                }
                ui->textBrowser_oknoSostoyaniya->append("<b>Новый пользователь добавлен в очередь</b>");
                printInfo();
            }
            QMessageBox error; //создаем всплывающее окно ошибки
            error.setIcon(QMessageBox::Information); //добавляем значок ошибки
            error.setText("<b>Изменения успешно загружены!<\b>"); //задаем сообщение окна ошибки
            error.exec(); //отображаем окно
        } catch(...) {
            QMessageBox error; //создаем всплывающее окно ошибки
            error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
            error.setText("<b>Во время чтения файла возникла ошибка!<\b>"); //задаем сообщение окна ошибки
            error.exec(); //отображаем окно
        }
        file.close(); //закрываем файл
    } else {
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
        error.setText("<b>Не удается открыть файл для чтения!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Возможно, файл не существует или защищен.");
        error.exec(); //отображаем окно
    }
}

void MainWindow::autoAdd() {
    //список возможных имен для автоматически добавляемых пользователей:
    string names[] = { "Ivan III", "Peter I", "Skyline", "Andrew", "I'rat",
        "==nagibator==", "Her Morjoviy", "Sega", "Niki Minaj", "Eminem" };
    //список возможных паролей для автоматически добавляемых пользователей:
    string parols[] = { "password", "12345", "xyixyi", "pussy", "qwerty" };
    int n = rand(); //получаем случайное число
    if (n%2 == 0) /* если полученное число четное */ {
        //создаем нового пользователя
        User *user = new User();
        user->setLogin(names[n%10]); //задаем случайный логин
        user->setPassword(parols[n%5]); //задаем случайный пароль
        lastUser = user; //сохраняем последнего пользователя
        line->push(user); //добавляем нового пользователя в конец очереди
    } else {
        //создаем нового клиента
        Client *client = new Client();
        client->setLogin(names[n%10]); //задаем случайный логин
        client->setPassword(parols[n%5]); //задаем случайный пароль
        client->setBill(n); //задаем случайный счет
        lastUser = client; //сохраняем последнего клиента
        line->push(client); //добавляем нового клиента в конец очереди
    }
    ui->textBrowser_oknoSostoyaniya->append("<b>\nНовый пользователь добавлен в очередь</b>");
    printInfo(); //вывод текущего состояния очереди
}

void MainWindow::autoDelete() {
    if (line->size() != 0) {
        User **user = line->pop(); //сохранение удаляемого элемента очереди и удаление из очереди
        if (line->size() == 0 && lastUser != NULL) {
            delete lastUser; //удаление последнего добавленного пользователя
            lastUser = NULL;
        }
        if (*user != NULL) {
            ui->textBrowser_oknoSostoyaniya->append("<b>\nПользователь удален</b>");
            printInfo(); //вывод состояния очереди
        }
    } else {
        printInfo(); //выводим информацию, а не ошибку.
    }
}

void MainWindow::testsFinished() {
    QMessageBox error; //создаем всплывающее окно
    error.setIcon(QMessageBox::Information); //добавляем значок
    error.setText("<b>Тесты завершены успешно!<\b>"); //задаем сообщение окна
    error.exec(); //отображаем окно
}
