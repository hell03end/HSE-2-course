// Класс представляющий пользователя, имеет поля логина и пароля, а так же методы работы с ними.
// Имеет метод (де)шифрования пароля с помощью исключающего или, а так же оператор для шифрование произвольного сообщения.
// Переопределены необходимые методы и операторы (в том числе сравнения)
// Виртуальные методы родительского класса могуть быть переопределены для объектов дочерних классов.
// Задание пароля и имени пользователя может быть переопределено в доченрних классах.
// В классе реализовано правило трех: деструктор, конструктор копирования и оператор присваивания)

#ifndef USER_H
#define USER_H

#include <string>

using namespace std;

class User {
    //операторы ввода/вывода объявлены как дружественные => имеют доступ к приватным полям класса
    friend ostream& operator <<(ostream &out, const User &user); //оператор ввода
    friend istream& operator >>(istream &in, User &user); //оператор вывода
public:
    User(); //конструктор по умолчанию
    User(const string &login, const string &password); //конструктор инициализации
    virtual ~User(); //деструктор (удаление объекта), сделан виртуальным для обеспечения корректности удаления
    User(const User &user); //конструктор копирования
    virtual void setLogin(const string &login); //задание имени пользователя
    virtual void setPassword(const string &password); //задание пароля
    string getLogin() const; //получение имени пользователя
    string getPassword(bool decrypt = false) const; //получение зашифрованного/расшифрованного пароля
    //перегрузка операторов:
    virtual User &operator =(const User &user); //оператор присваивания
    //логические операторы, методы константные, так как не требуется изменять аттрибутов:
    virtual bool operator ==(const User &user) const;
    virtual bool operator !=(const User &user) const;
    virtual bool operator >(const User &user) const;
    virtual bool operator >=(const User &user) const;
    virtual bool operator <(const User &user) const;
    virtual bool operator <=(const User &user) const;
    virtual string operator ()(const string &str) const; //оператор, использующий объет класса как функцию (шифрует переданную строку)
//    virtual operator Client() const; //оператор приведения типов
protected: //модификатор доступа, позволяющий только классам наследникам иметь доступ к полям и методам
    string login, password; //поля данных класса
    string crypt(const string &message) const; //метод класса, шифрующий/дешифрующий строку (для паролей) методом xor
//    string decrypt(const string &message) const; //расшифровывающий метод, не нужен для шифрования методом xor
};


#endif
