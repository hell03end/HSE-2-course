// Класс представляющий клиента, наследует от пользователя поля логина и пароля, а так же методы работы с ними.
// Расширяет базовый класс полем счета и методами работы с ним. В классе определен оператор приведения к базовому классу.
// Переопределены необходимые методы и операторы (в том числе сравнения)
// Некоторые методы класса намерено сделаны виртуальными, для возможности дальнейшего переопределения.
// В классе реализовано правило трех: деструктор, конструктор копирования и оператор присваивания)

#ifndef CLIENT_H
#define CLIENT_H

#include "user.h" //подключаем родительский класс

class Client : public User /* осуществляем наследование публичных и защищенных полей и методов родительского класса */ {
    //операторы ввода/вывода объявлены как дружественные => имеют доступ к приватным полям класса
    friend ostream& operator <<(ostream &out, const Client &client); //оператор ввода
    friend istream& operator >>(istream &in, Client &client); //оператор вывода
public:
    //остальные публичные методы класса пользователь наследуются классом клиента => возможно их использование
    Client(); //конструктор по умолчинию
    Client(const string &login, const string &password, double bill); //конструктор инициализации
    virtual ~Client(); //деструктор, сделан виртуальным для обеспечения корректности удаления
    Client(const Client &client); //конструктор копирования
    void setBill(double bill); //задание счета клиента
    double getBill() const; //получение счета клиента
    //переопределяем методы родительского класса
    virtual void setLogin(const string &login); //задание имени пользователя
    virtual void setPassword(const string &password); //задание пароля
    //перегрузка операторов:
    virtual Client &operator =(const Client &client); //оператор присваивания
    //логические операторы, методы константные, так как не требуется изменять аттрибутов:
    virtual bool operator ==(const Client &client) const;
    virtual bool operator !=(const Client &client) const;
    virtual bool operator >(const Client &client) const;
    virtual bool operator >=(const Client &client) const;
    virtual bool operator <(const Client &client) const;
    virtual bool operator <=(const Client &client) const;
    //арифметические операторы:
    virtual double operator +(double num);
    virtual double operator -(double num);
    virtual double operator *(double num);
    virtual double operator /(double num);
    //операторы инкремента/декримента:
    virtual double operator ++(); //префиксная форма
    virtual double operator ++(int); //постфиксная форма
    virtual double operator --();
    virtual double operator --(int);
    virtual operator User() const; //оператор приведения типов
private:
    //также класс наследует защищенные поля класса-родителя: логин и пароль
    double bill; //счет клиента
};

#endif // CLIENT_H
