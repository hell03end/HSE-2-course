#ifndef ADDERTHREAD_H
#define ADDERTHREAD_H

#include <QThread> //подключаем библиотеку потоков Qt
#include <random> //для случайного интервала времени добавления
#include <ctime> //для задания семени генератора случайных чисел
#include <iostream>
#include <unistd.h> //для функции sleep()
#include <QDebug> //для вывода сообщений о действиях в потоке

using namespace std;

class AdderThread : public QThread
{
    Q_OBJECT //для корректной работы потоков в Qt
public:
    void run()/* основной цикл потока */ {
        qDebug() << QString("Запуск потока добавления...\n");
        srand(time(0)); //задаем семя генератора случайных чисел
        for (int i = 0; i < 1000; i++) {
            int n = rand(); //получаем случайное число
            qDebug() << QString::fromStdString("Поток добавления остановлен на " + to_string(n%15) +  "секунд \n");
            sleep(n%13); //останавливаем программу на какое-то время
            emit(addUser()); //посылаем сигнал о добавлении нового пользователя в очередь
            qDebug() << QString("Добавление нового пользователя...\n");
        }
    }
signals:
    void addUser(); //сигнал о добавлении нового пользователя в очередь
};

#endif // ADDERTHREAD_H
