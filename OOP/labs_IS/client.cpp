#include "client.h"
#include <climits>

Client::Client() : User() /* вызов конструктора по умолчанию для родительского класса */ {
    bill = 0; //задание значения счета клиента по умолчанию
}
Client::Client(const string &login, const string &password, double bill) {
    setLogin(login); //задание имени клиента с проверкой ввода
    setPassword(password); //задание пароля с проверкой ввода
    setBill(bill); //задание счета клиента с проверкой ввода
}
Client::~Client() {
    //все поля класса статичны, удалять нечего
    //перед вызовом данного деструктора вызывается деструктор базового класса
}
Client::Client(const Client &client) {
    //конструктор символичный, так как компилятор автоматически копирует статические типы данных (не указатели)
    login = client.login; //копирование имени клиента
    password = client.password; //копирование пароля
    bill = client.bill; //копирование счета клиента
}
void Client::setBill(double bill) {
    if (bill < 0 || bill > ULONG_MAX) {
        this->bill = 0; //значение счета клиента по умолчанию
    } else {
        this->bill = bill; //задаем значение счета клиента
    }
}
double Client::getBill() const {
    return bill; //возвращаем счет клиента
}
void Client::setLogin(const string &login) {
    if (login.size() < 3 && !login.empty()) /*Проверка ввода на минимальную длину*/ {
        this->login = "name"; //стандартное имя пользователя
    } else {
        this->login = login; //заданное имя
    }
}
void Client::setPassword(const string &password) {
    if (password.size() < 5 && !password.empty()) /*Проверка ввода на минимальную длину*/ {
        this->password = crypt("password"); //стандартный пароль (шифруется)
    } else {
        this->password = crypt(password); //заданный пароль (шифруется)
    }
}
Client &Client::operator=(const Client &client) {
    if (this != &client) /* проверка равенства адрессов текущего объекта и переданного */ {
        /* если адресса не равны, то объекты тоже, поэтому удаляем исходный объект и заменяем новым */
        bill = client.bill; //копируем счет клиента
        login = client.login; //копируем имя клиента
        password = client.password; //копируем пароль
    }

    return *this; //возвращаем ссылку на получившийся объект (текущий)
}
bool Client::operator ==(const Client &client) const {
    return this->login == client.login && this->password == client.password && this->bill == client.bill;
}
bool Client::operator !=(const Client &client) const {
    return !(*this == client);
}
bool Client::operator >(const Client &client) const {
    return this->login > client.login && this->password > client.password && this->bill > client.bill;
}
bool Client::operator >=(const Client &client) const {
    return this->login >= client.login && this->password >= client.password && this->bill >= client.bill;
}
bool Client::operator <(const Client &client) const {
    return this->login < client.login && this->password < client.password && this->bill < client.bill;
}
bool Client::operator <=(const Client &client) const {
    return this->login <= client.login && this->password <= client.password && this->bill <= client.bill;
}
double Client::operator +(double num) {
    bill += num;
    if (bill < 0) {
        bill = 0;
    }
    return bill;
}
double Client::operator -(double num) {
    if (num > bill) {
        bill = 0;
    } else {
        bill -= num;
    }
    return bill;
}
double Client::operator *(double num) {
    if (num < 0) {
        bill = 0;
    } else {
        bill *= num;
    }
    return bill;
}
double Client::operator /(double num) {
    if (num < 0) {
        bill = 0;
    } else {
        bill /= num;
    }
    return bill;
}
double Client::operator ++() {
    bill += 1;
    return bill;
}
double Client::operator ++(int t) {
    double t_bill = bill;
    bill += 1;
    return t_bill;
}
double Client::operator --() {
    bill -= 1;
    if (bill < 0) {
        bill = 0;
    }
    return bill;
}
double Client::operator --(int t) {
    double t_bill = bill;
    bill -= 1;
    if (bill < 0) {
        bill = 0;
    }
    return t_bill;
}
Client::operator User() const {
    User user;
    user.setLogin(this->login);
    user.setPassword(this->password);
    return user;
}
