#-------------------------------------------------
#
# Project created by QtCreator 2016-10-11T21:18:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ISlab
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp\
        user.cpp\
        client.cpp

HEADERS  += mainwindow.h\
        user.h\
        line.h \
        client.h \
        tests.h \
        adderthread.h \
        removerthread.h

FORMS    += mainwindow.ui

CONFIG   += c++11
