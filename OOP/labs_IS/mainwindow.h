// Класс содержащий логику работы графического интерфейса пользователя.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <fstream>
#include <typeinfo>
#include "client.h" //данный класс также содержит "user.h"
#include "line.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0); //конструктор по умолчанию
    ~MainWindow(); //деструктор

private:
    Ui::MainWindow *ui; //интерфейс окна
    Line<User*> *line; //очередь указателей на базовый класс
    User *lastUser; //последний добавленный в очередь пользователь
    bool changes; //индикатор изменений

private slots:
    void on_action_triggered(); //пункт меню - выйти
    void on_action_2_triggered(); //пункт меню - справка
    void checkAddButton(); //проверяет, введены ли логин и пароль, затем делает кнопку доступной
    void deleteUser(); //удаление первого полозователя из очереди
    void addUser(); //добавление пользователя в конец очереди
    void printInfo(); //вывод информации об очереди в окно вывода состояния
    void saveToFile(); //сохранение состояния очереди в файл
    void loadFromFile(); //загрузка состояния очереди из файла
    void autoAdd(); //автоматическое добавление пользователей в очередь, запущенное в отдельном потоке
    void autoDelete(); //автоматическое удаление пользователей из очереди, запущенное в отдельном потоке
    void testsFinished(); //уведомление об успешном завершении тестов, запущенных в отдельном потоке
};

#endif // MAINWINDOW_H
