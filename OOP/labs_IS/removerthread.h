#ifndef REMOVERTHREAD_H
#define REMOVERTHREAD_H

#include <QThread> //подключаем библиотеку потоков Qt
#include <random> //для случайного интервала времени добавления
#include <ctime> //для задания семени генератора случайных чисел
#include <iostream>
#include <unistd.h> //для функции sleep()
#include <QDebug> //для вывода сообщений о действиях в потоке

using namespace std;

class RemoverThread : public QThread
{
    Q_OBJECT //для корректной работы потоков в Qt
public:
    void run() /* основной цикл потока */ {
        qDebug() << QString("Запуск потока удаления...\n");
        srand(time(0)+100); //задаем семя генератора случайных чисел
        for (int i = 0; i < 1000; i++) {
            int n = rand(); //получаем случайное число
            qDebug() << QString::fromStdString("Поток удаления остановлен на " + to_string(n%13) +  "секунд \n");
            sleep(n%15); //останавливаем программу на какое-то время
            emit(remove()); //посылаем сигнал о удалении пользователя из очереди
            qDebug() << QString("Удаление пользователя...\n");
        }
    }
signals:
    void remove(); //сигнал о удалении пользователя из очереди
};

#endif // REMOVERTHREAD_H
