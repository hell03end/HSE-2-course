// Класс содержащий тесты используемых классов.

#ifndef TESTS_H
#define TESTS_H

#include <QObject>
#include <QDebug> //для вывода сообщений о проведении тестирования
#include <iostream> //ввод-вывод с++
#include <string> //для работы со строками
#include "client.h" //подключение классов пользователя и дочернего ему - клиента (для тестов)
#include "line.h" // подключение класса очереди

class Tests : public QObject
{
    Q_OBJECT
public:
    explicit Tests(int t_n = 0, QObject *parent = 0): n(t_n) {} //инициализация номеров тестов
public slots:
    //метод, запускающих работу тестов
    void Run() {
        //тестирование классов пользователя, клиента и очереди
        if (n%10 != 0) /* Тесты для объектов класса User */ {
            qDebug() << QString("Начало тестов...\n");
//            qDebug() << QString(u1.getLogin()+"\n"+u1.getPassword()+"\n"+u1.getPassword(true)+"\n");
            User* u2 = new User("hseguest", "hsepassword"); //динамический экземпляр класса
//            qDebug() << QString(u2->getLogin()+"\n"+u2->getPassword()+"\n"+u2->getPassword(true)+"\n");
            delete u2; //срабатывание деструктора
            u2 = new User(u1); //проверка конструктора копирования
            u1.setLogin("hseguest"); //изменение имени пользователя
            u1.setPassword("hsepassword"); //изменение пароля
            if (!(u1(u1("hello")) == "hello")) {
                qDebug() << QString("Ошибка шифрования.\n");
                exit(1);
            }
            u1 = *u2;
            if ((u1.getLogin() != "login" || u1.getPassword(true) != "password")) {
                qDebug() << QString("Ошибка оператора =.\n");
                exit(2);
            }
            if (!(u1 == *u2)) {
                qDebug() << QString("Ошибка оператора ==.\n");
                exit(3);
            }
        } //конец тестов класса User
        if ((n/10)%10 != 0) /* Тесты для объектов класса Client */ {
            qDebug() << QString("Начало тестов...\n");
//            qDebug() << c1.getLogin() << "\n" << c1.getPassword() << "\n" << c1.getPassword(true) << "\n" << c1.getBill() << "\n";
            Client* c2 = new Client("hseguest", "hsepassword", 10001); //динамический экземпляр класса
//            qDebug() << c2->getLogin() << "\n" << c2->getPassword() << "\n" << c2->getPassword(true) << "\n" << c2->getBill() << "\n";
            delete c2; //срабатывание деструктора
            c2 = new Client(c1); //проверка конструктора копирования
            c1.setLogin("hseguest"); //изменение имени клиента
            c1.setPassword("hsepassword"); //изменение пароля
            c1.setBill(100); //изменение номера счета клиента
            if (!(c1(c1("hello")) == "hello")) {
                qDebug() << QString("Ошибка шифрования.\n");
                exit(1);
            }
            c1 = *c2;
            if ((c1.getLogin() != "login" || c1.getPassword(true) != "password") || c1.getBill() != 0) {
                qDebug() << QString("Ошибка оператора =.\n");
                exit(2);
            }
            if (!(c1 == *c2)) {
                qDebug() << QString("Ошибка оператора ==.\n");
                exit(3);
            }
            if (c1 + 100 != 100 || c1.getBill() != 100) {
                qDebug() << QString("Ошибка оператора +.\n");
                exit(4);
            }
            if (c1++ != 100 || c1.getBill() != 101) {
                qDebug() << QString("Ошибка оператора ++.\n");
                exit(5);
            }
            u1 = (User) c1; //тестирование оператора приведение типов
        } //конец тестов класса Client
        if ((n/100)%10 != 0) /* Тесты для объектов класса Line */ {
            qDebug() << QString("Начало тестов...\n");
            if (l1.show() != NULL) /*очередь пуста*/ {
                qDebug() << QString("Ошибка при создании очереди.\n");
                exit(1);
            }
            l1.push(&u1); //добавление пользователя в очередь, последующий вывод
            l1 + &c1; //добавление клиента в очередь, последующий вывод
            if (l1.size() != 2) {
                qDebug() << QString("Ошибка при добавлении в очередь.\n");
                exit(2);
            }
            l1 + &u1; l1 + &c1; //добавление пользователя и клиента в очередь
            //проверка оператора индексации:
            if (*(l1[1]) != c1) {
                qDebug() << QString("Ошибка в операторе индексации.\n");
                exit(3);
            }
            User *u2 = *(l1.pop()); //сохранение полученного из очереди элемента (после получение элемент удаляется)
            delete u2; //удаление полученного элемента, для предотвращения утечки памяти
            u2 = *(l1--); //проверка постфиксного оператора декримента
            delete u2;
        } //конец тестов класса Line
        qDebug() << QString("Тесты пройдены успешно!\n");
        emit(finished());
    }
signals:
    void finished(); //сигнал о успешном завершении тестов
private:
    int n; //номера тестов, которые нужно выполнить
    User u1; //статический экземпляр класса User
    Client c1; //статический экземпляр класса Client
    Line<User*> l1; //создание экземпляра класса очереди (конструктор по умолчанию)
};

#endif // TESTS_H
