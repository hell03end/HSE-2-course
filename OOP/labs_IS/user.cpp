#include "user.h"

User::User() {
    login = "login"; //стандартный логин
    password = crypt("password"); //стандартный пароль (хранится в зашифрованном виде)
}
User::User(const string &login, const string &password) {
    setLogin(login); //задание имени пользователя с проверкой ввода
    setPassword(password); //задание пароля с проверкой ввода
}
User::~User() {
    //все поля класса статичны, удалять нечего
}
User::User(const User &user) {
    //конструктор символичный, так как компилятор автоматически копирует статические типы данных (не указатели)
    login = user.login; //копирование имени пользователя
    password = user.password; //копирование пароля
}
void User::setLogin(const string &login) {
    if (login.size() < 3) /*Проверка ввода на минимальную длину*/ {
        this->login = "login"; //стандартное имя пользователя
    } else {
        this->login = login; //заданное имя
    }
}
void User::setPassword(const string &password) {
    if (password.size() < 5) /*Проверка ввода на минимальную длину*/ {
        this->password = crypt("password"); //стандартный пароль (шифруется)
    } else {
        this->password = crypt(password); //заданный пароль (шифруется)
    }
}
string User::getLogin() const {
    return login; //возвращает имя пользователя (не нуждается в проверках)
}
string User::getPassword(bool decrypt) const {
    if (decrypt) {
        return crypt(password); //возвращает расшифрованный пароль
    }
    return password; //возвращает зашифрованный пароль
}
User &User::operator=(const User &user) {
    if (this != &user) /* проверка равенства адрессов текущего объекта и переданного */ {
        /* если адресса не равны, то объекты тоже, поэтому удаляем исходный объект и заменяем новым */
        login = user.login; //копируем имя клиента
        password = user.password; //копируем пароль
    }

    return *this; //возвращаем ссылку на получившийся объект (текущий)
}
bool User::operator ==(const User &user) const {
    //проверка равенства полей
    return this->login == user.login && this->password == user.password;
}
bool User::operator !=(const User &user) const {
    //оператор, обратный оператору ==
    return !(*this == user);
}
bool User::operator >(const User &user) const {
    return this->login > user.login && this->password > user.password;
}
bool User::operator >=(const User &user) const {
    return this->login >= user.login && this->password >= user.password;
}
bool User::operator <(const User &user) const {
    return this->login < user.login && this->password < user.password;
}
bool User::operator <=(const User &user) const {
    return this->login <= user.login && this->password <= user.password;
}
string User::operator ()(const string &str) const {
    return crypt(str);
}
string User::crypt(const string &message) const {
    char key = 'X'; //ключ шифрования
    string result = "";
    for (int i = 0; i < message.size(); i++) {
        result += message[i] ^ key; //обрабатывая каждую букву исходного слова в цике, добавляем ее в результат
    }
    return result; //возврат зашифрованной/дешифрованной строки
}
