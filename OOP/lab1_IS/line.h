#ifndef LINE_H
#define LINE_H

#include <iostream>

using namespace std;

template <class T> //класс шаблонный - можно хранить объекты любых типов
class Line {
public:
    Line(); //конструктор по умолчанию
    Line(int size, T array[]); //конструктор инициализации - заполняет очередь копиями элементов переданного массива
    ~Line(); //деструктор (удаление объекта)
    Line(const Line& line); //конструктор копирования
    int size(void) const; //получение размера очереди
    void push(const T &user); //добавление элемента в конец очереди
    T *pop(void); //возвращает и удаляет элемент из начала очереди
    T *show(void) const; //возвращает первый элемент
private:
    int num; //число элементов в очереди
    typedef struct element {
        element *next = NULL, *previous = NULL; //указатели на следующий и предыдущий элементы
        T *data = NULL; //узазатель на хранимые данные
    } Element; //структура элемента очереди
    Element *begin, *end; //начало и конец очереди
};

//реализация методов класса:
template <class T>
Line<T>::Line() {
    begin = end = NULL; //в пустой очереди указатели на конец и начало = 0
    num = 0; //очередь пуста
}
template <class T>
Line<T>::Line(int size, T array[]) {
    begin = end = NULL; //в пустой очереди указатели на конец и начало = 0
    num = 0; //очередь пуста
    for (int i = 0; i < size; i++) {
        push(array[i]);
    }
}
template <class T>
Line<T>::~Line() {
    Element *current = begin; //текущий элемент равен первому
    while (current != NULL) /*Проходимся по всем элементам*/ {
        begin = begin->next; //первый элемент смещается на следующий за ним
        delete current->data; //удаляем хранимые данные
        delete current; //удаляем текущий элемент
        current = begin; //текущий элемент равен первому
    }
    begin = end = NULL; //обнуляем указатели
}
template <class T>
Line<T>::Line(const Line &line) {
    Line tempLine; //создаем временную очередь, в которую будут помещены все элементы исходной в обратном порядке
    for (int i = 0; i < line.size(); i++) {
        tempLine.push(*(line.show())); //помещаем каждый элемент из начала переданной очереди в конец временной
    }
    for (int i = 0; i < line.size(); i++) {
        this->push(*(tempLine.pop())); //помещаем каждый элемент из начала временной очереди в новую очередь
    }
}
template <class T>
int Line<T>::size() const {
    return num; //возвращаем количество элементов в очереди
}
template <class T>
void Line<T>::push(const T &user) {
    if (begin == NULL) /*очередь пуста*/ {
        begin = new Element; //создаем новый элемент очереди
        end = begin; //в очереди только один элемент, поэтому указатель на начало равен указателю на конец
    } else {
        end->next = new Element; //создаем новый элемент в конце очереди очереди
        end->next->previous = end; //в новом элементе запоминаем предыдущий элемент
        end = end->next;
    }
    end->data = new T(user); //создаем новый объект User и копируем в него переданные данные
    num++; //увеличиваем количество элементов
}
template <class T>
T *Line<T>::pop() {
    if (begin == NULL) /*очередь пуста*/ {
        return NULL; //возвращает нулевой указатель при отсутствии элементов в очереди
    } else {
        T *result = begin->data; //временное хранилище содержимого первого элемента

        if (begin->next == NULL) /*элемент единственный*/ {
            delete begin; //удаляем элемент
            begin = end = NULL; //присваиваем указателям на начало и конец очереди нули
        } else {
            begin = begin->next; //смещаем указатель на начало очереди на следующий элемент
            delete begin->previous; //удаляем первый элемент очереди
            begin->previous = NULL; //обнуляем указатель на певый элемент
        }
        num--; //уменьшаем количество элементов

        return result; //возвращает копию первого элемента очереди
    }
}
template <class T>
T *Line<T>::show() const {
    if (begin == NULL) /*очередь пуста*/ {
        return NULL; //возвращает нулевой указатель при отсутствии элементов в очереди
    } else {
        T *newUser = new T(*(begin->data)); //создает копию элемента данных
        return newUser; //возвращает копию первого элемента очереди
    }
}

#endif //LINE_H
