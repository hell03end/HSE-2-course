#ifndef USER_H
#define USER_H

#include <string>

using namespace std;

class User {
public:
    User(); //конструктор по умолчанию
    User(const string &login, const string &password); //конструктор инициализации
    ~User(); //деструктор (удаление объекта)
    User(const User &user); //конструктор копирования
    void setLogin(const string &login); //задание имени пользователя
    void setPassword(const string &password); //задание пароля
    string getLogin() const; //получение имени пользователя
    string getPassword(bool decrypt = false) const; //получение зашифрованного/расшифрованного пароля
private:
    string login, password; //поля данных класса
    string crypt(const string &message) const; //метод класса, шифрующий/дешифрующий строку (для паролей) методом xor

};


#endif
