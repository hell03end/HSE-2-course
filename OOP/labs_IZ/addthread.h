#ifndef ADDTHREAD_H
#define ADDTHREAD_H

#include <QThread> // подключаем библиотеку потоков Qt
#include <random> // для случайного интервала времени добавления
#include <ctime> // для задания семени генератора случайных чисел
#include <iostream>
#include <unistd.h> // для функции sleep()
#include <QDebug> // для вывода сообщений о действиях в потоке

using namespace std;

// наследуем класс от потока Qt
class AddThread : public QThread {
    Q_OBJECT // для корректной работы потоков в Qt
public:
    void run() /* основной цикл потока */ {
        qDebug() << QString("Запуск потока добавления...");
        srand(time(0)); // задаем семя генератора случайных чисел
        for (int i = 0; i < 10; ++i) {
            int n = rand(); // получаем случайное число
            qDebug() << QString::fromStdString("Поток добавления остановлен на "+to_string(n%20)+"секунд.");
            sleep(n%20); // останавливаем поток на какое-то время
            emit addOperation(); // посылаем сигнал о добавлении новой операции
            qDebug() << QString("Добавление новой операции...");
        }
    }
signals:
    void addOperation(); // сигнал о добавлении новоq операции
};

#endif // ADDTHREAD_H
