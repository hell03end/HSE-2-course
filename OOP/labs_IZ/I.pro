QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = I
TEMPLATE = app

CONFIG   += c++1y

SOURCES += main.cpp \
        mainwindow.cpp \
        Debit.cpp \
        Credit.cpp

HEADERS  += mainwindow.h \
         Debit.h \
         Credit.h \
         btree.h \
         addthread.h \
         removethread.h

FORMS    += mainwindow.ui
