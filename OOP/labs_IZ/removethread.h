#ifndef REMOVETHREAD_H
#define REMOVETHREAD_H

#include <QThread> // подключаем библиотеку потоков Qt
#include <random> // для случайного интервала времени добавления
#include <ctime> // для задания семени генератора случайных чисел
#include <iostream>
#include <unistd.h> // для функции sleep()
#include <QDebug> // для вывода сообщений о действиях в потоке

using namespace std;

// наследуем класс от потока Qt
class RemoveThread : public QThread {
    Q_OBJECT //для корректной работы потоков в Qt
public:
    void run() /* основной цикл потока */ {
        qDebug() << QString("Запуск потока удаления...");
        srand(time(0)+100); // задаем семя генератора случайных чисел (отличающееся от семени генератора к потоке добавления)
        for (int i = 0; i < 50; ++i) {
            int n = rand(); // получаем случайное число
            qDebug() << QString::fromStdString("Поток удаления остановлен на "+to_string(n%10)+"секунд.");
            sleep(n%10); // останавливаем программу на какое-то время
            // так как удаление происходит по случайному ключу (которого может не быть в дереве), производить его приходится чаще
            emit removeOperation(); // посылаем сигнал о удалении операции
            qDebug() << QString("Удаление операции...");
        }
    }
signals:
    void removeOperation(); //сигнал о удалении операции
};

#endif // REMOVETHREAD_H
