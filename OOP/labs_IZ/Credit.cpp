#include "Credit.h"

Credit::Credit() : Debit() /* преимущество наследования - больщую часть параметов инициализирует родительский класс */ {
    m_percent = 15;
}

Credit::Credit(double bill, double income, double consumption, float percent, const std::string& sender,
               const std::string& receiver) : Debit(bill, income, consumption, sender, receiver)
/* преимущество наследования - больщую часть параметов инициализирует родительский класс */ {
    set_percent(percent);
}

Credit::~Credit() {
    // удалять нечего, так как нет использования динамически выделяемой памяти
}

Credit::Credit(const Credit& credit) {
    // копируем все параметры из полученного объекта
    m_percent = credit.m_percent;
    m_time = credit.m_time;
    m_bill = credit.m_bill;
    m_income = credit.m_income;
    m_consumption = credit.m_consumption;
    m_sender = credit.m_sender;
    m_receiver = credit.m_receiver;
}

void Credit::set_percent(float percent) {
    if (((percent > 0) ? percent : -percent) <= 100) {
        m_percent = (percent > 0) ? percent : -percent; // значение процента всегда положительно
    } else {
        m_percent = 15; // присваиваем стандартное значение, если значение процента некорректно.
    }
}

float Credit::get_percent() const {
    return m_percent;
}

bool Credit::operator==(const Credit& credit) const {
    return m_bill == credit.m_bill && m_percent == credit.m_percent;
}

bool Credit::operator>(const Credit& credit) const {
    return m_bill > credit.m_bill && m_percent > credit.m_percent;
}

bool Credit::operator>=(const Credit& credit) const {
    return m_bill >= credit.m_bill && m_percent >= credit.m_percent;
}

const Credit& Credit::operator=(const Credit& credit) {
    if (this != &credit) /* если объекты не равны */ {
        // копируем все параметры из полученного объекта
        m_percent = credit.m_percent;
        m_time = credit.m_time;
        m_bill = credit.m_bill;
        m_income = credit.m_income;
        m_consumption = credit.m_consumption;
        m_sender = credit.m_sender;
        m_receiver = credit.m_receiver;
    }

    return *this; // возвращаем ссылку на текущий объект
}

Credit& Credit::operator=(const Debit& debit) {
    if (this != &debit) /* если объекты не равны */ {
        // копируем все параметры из полученного объекта
        m_percent = 15; // так как родительский объект не имеет поля с процентами, инициализируем значением по умолчанию
        m_time = debit.get_time();
        m_bill = debit.get_bill();
        m_income = debit.get_income();
        m_consumption = debit.get_consumption();
        m_sender = debit.get_sender();
        m_receiver = debit.get_receiver();
    }

    return *this; // возвращаем ссылку на текущий объект
}

void Credit::operator()(double bill, double income, double consumption, float percent, const std::string& sender,
                        const std::string& receiver) {
    // преимущество наследования - больщую часть параметов инициализирует родительский класс:
    Debit::operator()(bill, income, consumption, sender, receiver);
    set_percent(percent);
}

//void void Credit::operator()(double bill, double income, double consumption, const std::string& sender,
//                             const std::string& receiver) {
//    Debit::operator()(bill, income, consumption, sender, receiver);
//    set_percent(15);
//}

Credit::operator Debit() const {
    Debit debit(this->m_bill, this->m_income, this->m_consumption, m_sender, m_receiver); // создаем новый объект родительского типа
    return debit; // возвращаем его
}
