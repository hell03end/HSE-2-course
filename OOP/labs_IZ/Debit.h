#ifndef TEMPCLASSES_DEBIT_H
#define TEMPCLASSES_DEBIT_H

#include <ctime>
#include <string>
#include <iosfwd> // для использования ostream/istream

// Класс банковской операции
// В классе реализованно правило трех - переопределены конструктор копирования, деструктор и оператор присваивания для корректной работы с памятью
// Больщинство методов объявленны виртуальными для того, чтобы была возможность переопределять их в дочерних классах при необходимости
// Для класса переопределены многие операторы в том чилсе операторы потокового вывода/ ввода

// Интерфейс класса (описание методов и полей):
class Debit {
public:
    //операторы ввода/вывода объявлены как дружественные => имеют доступ к приватным полям класса
    friend std::ostream& operator <<(std::ostream &out, const Debit &client); //оператор ввода
    friend std::istream& operator >>(std::istream &in, Debit &client); //оператор вывода
public:
    Debit(); // конструктор по умолчанию
    Debit(double bill, double income, double consumption, const std::string& sender = "Mr. Slim", const std::string& receiver = "Dr. Dre"); // конструктор инициализации
    virtual ~Debit(); // деструктор, объявлен виртуальным для корректного удаления дочерних классов
    Debit(const Debit& debit); // конструктор копирования

    // интеллектуальные модификаторы (ограничивают данные, возможные для воода в поля класса):
    virtual void force_set_time(time_t); // метод для задания времени (обычно, время задается автоматически)
    virtual void set_bill(double);
    virtual void set_income(double);
    virtual void set_consumption(double);
    virtual void set_sender(const std::string&);
    virtual void set_receiver(const std::string&);

    // селекторы - не изменяют полей класса, поэтому константны:
    virtual time_t get_time() const;
    virtual double get_bill() const;
    virtual double get_income() const;
    virtual double get_consumption() const;
    virtual std::string get_sender() const;
    virtual std::string get_receiver() const;

    virtual double operator +(double); // добавляет переданное число к счету
    virtual double operator -(double); // вычитает переданное число из счета
    virtual double operator *(double); // умножает счет на переданное число (всегда положительное)
    virtual double operator /(double); // дели счет на переданное число (всегда положительное)
    virtual double operator %(double) const; // возвращает процент от счета
    virtual double operator ^(unsigned int); // возводит счет в степень числа (положительного)

    virtual double operator ++(); // добавляет 100 к счету
    virtual double operator ++(int); // добавляет 100 к счету, возвращает прежднее значение счета
    virtual double operator --(); // удаляет 100 из счета
    virtual double operator --(int); // удаляет 100 из счета, возвращает прежднее значение счета

    // логические операторы (сравнения), не изменяют полей класса, поэтому объявленны константными:
    // сравнение ведется по счету (сумме)
    virtual bool operator ==(const Debit& debit) const;
    virtual bool operator !=(const Debit& debit) const;
    virtual bool operator >(const Debit& debit) const;
    virtual bool operator >=(const Debit& debit) const;
    virtual bool operator <(const Debit& debit) const;
    virtual bool operator <=(const Debit& debit) const;

    virtual Debit& operator =(const Debit& debit); // оператор присвоения
    // оператор () служит для инициализации объекта, как конструктор инициализации
    virtual void operator ()(double bill, double income, double consumption, const std::string& sender = "Mr. Slim", const std::string& receiver = "Dr. Dre");

protected: //для того, чтобы было возможно наследование полей
    time_t m_time; // для хранение даты и времени
    double m_bill; // для хранения счета
    double m_income; // для хранения дохода
    double m_consumption; // для хранения расхода
    std::string m_sender; // для хранения отправителя
    std::string m_receiver; // для хранения получателя

    // метод очень короткий, поэтому объявлен inline, что значит, что компилятор может вместо вызова функции подставить ее тело в нужное место
    inline void set_time(); // метод для автоматического определения и задания времени
};

#endif //TEMPCLASSES_DEBIT_H
