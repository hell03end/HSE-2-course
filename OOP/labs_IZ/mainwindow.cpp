#include "mainwindow.h"
#include "ui_mainwindow.h"

// операторы потокового вывода для классов банковских операций:
std::ostream& operator <<(std::ostream &out, const Debit &debit) {
    out << "%%%" << std::endl; // ключ класса родителя для записи в файл
    out << debit.m_sender << std::endl;
    out << debit.m_receiver << std::endl;
    out << debit.m_bill << std::endl;
    out << debit.m_income << std::endl;
    out << debit.m_consumption << std::endl;
    out << debit.m_time << std::endl;
}
std::ostream& operator <<(std::ostream &out, const Credit &credit) {
    out << "###" << std::endl; // ключ дочернего класса для записи в файл
    out << credit.m_sender << std::endl;
    out << credit.m_receiver << std::endl;
    out << credit.m_bill << std::endl;
    out << credit.m_income << std::endl;
    out << credit.m_consumption << std::endl;
    out << credit.m_time << std::endl;
    out << credit.m_percent << std::endl;
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), m_operation(nullptr),
    ui(new Ui::MainWindow) /* создаем новый объект интерфейса пользователя */ {
    ui->setupUi(this);

    // проверка на то, что данные введены в форму:
    QRegExp text("[a-zA-Zа-яА-Я0-9.-_ ]{2,30}"); //регулярное выражение, для осуществления проверки
    QRegExp key("[a-zA-Zа-яА-Я0-9.-_ ]{1,30}"); //регулярное выражение, для осуществления проверки
    QRegExp numbers("([0-9])*([\.])?([0-9])*"); //проверка на то, что строка является числом
    ui->lineEdit_new_key->setValidator(new QRegExpValidator(key, this)); //устанавливаем проверку в поле
    ui->lineEdit_sender->setValidator(new QRegExpValidator(text, this)); //устанавливаем проверку в поле
    ui->lineEdit_receiver->setValidator(new QRegExpValidator(text, this)); //устанавливаем проверку в поле
    ui->lineEdit_bill->setValidator(new QRegExpValidator(numbers, this)); //устанавливаем проверку в поле
    connect(ui->lineEdit_new_key, SIGNAL(textEdited(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием
    connect(ui->lineEdit_sender, SIGNAL(textEdited(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием
    connect(ui->lineEdit_receiver, SIGNAL(textEdited(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием
    connect(ui->lineEdit_bill, SIGNAL(textEdited(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием
    connect(ui->lineEdit_new_key, SIGNAL(textChanged(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием
    connect(ui->lineEdit_sender, SIGNAL(textChanged(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием
    connect(ui->lineEdit_receiver, SIGNAL(textChanged(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием
    connect(ui->lineEdit_bill, SIGNAL(textChanged(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием

    connect(ui->checkBox, SIGNAL(clicked(bool)), this, SLOT(checkCredit())); //связываем проверку с действием
    connect(ui->lineEdit_key, SIGNAL(textChanged(QString)), this, SLOT(checkNavigation())); //связываем проверку с действием

    //соединение кнопок с действиями:
    connect(ui->pushButton_add, SIGNAL(clicked(bool)), this, SLOT(addOperation()));
    connect(ui->pushButton_delete, SIGNAL(clicked(bool)), this, SLOT(deleteOperation()));
    connect(ui->pushButton_find, SIGNAL(clicked(bool)), this, SLOT(findOperation()));
    connect(ui->pushButton_save, SIGNAL(clicked(bool)), this, SLOT(saveToFile()));
    connect(ui->pushButton_load, SIGNAL(clicked(bool)), this, SLOT(loadFromFile()));

    // соединение сигнала о размере хранилища со строкой прогресса (индикатором заполненности)
    connect(this, SIGNAL(Size(int)), ui->progressBar, SLOT(setValue(int)));
}

MainWindow::~MainWindow() {
    if (m_operation != nullptr) {
        delete m_operation; //удаляем последнюю операцию (если она не была удалена прежде)
    }
    delete ui; //удаляем объект графического интерфейса
}

void MainWindow::printInfo(bool clear, const std::string& case1, const std::string& case2) {
    // вывод информации об операции:
    if (clear) {
        ui->textBrowser->clear(); // очищаем текстовое поле для вывода
    }
    if (m_operation != nullptr) /* если операция существует*/ {
        m_mutex.lock(); // блокируем ресурсы
        // выводим заголовок в зависимости от типа хранимой операции
        if (typeid(*m_operation) == typeid(Debit)) {
            ui->textBrowser->append(QString::fromStdString(case1));
        } else {
            ui->textBrowser->append(QString::fromStdString(case2));
        }
        ui->textBrowser->append(QString::fromStdString("От: "+m_operation->get_sender()+", кому: "+m_operation->get_receiver()+"\n"));
        ui->textBrowser->append(QString::fromStdString("Текущий счет: "+std::to_string(m_operation->get_bill())+"\n"));
        ui->textBrowser->append(QString::fromStdString("Доход: "+std::to_string(m_operation->get_income())+"\n"));
        ui->textBrowser->append(QString::fromStdString("Расход: "+std::to_string(m_operation->get_consumption())+"\n"));
        // если операция дочернего типа - выводим дополнительное поле
        if (typeid(*m_operation) == typeid(Credit)) {
            ui->textBrowser->append(QString::fromStdString("Процент: "+std::to_string(static_cast<Credit*>(m_operation)->get_percent())+"\n"));
        }
        // выводим время... (Qt не захотел выводить время обычным ctime(time_t), решение взято с "http://cplusplus.com/reference/ctime/". Спросить есть ли другое
        time_t rawtime;
        struct tm* timeinfo;
        char buffer [80];
        time(&rawtime);
        timeinfo = localtime(&rawtime);
        strftime (buffer, 80, "%c",timeinfo);
        std::string* temp = new std::string(buffer); // преобразовываем массив символов в строку
        ui->textBrowser->append(QString::fromStdString(*temp));
        ui->textBrowser->append(QString::fromStdString("\n"));
        delete temp;
        m_mutex.unlock();
    }
    emit Size(m_container.size()); // посылаем сигнал об изменении индикации хранилища
}

void MainWindow::checkAddButton() {
    // проверяет, введены ли корректные данные для выполнения платежа.
    // устанавливает доступность кнопки
    ui->pushButton_add->setEnabled(ui->lineEdit_sender->hasAcceptableInput() &&
                                   ui->lineEdit_receiver->hasAcceptableInput() &&
                                   ui->lineEdit_new_key->hasAcceptableInput() &&
                                   ui->lineEdit_bill->hasAcceptableInput() &&
                                   ui->lineEdit_bill->text().toStdString().size() <= 15);
    //устанавливает кнопку "добавить пользователя" выбранной по умолчанию
    ui->pushButton_add->setDefault(ui->lineEdit_sender->hasAcceptableInput() &&
                                   ui->lineEdit_receiver->hasAcceptableInput() &&
                                   ui->lineEdit_new_key->hasAcceptableInput() &&
                                   ui->lineEdit_bill->hasAcceptableInput() &&
                                   ui->lineEdit_bill->text().toStdString().size() <= 15);
}

void MainWindow::checkCredit() {
    // устанавливает элементы для выбора процента кредитования активными, если поставленна галочка
    ui->label_percent->setEnabled(ui->checkBox->isChecked());
    ui->spinBox->setEnabled(ui->checkBox->isChecked());
}

void MainWindow::checkNavigation() {
    // активирует кнопки найти и удалить, если в поле с ключем введено корректное значение
    ui->pushButton_find->setEnabled(ui->lineEdit_key->hasAcceptableInput() && ui->lineEdit_key->text().toStdString().size() > 0);
    ui->pushButton_delete->setEnabled(ui->lineEdit_key->hasAcceptableInput() && ui->lineEdit_key->text().toStdString().size() > 0);
}

void MainWindow::addOperation() {
    if (m_container.size() < 100) {
        // если хранилище не переполненно:
        std::string key = ui->lineEdit_new_key->text().toStdString();
        m_mutex.lock(); //блокируем ресурсы
        if (!ui->checkBox->isChecked()) {
            // если проценты кредитования не выбраны, значит объект родительского класса
            m_operation = new Debit(); // создаем новый объект и заполняем его введенными значениями
            m_operation->set_bill(ui->lineEdit_bill->text().toDouble());
            m_operation->set_sender(ui->lineEdit_sender->text().toStdString());
            m_operation->set_receiver(ui->lineEdit_receiver->text().toStdString());
        } else {
            m_operation = new Credit(); // создаем новый объект и заполняем его введенными значениями
            m_operation->set_bill(ui->lineEdit_bill->text().toDouble());
            m_operation->set_sender(ui->lineEdit_sender->text().toStdString());
            m_operation->set_receiver(ui->lineEdit_receiver->text().toStdString());
            static_cast<Credit*>(m_operation)->set_percent(ui->spinBox->value());
        }
        m_container.add(key, m_operation); // добавляем созданный объект в контейнер
        m_mutex.unlock(); // разблокируем ресурсы
        printInfo(true, "<b>Произведен новый перевод:</b>\n", "<b>Произведен новый перевод с кредитованием:</b>\n");
        // очистка полей:
        ui->lineEdit_sender->clear();
        ui->lineEdit_receiver->clear();
        ui->lineEdit_new_key->clear();
        ui->lineEdit_bill->clear();
        ui->spinBox->setValue(5);
        ui->checkBox->setChecked(false);
    } else {
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
        error.setText("<b>Хранилище переполненно!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Попробуйте в другой раз."); // задаем дополнительную информацию
        error.exec(); //отображаем окно
    }
}

void MainWindow::findOperation() {
    try {
        m_mutex.lock(); // блокируем ресурсы
        // ищем операцию с заданным ключем
        m_operation = m_container[ui->lineEdit_key->text().toStdString()];
        m_mutex.unlock(); // разблокируем ресурсы
        printInfo(true, "<b>Операция:</b>\n", "<b>Операция с кредитованием:</b>\n");
    } catch (...) {
        m_mutex.unlock(); // разблокируем ресурсы
        QMessageBox window; // создаем всплывающее окно ошибки
        window.setIcon(QMessageBox::Critical); // добавляем значок ошибки
        window.setText("<b>Не удалось найти операцию с заданным ключем!<\b>"); // задаем сообщение окна ошибки
        window.exec(); // отображаем окно
        // очистка полей:
        ui->textBrowser->clear();
        ui->lineEdit_key->clear();
    }
    ui->lineEdit_key->clear(); // очищаем поле ввода ключа
}

void MainWindow::deleteOperation() {
    try {
        m_mutex.lock(); // блокируем ресурсы на время выполнения метода
        // находим объект с заданным значением:
        m_operation = m_container[ui->lineEdit_key->text().toStdString()];
        m_mutex.unlock(); // разблокируем ресурсы для выполнения следующего метода:
        printInfo(true, "<b>Операция:</b>\n", "<b>Операция с кредитованием:</b>\n");
        m_mutex.lock(); // блокируем ресурсы
        if (m_container.del(ui->lineEdit_key->text().toStdString())) {
            // если удаление прошло успешно
            delete m_operation; // удаляем операцию
            m_operation = nullptr; // забываем указатель
            m_mutex.unlock(); // разблокируем ресурсы
            emit Size(m_container.size()); // изменяем индекатор заполенности
            ui->textBrowser->append(QString::fromStdString("<b>Операция удаленна!</b>\n"));
            QMessageBox window; // создаем всплывающее окно
            window.setIcon(QMessageBox::Information); // добавляем значок информации
            window.setText("<b>Успешно удалено!<\b>"); // задаем сообщение окна
            window.exec(); // отображаем окно
        } else {
            m_mutex.unlock(); // разблокируем ресурсы
            ui->textBrowser->append(QString::fromStdString("<b>Проблемы с удалением.</b>\n"));
            QMessageBox window; // создаем всплывающее окно
            window.setIcon(QMessageBox::Critical); // добавляем значок ошибки
            window.setText("<b>Невозможно произвести удаление!<\b>"); // задаем сообщение окна
            window.exec(); // отображаем окно
        }
    } catch (...) {
        m_mutex.unlock(); // отображаем окно
        QMessageBox window; // создаем всплывающее окно ошибки
        window.setIcon(QMessageBox::Critical); // добавляем значок ошибки
        window.setText("<b>Не удалось найти операцию с заданным ключем!<\b>"); // задаем сообщение окна ошибки
        window.exec(); // отображаем окно
        // очистка полей:
        ui->textBrowser->clear();
        ui->lineEdit_key->clear();
    }
    ui->lineEdit_key->clear(); // очистка поля ввода ключа
}

void MainWindow::saveToFile() {
    std::ofstream file("file.txt"); //открытие файла для записи
    if (file.is_open()) {
        //если файл был успешно открыт, записываем данные
        m_mutex.lock(); // блокируем ресурсы на время записи в файл
        file << std::to_string(m_container.size()) << std::endl; //первой строкой записываем размер
        std::list<std::string> lst = m_container.keys(); // получаем список ключей дерева
        for (auto s : lst) {
            // для каждого элемента полученного списка (для каждого ключа)
            file << s << std::endl; // записываем в файл ключ
            // записываем в файл объект из хранилища, в зависимости от его типа
            if (typeid(*(m_container[s])) == typeid(Debit)) {
                file << *(static_cast<Debit*>(m_container[s]));
            } else {
                file << *(static_cast<Credit*>(m_container[s]));
            }
        }
        m_mutex.unlock(); // разблокируем ресурсы
        file.close(); //закрываем файл
        QMessageBox error; //создаем всплывающее окно
        error.setIcon(QMessageBox::Information); //добавляем значок
        error.setText("<b>Изменения успешно сохранены!<\b>"); //задаем сообщение окна
        error.setInformativeText("Файл с сохраненными данными (file.txt) находится в одном каталоге с программой.");
        error.exec(); //отображаем окно
    } else {
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
        error.setText("<b>Не удается открыть файл для записи!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Возможно, создание файла в данном разделе запрещено или файл защищен.");
        error.exec(); //отображаем окно
    }
}

void MainWindow::loadFromFile() {
    std::ifstream file("file.txt"); //открытие файла для чтения
    if (file.is_open()) {
        //если файл был успешно открыт:
        int n; //переменная для хранения размера считываемых данных
        std::string tmp; //переменная для временного хранения считанных данных
        //для обработки возможных ошибок при чтении воспользуемся обработкой исключений:
        ui->textBrowser->clear();
        try {
            std::getline(file, tmp); // считываем первую строку файла
            n = std::stoi(tmp); // запоминаем количество данных для считывания
            for (int i = 0; i < n; i++) {
                std::string key; // для хранения ключа операции
                std::getline(file, key); // считываем ключ операции
                std::getline(file, tmp); //считываем следующую строку
                if (tmp == "%%%") {
                    // считывание родительского объекта
                    std::string sender, receiver;
                    double bill, income, consumption;
                    time_t t;
                    std::getline(file, sender);
                    std::getline(file, receiver);
                    std::getline(file, tmp);
                    bill = std::stod(tmp);
                    std::getline(file, tmp);
                    income = std::stod(tmp);
                    std::getline(file, tmp);
                    consumption = std::stod(tmp);
                    std::getline(file, tmp);
                    t = std::stoul(tmp);
                    m_mutex.lock(); // блокируем ресурсы
                    // запоминаем считанную операцию:
                    m_operation = new Debit(bill, income, consumption, sender, receiver);
                    m_operation->force_set_time(t); // изменяем время
                } else if (tmp == "###") {
                    // считывание дочернего объекта
                    std::string sender, receiver;
                    double bill, income, consumption, percent;
                    time_t t;
                    std::getline(file, sender);
                    std::getline(file, receiver);
                    std::getline(file, tmp);
                    bill = std::stod(tmp);
                    std::getline(file, tmp);
                    income = std::stod(tmp);
                    std::getline(file, tmp);
                    consumption = std::stod(tmp);
                    std::getline(file, tmp);
                    t = std::stoul(tmp);
                    std::getline(file, tmp);
                    percent = std::stod(tmp);
                    m_mutex.lock(); // блокируем ресурсы
                    // запоминаем считанную операцию:
                    m_operation = new Credit(bill, income, consumption, percent, sender, receiver);
                    m_operation->force_set_time(t); // изменяем время
                } else {
                    // если данные повреждены - сообщеаем пользователю через исключение
                    throw static_cast<std::string>("error");
                }
                m_container.add(key, m_operation);
                m_mutex.unlock(); // разблокирование ресурсов
                printInfo(); // вывод информации о загруженной операции
            }
            QMessageBox error; //создаем всплывающее окно
            error.setIcon(QMessageBox::Information); //добавляем значок
            error.setText("<b>Изменения успешно загружены!<\b>"); //задаем сообщение окна
            error.exec(); //отображаем окно
        } catch(...) {
            QMessageBox error; //создаем всплывающее окно ошибки
            error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
            error.setText("<b>Во время чтения файла возникла ошибка!<\b>"); //задаем сообщение окна ошибки
            error.exec(); //отображаем окно
        }
        file.close(); //закрываем файл
    } else {
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
        error.setText("<b>Не удается открыть файл для чтения!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Возможно, файл не существует или защищен.");
        error.exec(); //отображаем окно
    }
}

void MainWindow::autoAdd() {
    if (m_container.size() < 100) {
        // список возможных имен для автоматического добавления:
        std::string senders[] = { "Jake", "Peter I", "Skyline", "Andrew", "Artem",
            "admin", "Ivan III", "Sega", "Niki Minaj", "Eminem" };
        std::string receivers[] = { "Dr. Dre", "Mr. Slim", "Morison", "Simpson", "Finn" };
        std::string keys[] = {"q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m"};
        int n = rand(); //получаем случайное число
        if (n%2 == 0) /* если полученное число четное */ {
            // создаем новую родительскую операцию:
            Debit* debit = new Debit((double) n/10, (double) n/100, (double) n/90, senders[n%10], receivers[n%5]);
            m_mutex.lock(); // блокируем ресурсы
            m_operation = debit; //сохраняем последнюю операцию
        } else {
            // создаем новую дочернюю операцию:
            Credit* credit = new Credit((double) n/10, (double) n/100, (double) n/90, 5+n%46, senders[n%10], receivers[n%5]);
            m_mutex.lock(); // блокируем ресурсы
            m_operation = credit; //сохраняем последнюю операцию
        }
        if (m_container.add(keys[n%26], m_operation)) {
            qDebug() << QString::fromStdString("Новая операция добавленна.");
        } else {
            qDebug() << QString::fromStdString("Ошибка при добавлении!");
        }
        m_mutex.unlock(); // разблокируем ресурсы
        printInfo(true, "<b>Произведен новый перевод:</b>\n", "<b>Произведен новый перевод с кредитованием:</b>\n");
    } else {
        qDebug() << QString::fromStdString("Хранилище переполненно.");
    }
}

void MainWindow::autoDelete() {
    // список возможных ключей для автоматического удаления:
    std::string keys[] = {"q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m"};
    if (m_container.size() != 0) {
        int n = rand(); // получаем случайное число
        try {
            m_mutex.lock(); // блокируем ресурсы на время выполнения операции
            m_operation = m_container[keys[n%26]]; // находим операцию по заданному ключу
            m_mutex.unlock(); // разблокируем ресурсы для выполнения следующего метода:
            printInfo(true, "<b>Попытка удалить операцию...</b>\n", "<b>Попытка удалить операцию...</b>\n"); //вывод состояния очереди
            m_mutex.lock(); // снова блокируем ресурсы
            if (m_container.del(keys[n%26])) {
                // если удаление прошло успешно:
                delete m_operation; // удаляем последнюю операцию
                m_operation = nullptr; // забываем указатель на нее
                m_mutex.unlock(); // разблокируем ресурсы
                emit Size(m_container.size()); // изменяем индикатор размера хранилища
                qDebug() << QString::fromStdString("Операция удалена.");
                ui->textBrowser->append(QString::fromStdString("<b>Операция удаленна!</b>\n"));
            } else {
                m_mutex.unlock(); // разблокируем ресурсы
                qDebug() << QString::fromStdString("<b>Проблемы с удалением.</b>\n");
                ui->textBrowser->append(QString::fromStdString("При удалении возникли неполадки...\n"));
            }
        } catch (...) {
            m_mutex.unlock(); // разблокируем ресурсы
            qDebug() << QString::fromStdString("Отсутствие ключа удаления.");
        }
    } else {
        qDebug() << QString::fromStdString("Хранилище пусто.");
    }
}
