#include "Debit.h"

// Реализация описанных в интерфейсе методов:

Debit::Debit() {
    // задаем значения по умолчанию для полей класса
    set_time(); // время выполения операции
    m_bill = 0;
    m_income = 0;
    m_consumption = 0;
    m_sender = "Mr. Slim";
    m_receiver = "Dr. Dre";
}

Debit::Debit(double bill, double income, double consumption, const std::string& sender, const std::string& receiver) {
    // инициализируем все поля класса переданными значениями через интеллектуальные модификаторы
    set_time(); // время выполнения операции
    set_bill(bill);
    set_income(income);
    set_consumption(consumption);
    set_sender(sender);
    set_receiver(receiver);
}

Debit::~Debit() {
    // удалять нечего, так как нет использования динамически выделяемой памяти
}

Debit::Debit(const Debit& debit) {
    // копируем все параметры из полученного объекта
    m_time = debit.m_time;
    m_bill = debit.m_bill;
    m_income = debit.m_income;
    m_consumption = debit.m_consumption;
    m_sender = debit.m_sender;
    m_receiver = debit.m_receiver;
}

void Debit::force_set_time(time_t t) {
    m_time = t;
}

void Debit::set_bill(double bill) {
    set_time(); // время выполнения операции
    if (m_bill < bill) {
        m_income = bill - m_bill; // записываем разницу в доход
    } else {
        m_consumption = bill - m_bill; // записываем разницу в расход
    }
    m_bill = bill;
}

void Debit::set_income(double income) {
    m_income = (income >= 0) ? income : -income; // доход всегда положительное число (или 0)
}

void Debit::set_consumption(double consumption) {
    m_consumption = (consumption <= 0) ? consumption : -consumption; // расход всегда отрицательное число (или 0)
}

void Debit::set_sender(const std::string& sender) {
    m_sender = (sender.size() > 2) ? sender : "Mr. Slim"; // если длина переданного имени меньше 2х, задаем имя по-умолчанию
}

void Debit::set_receiver(const std::string& receiver) {
    m_receiver = (receiver.size() > 2) ? receiver : "Dr. Dre"; // если длина переданного имени меньше 2х, задаем имя по-умолчанию
}

time_t Debit::get_time() const {
    return m_time;
}

double Debit::get_bill() const {
    return m_bill;
}

double Debit::get_income() const {
    return m_income;
}

double Debit::get_consumption() const {
    return m_consumption;
}

std::string Debit::get_sender() const {
    return m_sender;
}

std::string Debit::get_receiver() const {
    return m_receiver;
}

double Debit::operator+(double n) {
    set_bill(m_bill + n); // устанавливаем новое значение счета
    return m_bill;
}

double Debit::operator-(double n) {
    set_bill(m_bill + n); // устанавливаем новое значение счета
    return m_bill;
}

double Debit::operator*(double n) {
    set_bill(m_bill * ((n >= 0) ? n : -n)); // умножаме только на положительное число. Устанавливаем новое значение счета
    return m_bill;
}

double Debit::operator/(double n) {
    set_bill(m_bill / ((n >= 0) ? n : -n)); // делим только на положительное число. Устанавливаем новое значение счета
    return m_bill;
}

double Debit::operator%(double n) const {
    return ((n >= 0) ? n : -n)*m_bill/100; // получаем процент от переданного числа (положительного)
}

double Debit::operator^(unsigned int n) {
    double t = m_bill;
    // возводим счет в степень (только положительную):
    for (int i = 0; i < n; ++i) {
        set_bill(m_bill*t);
    }
    return m_bill;
}

double Debit::operator++() {
    set_bill(m_bill + 100); // изменяем счет
    return m_bill;
}

double Debit::operator++(int) {
    double t = m_bill; // запоминаем преждний счет
    set_bill(m_bill + 100); // изменяем текущий счет
    return t;
}

double Debit::operator--() {
    set_bill(m_bill - 100); // изменяем счет
    return m_bill;
}

double Debit::operator--(int) {
    double t = m_bill; // запоминаем преждний счет
    set_bill(m_bill - 100); // изменяем текущий счет
    return t; // возвращаем преждний счет
}

bool Debit::operator==(const Debit& debit) const {
    return m_bill == debit.m_bill;
}

bool Debit::operator!=(const Debit& debit) const {
    return *this == debit;
}

bool Debit::operator>(const Debit& debit) const {
    return m_bill > debit.m_bill;
}

bool Debit::operator>=(const Debit& debit) const {
    return m_bill >= debit.m_bill;
}

bool Debit::operator<(const Debit& debit) const {
    return !(*this >= debit);
}

bool Debit::operator<=(const Debit& debit) const {
    return !(*this > debit);
}

Debit& Debit::operator=(const Debit& debit) {
    if (this != &debit) /* если объекты не равны */ {
        // копируем все параметры из полученного объекта
        m_time = debit.m_time;
        m_bill = debit.m_bill;
        m_income = debit.m_income;
        m_consumption = debit.m_consumption;
        m_sender = debit.m_sender;
        m_receiver = debit.m_receiver;
    }

    return *this; // возвращаем ссылку на текущий объект
}

void Debit::operator()(double bill, double income, double consumption, const std::string& sender, const std::string& receiver) {
    // инициализируем все поля класса переданными значениями через интеллектуальные модификаторы
    set_time();
    set_bill(bill);
    set_income(income);
    set_consumption(consumption);
    set_sender(sender);
    set_receiver(receiver);
}

void Debit::set_time() {
    m_time = time(NULL); // получаем текущее время
}
