#ifndef BTREE_H
#define BTREE_H

#include <mutex> // для использования мъютекса из стандартной библиотеки (класс может не зависить от Qt)
#include <list> // для использования списка из стандартной библиотеки

// Класс бинарного дерева с доступом по ключу
// В классе реализованно правило трех - переопределены конструктор копирования, деструктор и оператор присваивания для корректной работы с памятью
// Шаблонный класс - может быть использован для хранения различных типов: Key - тип ключа, T - тип хранимого значения
// Поскольку класс контейнерный, методы, на время своего выполнения блокируют мьютекс, тем самым запрещая изменения дерева во время своей работы
// Больщинство методов объявленны виртуальными для того, чтобы была возможность переопределять их в дочерних классах при необходимости

// Интерфейс класса (описание методов и полей):
template <class Key, class T>
class BTree {
public: // публичные (доступные для использования вне класса) методы и данные
    typedef struct _Pair { Key key; T data; } Pair; // тип для хранения пары ключ:значение

    BTree(); // конструктор по умолчанию
    BTree(unsigned int n, Pair arr[]); // конструктор инициализации дерева массивом пар элементов ключ:значение
    virtual ~BTree(); // деструктор, объявлен виртуальным для корректного удаления дочерних классов
    BTree(const BTree& tree); // конструктор копирования

    // метод очень короткий, поэтому объявлен inline, что значит, что компилятор может вместо вызова функции подставить ее тело в нужное место
    inline unsigned int size() const; // метод, возвращающий текущий размер дерева (константный, так как не изменяет полей класса)

    virtual bool add(const Key& key, const T& obj); // метод, добавляющий в дерево значение по ключу (если значение существует - оно обновляется)
    virtual bool del(const Key& key); // метод удаления значения из дерева по ключу
    virtual std::list<Key> keys(void) const; // метод, возвращающий список хранимых в дереве ключей

    virtual T operator [](const Key& key); // оператор доступа к данным по ключу
    virtual BTree<Key, T>& operator =(const BTree<Key, T>& tree); // оператор присваивания
    virtual bool operator +(const Pair& pair); // оператор добавления в дерево пары ключ:значение
    virtual bool operator -(const Key& key); // оператор удаления значения из дерева по ключу

protected: // поля и методы ниже защищенные, что значит, что дочерние классы могут их наследовать
    typedef struct _Node {
        Key key; // ключ доступа к элементу дерева (уникальный!)
        T data; // хранимые данные (не определены)
        // по умолчанию все данные узла неопределены = nullptr
        _Node* left = nullptr; // указатель на левое поддерево
        _Node* right = nullptr; // указатель на правое поддерево
        _Node* parent = nullptr; // указатель на узел родителя
    } Node; // определяем новый тип для узла дерева

    Node* m_root; // указатель на корень дерева
    unsigned int m_size; // размер дерева
    // mutex объявлен mutable для того, чтобы была возможность его блокировки из константных методов и корректной работы класса
    mutable std::mutex mutex; // мьютекс для ограничения одновременного доступа к дереву во избежание коллизий

    // так как тип Node - приватный, следующие методы могут быть использованы только внутри класса:
    virtual void add(Node* node); // метод, добавляющий в дерево элементы начиная с определенной вершины
    virtual void readd(Node* node); // метод, добавляющий в дерево элементы начиная с определенной вершины, удаляя добавленне данные
    virtual void clear(Node* node); // очистка дерева начиная с определенной вершины
    virtual void key_itterating(Node* node, std::list<Key>* list) const; // метод, собирающий в список все ключи в дереве
};

// Реализация описанных в интерфейсе методов:

template <class Key, class T>
BTree<Key, T>::BTree() {
    // контейнер пуст
    m_size = 0; // размер = 0
    m_root = nullptr; // нет элементов => корень = nullptr
}

template <class Key, class T>
BTree<Key, T>::BTree(unsigned int n, Pair arr[]) {
    // начальная инициализация
    m_size = 0; // размер = 0
    m_root = nullptr; // нет элементов => корень = nullptr
    for (int i = 0; i < n; ++i) {
        add(arr[i].key, arr[i].data); // добавляем переданные в массиве элементы в дерево
    }
}

template <class Key, class T>
BTree<Key, T>::~BTree() {
    clear(m_root); // удаляем все элементы дерева, включая корень
    m_root = nullptr; // в дереве не осталось данных => корень = nullptr (для избежания ошибки разыменования указателя на мусор)
    m_size = 0; // после удаления размер списка = 0
}

template <class Key, class T>
BTree<Key, T>::BTree(const BTree& tree) {
    add(tree.m_root); // рекурсивно добавляем все элементы переданного дерева в текущее
}

template <class Key, class T>
unsigned int BTree<Key, T>::size() const {
    mutex.lock(); // блокируем изменения списка на время выполнения метода
    unsigned int size = m_size; // запоминаем текущий размер списка
    mutex.unlock(); // разблокируем мьютекс
    return size; // возвращаем полученный ранее размер
}

template <class Key, class T>
bool BTree<Key, T>::add(const Key& key, const T& obj) {
    try {
        mutex.lock(); // блокируем изменения списка на время выполнения метода
        if (m_root == nullptr) {
            // если в дереве нет корня - создаем новый
            m_root = new Node;
            // сохраняем переданные ключ и значение:
            m_root->key = key;
            m_root->data = obj;
            m_size++; // увеличиваем размер дерева на один
            mutex.unlock(); // разблокируем мьютекс
            return true;
        }
        // создаем указатели, с помощью которых будем передвигаться по дереву
        Node* p_tmp = m_root; // указатель на текущий элемент
        Node* p_tmp_parent = p_tmp->parent; // указатель на его родителя
        while (p_tmp != nullptr && p_tmp->key != key) {
            // пока не дойдем до пустого листа дерева или не найдем узел с таким же ключем, передвигаемся по дереву
            p_tmp_parent = p_tmp; // смещаем указатель родителя на текущий элемент
            p_tmp = (p_tmp->key > key) ? p_tmp->left : p_tmp->right; // смещаем указатель на текущий элемент в зависимости от ключа (врава - больше, влево - меньше)
        }
        if (p_tmp == nullptr) {
            // если текущий элемент - пустой лист, добавляем новый узел с данными
            p_tmp = new Node;
            p_tmp->parent = p_tmp_parent; // запоминаем родителя
            // сохраняем переданные ключ и значение:
            p_tmp->key = key;
            p_tmp->data = obj;
            // запоминаем созданный узел для родителя:
            if (p_tmp->key < p_tmp_parent->key) {
                p_tmp_parent->left = p_tmp;
            } else {
                p_tmp_parent->right = p_tmp;
            }
            m_size++; // увеличиваем размер дерева на один
            mutex.unlock();
            return true;
        } else if (p_tmp->key == key) {
            // обновляем данные текущего элемента
            p_tmp->data = obj;
            mutex.unlock(); // разблокируем мьютекс
            return true;
        }
        mutex.unlock(); // разблокируем мьютекс
        return false;
    } catch (...) {
        mutex.unlock();
        return false;
    }
}

template <class Key, class T>
bool BTree<Key, T>::del(const Key& key) {
    try {
        mutex.lock(); // блокируем изменения списка на время выполнения метода
        if (m_root == nullptr) {
            // дерево пусто - удалять нечего
            mutex.unlock(); // разблокируем мьютекс
            return false;
        }
        // создаем указатели, с помощью которых будем передвигаться по дереву
        Node* p_tmp = m_root; // указатель на текущий элемент
        Node* p_tmp_parent = p_tmp->parent; // указатель на его родителя
        while (p_tmp != nullptr && p_tmp->key != key) {
            // пока не дойдем до пустого листа дерева или не найдем узел с таким же ключем, передвигаемся по дереву
            p_tmp_parent = p_tmp; // смещаем указатель родителя на текущий элемент
            p_tmp = (p_tmp->key > key) ? p_tmp->left : p_tmp->right; // смещаем указатель на текущий элемент в зависимости от ключа (врава - больше, влево - меньше)
        }
        if (p_tmp == nullptr) {
            // ключ не найден - удалять нечего
            mutex.unlock(); // разблокируем мьютекс
            return false;
        }
        if (p_tmp == m_root) {
            // обработка удаления корня дерева:
            if (p_tmp->left) {
                // если имеется левое поддерево, делаем его корнем
                m_root = m_root->left;
                readd(m_root->right); // заново добавляем в дерево правое поддерево
            } else {
                m_root = m_root->right;
                if (m_root) {
                    // если корень дерева не пуст (что может быть в дереве из одного элемента):
                    readd(m_root->left); // заново добавляем в дерево правое поддерево
                }
            }
            if (m_root) {
                // если корень дерева не пуст (что может быть в дереве из одного элемента):
                m_root->parent = nullptr; // забываем родителя
            }
        } else if (p_tmp == p_tmp_parent->left) {
            p_tmp_parent->left = nullptr; // забываем узел для родителя
            readd(p_tmp->right); // заново добавляем в полученное дерево значения правого поддерева текущего узла
        } else {
            p_tmp_parent->right = nullptr;  // забываем узел для родителя
            readd(p_tmp->left); // заново добавляем в полученное дерево значения левого поддерева текущего узла
        }
        delete p_tmp; // удаляем текущий узел
        m_size--; // уменьшаем размер дерева
        mutex.unlock(); // разблокируем мьютекс
        return true;
    } catch (...) {
        mutex.unlock();
        return false;
    }
}

template <class Key, class T>
std::list<Key> BTree<Key, T>::keys() const {
    std::list<Key> keys; // создаем список, в который будем помещать ключи
    mutex.lock(); // блокируем изменения списка на время выполнения метода
    key_itterating(m_root, &keys); // добавляем в список все ключи дерева
    mutex.unlock(); // разблокируем мьютекс
    return keys; // возвращаем полученный список
}

template <class Key, class T>
T BTree<Key, T>::operator[](const Key& key) {
    mutex.lock(); // блокируем изменения списка на время выполнения метода
    Node* p_tmp = m_root; // указатель, с помощью которого будем перемещаться по дереву
    while (p_tmp != nullptr && p_tmp->key != key) {
        p_tmp = (p_tmp->key > key) ? p_tmp->left : p_tmp->right;
    }
    if (p_tmp == nullptr) {
        mutex.unlock(); // разблокируем мьютекс
        throw static_cast<std::string>("Can't find key!"); // заданного ключа нет в списке - генерируем исключение
    }
    T result = p_tmp->data; // копируем данные узла
    mutex.unlock(); // разблокируем мьютекс
    return result;
}

template <class Key, class T>
BTree<Key, T>& BTree<Key, T>::operator=(const BTree<Key, T>& tree) {
    if (this != &tree) /* если перед нами не один и тот же объект */ {
        mutex.lock(); // блокируем мьютекс
        clear(m_root); // очищаем все данные текущего дерева
        add(tree.m_root); // добавляем все элементы переданного дерева в текущее
        mutex.unlock(); // разблокируем мьютекс для последующих операций над деревом
    }
    return *this; // возвращаем ссылку на текущий объект
}

template <class Key, class T>
bool BTree<Key, T>::operator +(const Pair& pair) {
    return add(pair.key, pair.data); // добвляем в дерево заданную пару ключ:значение
}

template <class Key, class T>
bool BTree<Key, T>::operator-(const Key& key) {
    return del(key); // удаляем из дерева узел с заданным ключом
}

template <class Key, class T>
void BTree<Key, T>::add(Node* node) {
    if (node != nullptr) {
        // пока не дойдем до пустых листьев:
        readd(node->left); // рекурсивное добавление левого поддерева
        readd(node->left); // рекурсивное добавление правого поддерева
        mutex.unlock(); // для использования метода add следует разблокировать мьютекс, так как метод блокирует его сам
        add(node->key, node->data); // добавляем новый узел с текущими данными
        mutex.lock(); // после добавления блокируем мьютекс снова
    }
}

template <class Key, class T>
void BTree<Key, T>::readd(Node* node) {
    if (node != nullptr) {
        // пока не дойдем до пустых листьев:
        readd(node->left); // рекурсивное добавление левого поддерева
        readd(node->right); // рекурсивное добавление правого поддерева
        mutex.unlock(); // для использования метода add следует разблокировать мьютекс, так как метод блокирует его сам
        add(node->key, node->data); // добавляем новый узел с текущими данными
        mutex.lock(); // после добавления блокируем мьютекс снова
        m_size--; // уменьшаем количество элементов дерева на один, так как метод добавление увеличивает его
        delete node; // очистка занимаемой памяти
    }
}

template <class Key, class T>
void BTree<Key, T>::clear(Node* node) {
    if (node != nullptr) {
        // пока не дойдем до пустых листьев:
        clear(node->left); // рекурсивно удаляем левое поддерево
        clear(node->right); // рекурсивно удаляем правое поддерево
        delete node; // удаляем текуший узел
    }
}

template <class Key, class T>
void BTree<Key, T>::key_itterating(Node* node, std::list<Key>* list) const {
    if (node != nullptr) {
        // пока не дойдем до пустых листьев:
        key_itterating(node->left, list); // рекурсивно вызываем метод для левого поддерева
        key_itterating(node->right, list);  // рекурсивно вызываем метод для правого поддерева
        list->push_back(node->key); // добавляем ключ текущего узла в список
    }
}

#endif //BTREE_H
