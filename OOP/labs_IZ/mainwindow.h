#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "Credit.h" // данный файл также содержит класс Debit
#include "btree.h" // класс бинарного дерева
#include <QMainWindow> // окно графического интрефейса
#include <QMessageBox> // для вывода сообщений пользователю
#include <QMutex> // для ограничения одновременного доступа к разделяемым ресурсам
#include <fstream> // для сохранения в файл средствами библиотеки iostream
#include <typeinfo> // для оперделения типа объектов, хранимых в контейнере (родительский/дочерний класс)
#include <QDebug> // для вывода информации

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindow(QWidget* parent = 0); // конструктор по умолчанию
    ~MainWindow(); // деструктор
private:
    Ui::MainWindow* ui; // интерфейс окна
    BTree<std::string, Debit*> m_container; // дерево указателей на базовый класс (значит хранить можно и указатели на дочерний)
    Debit* m_operation; // последняя выполненная операция (переменная может хранить также указатель на дочерний класс)
    QMutex m_mutex; // мьютекс для блокировки доступа к разделяемым данным

    // метод, выводящий информацию о текущей операции с заданным форматом:
    void printInfo(bool clear = false, const std::string& case1 = "", const std::string& case2 = "");
signals:
    void Size(int n); // сигнал для индикации заполненности хранилища
public slots:
    // слоты для добавления / удаления операций в потоке:
    void autoAdd();
    void autoDelete();
private slots:
    void checkAddButton(); // проверяет, введены ли данные, необходимые для выполнения операции, затем делает кнопку доступной
    void checkCredit(); // проверяет, желает ли пользователь взять кредит
    void checkNavigation(); // проверяет, введен ли ключ для навигации
    void addOperation(); // добавляет операцию в дерево
    void findOperation(); // находит операцию в дереве
    void deleteOperation(); // удаляет операцию из дерева
    void saveToFile(); // сохранение выполенных операций в файл
    void loadFromFile(); // загрузка операций из файла
};

#endif // MAINWINDOW_H
