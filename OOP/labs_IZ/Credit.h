#ifndef TEMPCLASSES_CREDIT_H
#define TEMPCLASSES_CREDIT_H

#include "Debit.h"

// Класс банковской операции, дочерний к классу другой операции (добавлено поле процент и методы необходимые для работы с ним)
// Необходимые методы(деструктор) и операторы(присвоения) перегружены.
// В классе реализованно правило трех - переопределены конструктор копирования, деструктор и оператор присваивания для корректной работы с памятью
// Больщинство методов объявленны виртуальными для того, чтобы была возможность переопределять их в дочерних классах при необходимости
// Для класса переопределены многие операторы в том чилсе операторы потокового вывода/ ввода

// Интерфейс класса (описание методов и полей):
class Credit : public Debit {
public:
    //операторы ввода/вывода объявлены как дружественные => имеют доступ к приватным полям класса
    friend std::ostream& operator <<(std::ostream &out, const Credit &client); //оператор вывода
    friend std::istream& operator >>(std::istream &in, Credit &client); //оператор ввода
public:
    Credit(); // конструктор по умолчанию
    Credit(double bill, double income, double consumption, float percent, const std::string& sender = "Mr. Slim", const std::string& receiver = "Dr. Dre"); // конструктор инициализации
    virtual ~Credit(); // деструктор, объявлен виртуальным для корректного удаления дочерних классов
    Credit(const Credit& credit); // конструктор копирования

    // селекторы и модификаторы для новых полей класса:
    virtual void set_percent(float);
    virtual float get_percent() const;

    virtual bool operator ==(const Credit& credit) const;
    virtual bool operator >(const Credit& credit) const;
    virtual bool operator >=(const Credit& credit) const;

    virtual const Credit& operator =(const Credit& credit); // оператор присваивания
    virtual Credit& operator =(const Debit& debit); // переопределение оператора присваивания базового класса
    // оператор () служит для инициализации объекта, как конструктор инициализации
    virtual void operator ()(double bill, double income, double consumption, float percent, const std::string& sender = "Mr. Slim", const std::string& receiver = "Dr. Dre");
    //@todo:
//    virtual void operator ()(double bill, double income, double consumption, const std::string& sender = "Mr. Slim", const std::string& receiver = "Dr. Dre");
    virtual operator Debit() const; //оператор приведения типов

protected: //для того, чтобы было возможно наследование полей
    float m_percent; // процент по кредиту
};

#endif //TEMPCLASSES_CREDIT_H
