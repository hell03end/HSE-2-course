//
// Created by d_pch on 04-Sep-16.
//

#ifndef LAB1_TESTBOOK_H
#define LAB1_TESTBOOK_H

#define TEST_NAME "Harry Potter"
#define TEST_YEAR 1984
#define TEST_INFO "The book \"Harry Potter\" was published in 1984 year."

#include "../src/Book.h"
#include "CommonTest.h"

class TestBook : public CommonTest {
public:
    TestBook(void);
    ~TestBook(void);
private:
    Book m_book; //static
    Book* m_pntr_book; //dynamically allocated
    Book* m_pntr_copy_of_book; //to test coping constructor

    //Methods:
    void test_pass(bool test_with_gui);
};


#endif //LAB1_TESTBOOK_H
