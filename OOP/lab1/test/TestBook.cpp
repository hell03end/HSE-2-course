//
// Created by d_pch on 04-Sep-16.
//

#include "TestBook.h"

TestBook::TestBook() {
    m_pntr_book = new Book("The Doors Of Perception", 1954); //allocate memory for instance of class
    m_pntr_copy_of_book = NULL;
}

TestBook::~TestBook() {
    if (m_pntr_book != NULL) {
        delete m_pntr_book;
    }
    if (m_pntr_copy_of_book != NULL) {
        delete m_pntr_copy_of_book;
    }
}

void TestBook::test_pass(bool test_with_gui) {
    std::string initial_info = m_pntr_book->get_info();

    *m_pntr_book = m_book; //assigning
    report_error((initial_info != m_pntr_book->get_info()), "Assignment operator=");
    report_error((m_book == *m_pntr_book), "Comparison operator==");

    m_pntr_copy_of_book = new Book(m_book); //coping
    report_error((m_book == *m_pntr_book), "Coping constructor");
    delete m_pntr_copy_of_book;
    m_pntr_copy_of_book = NULL;

    m_book.set_year(TEST_YEAR);
    report_error((m_book.get_year() == TEST_YEAR), "Year's setter");
    m_book.set_name(TEST_NAME);
    report_error((m_book.get_name() == TEST_NAME), "Name's setter");
    report_error((m_book.get_info() == TEST_INFO), "Info/Name/Year getter");

    if (test_with_gui) {
        std::cout << m_book << "\n";
    }
}
