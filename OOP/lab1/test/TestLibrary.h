//
// Created by d_pch on 04-Sep-16.
//

#ifndef LAB1_TESTLIBRARY_H
#define LAB1_TESTLIBRARY_H

#include "../src/Library.h"
#include "CommonTest.h"

#define TEST_NAME "A Book"
#define TEST_YEAR 2016

class TestLibrary : public CommonTest {
public:
    TestLibrary(void);
    ~TestLibrary(void);
private:
    Book m_book; //static
    Book* m_pntr_book; //dynamically allocated
    Library m_library; //static
    Library* m_pntr_library; //dynamically allocated
    struct __fixtures {
        const int amount = 6;
        std::string names[6] = {"Robin Hood", "The doors of perception", "Mine Kampf",
                                "Rework", "Robin Hood", "Harry Potter"};
        int years[6] = {1488, 1945, 1984, 1997, 2000, 2033};
    } test_fixtures;

    //Methods:
    void test_pass(bool test_with_gui);
};

#endif //LAB1_TESTLIBRARY_H
