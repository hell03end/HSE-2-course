//
// Created by d_pch on 04-Sep-16.
//

#ifndef LAB1_COMMONTEST_H
#define LAB1_COMMONTEST_H

#ifndef DO_NOT_TEST_GUI
#define DO_NOT_TEST_GUI false
#endif
#ifndef TEST_GUI
#define TEST_GUI true
#endif

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

class CommonTest {
public:
    CommonTest(void);
    void test(int number_of_generated_error, bool test_with_gui = TEST_GUI); //test entering point
protected:
    bool m_success; //result of testing

    //      Methods:
    virtual void test_pass(bool test_with_gui) = 0; //abstract function, contains tests themselves
    //create error message and change test status to false
    void report_error(bool a_assumption, const std::string& a_message = "Something",
                      const std::string& a_conclusion = "doesn't work");
};

#endif //LAB1_COMMONTEST_H
