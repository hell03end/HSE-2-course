//
// Created by d_pch on 04-Sep-16.
//

#include "TestLibrary.h"

TestLibrary::TestLibrary() {
    m_pntr_book = NULL;
    m_pntr_library = NULL;
}

TestLibrary::~TestLibrary() {
    if (m_pntr_book) {
        delete m_pntr_book;
    }
    if (m_pntr_library) {
        delete m_pntr_library;
    }
}

void TestLibrary::test_pass(bool test_with_gui) {
    Book* temp_pntr_book = NULL; //to collect memory from some functions and free it
    report_error((m_library.empty()), "Empty method");
    report_error((m_library.get_amount_of_books() == 0), "Amount's getter");
    report_error((m_library.get_amount_of_books() == 0 && m_library.empty()), "Default constructor");
    report_error((m_library.get_position() == -1), "Find position (empty list)");
    report_error(((m_library + m_book) && m_library.get_amount_of_books() == 1), "Adding book");
    report_error((!(m_library + m_book) && m_library.get_amount_of_books() == 1),
                 "Adding the same books to the library", "isn't valid");
    report_error((m_library.get_position() == 0), "Find position (right info)");
    report_error((m_library.get_position(TEST_NAME, TEST_YEAR) == -2), "Find position (wrong info)");
    m_pntr_book = new Book(TEST_NAME, TEST_YEAR);
    report_error(((m_library + *m_pntr_book) && m_library.get_amount_of_books() == 2), "Operator+");
    report_error(!(m_library + *m_pntr_book), "Operator+", "added the same book");
    report_error((m_library.get_position(TEST_NAME, TEST_YEAR) == 0), "Operator+", "with wrong order of adding");
    report_error(((m_library - *m_pntr_book) && m_library.get_amount_of_books() == 1), "Operator-");
    report_error((m_library.get_position(TEST_NAME, TEST_YEAR) == -2), "Operator- (deeply)");
    report_error((m_pntr_book->get_name() == TEST_NAME), "Operator-", "changed given data somehow");
    report_error(!(m_library - *m_pntr_book), "Operator-", "tries to delete non-existing book");
    delete m_pntr_book; //delete allocated memory
    report_error((m_library.add_book(m_book.get_name(), TEST_YEAR) && m_library.get_amount_of_books() == 2), "");
    m_pntr_book = new Book(m_book.get_name(), TEST_YEAR);
    report_error(!(m_library + *m_pntr_book), "Adding the same books through the op+", "isn't valid");
    report_error((m_library.delete_book(m_pntr_book->get_name(), m_pntr_book->get_year())), "Deleting through pointer");
    m_library.add_book(TEST_NAME, TEST_YEAR);

    for (int i = 0; i < test_fixtures.amount; ++i) {
        if (m_pntr_book) {
            delete m_pntr_book;
        }
        m_pntr_book = new Book(test_fixtures.names[i], test_fixtures.years[i]);
        report_error(((m_library + *m_pntr_book) && m_library.get_amount_of_books() == (i + 3)), "Adding cycle");
    }
    delete m_pntr_book; //delete allocated memory
    m_pntr_book = NULL;
    report_error(((temp_pntr_book = m_library.get_book(0))->get_name() == TEST_NAME), "Getter book");
    delete temp_pntr_book; //delete allocated memory
    report_error((m_library.delete_book(TEST_NAME, TEST_YEAR)), "Deleting through name");
    report_error((m_library.delete_book(0) && m_library.get_amount_of_books() == 6), "Deleting throw number");
    report_error(((temp_pntr_book = m_library.get_book(-1))->get_name() == test_fixtures.names[1]),
                 "Getter book (flip order)");
    delete temp_pntr_book; //delete allocated memory
    report_error((m_library.delete_book(-1) && m_library.get_book(-1)->get_name() != test_fixtures.names[1]),
                 "Deleting throw pointer (flip order)");

    m_pntr_library = new Library(m_library); //coping
    m_library + *m_pntr_library;
    report_error((m_library.get_amount_of_books() == 5), "Operator+ (added the same)");
    m_library.clear();
    report_error(!((temp_pntr_book = m_library.get_book(0)) && m_library.get_amount_of_books() == 0), "Clear");
    delete temp_pntr_book; //delete allocated memory
    report_error((m_pntr_library->get_position()), "Coping constructor");

    //add new books to the m_library
    for (int i = 0; i < test_fixtures.amount; ++i) {
        if (m_pntr_book) {
            delete m_pntr_book;
        }
        m_pntr_book = new Book(test_fixtures.names[i], test_fixtures.years[i]);
        m_library + *m_pntr_book;
    }
    m_library + *m_pntr_library;
    report_error((m_library.get_amount_of_books() == 7), "Operator+ to lists");
    m_library - *m_pntr_library;
    report_error((m_library.get_amount_of_books() == 2), "Operator- to lists");

    if (test_with_gui) {
        std::cout << m_library << "\n";
        std::cout << *m_pntr_library << "\n";
        getchar(); //pause
    }

    delete m_pntr_library;

}