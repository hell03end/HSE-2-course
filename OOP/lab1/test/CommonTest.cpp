//
// Created by d_pch on 04-Sep-16.
//

#include "CommonTest.h"

CommonTest::CommonTest() {
    m_success = true;
}

void CommonTest::test(int number_of_generated_error, bool test_with_gui) {
    test_pass(test_with_gui); //do testing
    if (!m_success) {
        getchar(); //pause
        exit(number_of_generated_error);
    }
}

void CommonTest::report_error(bool a_assumption, const std::string &a_message, const std::string& a_conclusion) {
    if (!a_assumption) {
        m_success = false;
        std::cerr << "Error: " << a_message << " " << a_conclusion << "!\n"; //generates error message
    }
}
