//  Created by Pchelkin Dmitriy Alexeevich, BIV-151, number in group = 10 -> variant 11 and 3.
//  Task: create a list(library), which stores books.
//  There will be 2 classes Book and Library. Class::Book will contain data (name, year of publishing) and methods
//  (getters and setters) to manipulate these data. Class::Library will store a list of structures which contains
//  pointers to connect elements of list together (next, previous) and pointer to data (Book::obj). It will be done with
//  aim to divide functions of Class::Book and Class::Library (Previously, pointers to link elements had been stored
//  into Class::Book). Also, Class::Library will contain methods to manipulate with objects which it's stores. All
//  methods designed is the way they aren't give access to data of their Class (return only copies).
//  Tests will be independent and automatic as much as it's possible.
//  version 1.0: 06-Sep-16

#include "test/TestBook.h"
#include "test/TestLibrary.h"

std::ostream& operator<<(std::ostream& os, const Library& a_library);

int main() {
    CommonTest* test_class_book = new TestBook(); //reserve memory and creates objects of test-classes
    CommonTest* test_class_library = new TestLibrary();

    test_class_book->test(1, DO_NOT_TEST_GUI); //start testing with error code 1 and without GUI
    test_class_library->test(2); //start testing with error code 2

    delete test_class_book; //free memory from test-classes
    test_class_book = NULL;
    delete test_class_library;
    test_class_library = NULL;

    //getchar(); //pause
    return EXIT_SUCCESS;
}

//friend function to Class::Library which overload operator<< for std::cout:
std::ostream& operator<<(std::ostream& os, const Library& a_library) {
    os << "Books in library:\n";

    if (a_library.empty()) {
        os << "Nothing.\n";
    } else {
        for (int i = 0; i < a_library.get_amount_of_books(); i++) {
            os << i << " - " << a_library.get_book(i)->get_info() << "\n";
        }
    }

    return os;
}

//  Both classes was created and tested. Each class has it's own test-class, which inherits from abstract
//  Class::CommonTest in which common tools are stored and has it's unique behaviour.
//  WARNING: One test doesn't pass -> system may works incorrectly.
//  @todo: check the problem (destructor in Class::Library)

