#include "Book.h"

Book::Book(const std::string &a_name, const int a_year) {
    set_year(a_year);
    set_name(a_name);
} //@tested

//                  Setters:
inline void Book::set_name(const std::string& a_name) {
    m_name = (a_name.empty()) ? DEFAULT_NAME : a_name; //check the name of book is valid
} //@tested

inline void Book::set_year(const int a_year) {
    m_year = (a_year < MIN_YEAR || a_year > MAX_YEAR) ? UNKNOWN_YEAR : a_year; //check the date of publishing is valid
} //@tested

//                  Getters:
inline std::string Book::get_name() const {
    return m_name;
} //@tested

inline int Book::get_year() const {
    return m_year;
} //@tested

//@todo can it be done without sprintf()?
std::string Book::get_info() const {
    char year[] = "unknown"; //shown if year is invalid

    if (m_year != UNKNOWN_YEAR) {
        sprintf(year, "%d", m_year); //cast integer to string
    }

    return ("The book \"" + m_name + "\" was published in " + year + " year.");
} //@tested

//                  Operators:
Book& Book::operator=(const Book& a_book) {
    //check whether objects are equal:
    if (this != &a_book) {
        m_year = a_book.get_year();
        m_name = a_book.get_name();
    }

    return *this;
} //@tested

bool Book::operator==(const Book& a_book) const {
    return (m_year == a_book.m_year && m_name == a_book.m_name) ? true : false;
} //@tested

//                  Friend functions:
std::ostream& operator<<(std::ostream& os, const Book& a_book) {
    os << a_book.get_info();

    return os;
} //@tested
