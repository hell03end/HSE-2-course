// @todo: Do the same with custom container (template)

#ifndef LAB1_LIBRARY_H
#define LAB1_LIBRARY_H

#include <cstdarg>
#include "Book.h"

class Library {
    friend std::ostream& operator<<(std::ostream& os, const Library& a_library);
public:
    Library(void); //standard constructor
    //@todo: Library(const int args, const Book books[]); //any amount of arguments
    ~Library(void); //destructor
    Library(const Library& a_library); //coping constructor

    //Getters:
    int get_amount_of_books(void) const;
    Book* get_book(int position) const; //return copy of Book::obj from custom position
    int get_position(const std::string& a_name = DEFAULT_NAME, int a_year = UNKNOWN_YEAR) const; //find book
    bool empty(void) const;

    //Operators:
    bool operator+(const Book& new_book); //overloading the operator of adding to add a book to the library
    bool operator-(const Book& old_book); //overloading the operator of subtracting to remove a book from the library
    void operator+(const Library& a_library); //overloading the operator of adding to add one library to another
    void operator-(const Library& a_library); //overloading the operator of subtracting to remove one library from another

    //adding new book with custom parameters*:
    bool add_book(const std::string &a_name = DEFAULT_NAME, int a_year = UNKNOWN_YEAR);
    //deleting a book with custom parameters*:
    bool delete_book(const std::string &a_name = DEFAULT_NAME, int a_year = UNKNOWN_YEAR);
    bool delete_book(int position); //overwrite deleting a book by number*
    //*: return true if succeed and false if book with such parameters wasn't found or already exists

    void clear(void); //delete all books (deeply)
private:
    typedef struct __BookRecord {
        Book* current_book = NULL; //Pointer to Book::obj itself
        __BookRecord* next = NULL;
        __BookRecord* previous = NULL;
    } BookRecord; //structure which stores information of one element of list
    BookRecord* m_pntr_head; //pointer to the start of list
    BookRecord* m_pntr_tail; //pointer to the end of list
    int m_number_of_records;

    //Methods (unsafe to use them directly 'cause they can modify original objects):
    bool delete_book(const Book& old_book); //overwrite deleting a book by link on it*
    void delete_record(BookRecord* old_record); //delete control structure
    bool add_book(Book* new_book); //overwrite adding new book by link on it*
};

#endif //LAB1_LIBRARY_H
