#ifndef LAB1_BOOKS_H
#define LAB1_BOOKS_H

#include <string>
#include <cstdio>

#define DEFAULT_NAME "Some Book"
#define UNKNOWN_YEAR 0
#define MIN_YEAR 1900
#define MAX_YEAR 2016

class Book {
    friend std::ostream& operator<<(std::ostream& os, const Book& a_book); //sent Book::obj to std::cout
public:
    Book(const std::string& a_name = DEFAULT_NAME, const int a_year = UNKNOWN_YEAR); //custom constructor

    //      Setters:
    void set_name(const std::string& a_name);
    void set_year(const int a_year);

    //      Getters:
    std::string get_name(void) const;
    int get_year(void) const;
    std::string get_info(void) const; //cast all info about object to string

    //      Operators:
    Book& operator=(const Book& a_book); //overload operator of assignment
    bool operator==(const Book& a_book) const; //overload operator of comparison
private:
    std::string m_name;
    int m_year;
};

#endif //LAB1_BOOKS_H
