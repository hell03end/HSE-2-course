#include "Library.h"

Library::Library() {
    m_number_of_records = 0;
    m_pntr_head = nullptr;
    m_pntr_tail = nullptr;
} //@tested

Library::~Library() {
    clear(); //delete all records
} //@tested

//Library::Library(const int args, const Book *books) {
//    Library();
//
//    for (int i = 0; i < args; i++) {
//        *this + books[i];
//    }
//}

Library::Library(const Library &a_library):
    Library()
{
    for (BookRecord* temp_pntr = a_library.m_pntr_head; temp_pntr != nullptr; temp_pntr = temp_pntr->next) {
        *this + *temp_pntr->current_book; //add a copy of element in the new list
    }
} //@tested

//                  Getters:
int Library::get_amount_of_books() const {
    return m_number_of_records;
} //@tested

Book* Library::get_book(int position) const {
    if (empty()) {
        return NULL;
    }

    BookRecord* tmp_pntr;

    if (position >= 0 && position < m_number_of_records) {
        for (tmp_pntr = m_pntr_head; position--; tmp_pntr = tmp_pntr->next)
            ; //search upwards, 'cause position is positive (and valid)
    } else if (position < 0 && position > -m_number_of_records) {
        for (tmp_pntr = m_pntr_tail; ++position; tmp_pntr = tmp_pntr->previous)
            ; //search backwards, 'cause position is negative (and valid)
    } else {
        return NULL; //invalid position
    }

    //creates a copy of Book::obj to protect original value:
    Book* new_book = new Book(tmp_pntr->current_book->get_name(), tmp_pntr->current_book->get_year());

    return new_book; //return copy
} //@tested

int Library::get_position(const std::string &a_name, int a_year) const {
    if (empty()) {
        return -1; //error code which signals that list is empty
    }

    int position = 0;

    for (BookRecord* tmp_pntr  = m_pntr_head; tmp_pntr != NULL && tmp_pntr->current_book->get_name() != a_name && \
        tmp_pntr->current_book->get_year() != a_year; tmp_pntr = tmp_pntr->next, position++)
        ; //search object in list

    if (position >= m_number_of_records) {
        return -2; //error code which signals that object not found
    } else {
        return position;
    }
} //@tested

inline bool Library::empty() const {
    return (m_pntr_head == NULL);
} //@tested

//                  Operators:
bool Library::operator+(const Book& new_book) {
    Book* tmp_book = new Book(new_book.get_name(), new_book.get_year()); //creates new Book::obj to add it in list

    return add_book(tmp_book);
} //@tested

bool Library::operator-(const Book &old_book) {
    Book tmp_book; //new Book::obj to delete the same from list

    tmp_book.set_name(old_book.get_name());
    tmp_book.set_year(old_book.get_year());

    return delete_book(tmp_book);
} //@tested

void Library::operator+(const Library &a_library) {
    for (BookRecord* temp_pntr = a_library.m_pntr_head; temp_pntr != NULL; temp_pntr = temp_pntr->next) {
        *this + *temp_pntr->current_book; //add a copy of element in the new list
    }
} //@tested

void Library::operator-(const Library &a_library) {
    for (BookRecord* temp_pntr = a_library.m_pntr_head; temp_pntr != NULL; temp_pntr = temp_pntr->next) {
        *this - *temp_pntr->current_book; //add a copy of element in the new list
    }
}

//                  Adding:
bool Library::add_book(const std::string &a_name, int a_year) {
    Book* new_book = new Book(a_name, a_year); //creates new Book::obj to add it in list

    return add_book(new_book);
} //@tested

//                  Deleting:
bool Library::delete_book(const std::string &a_name, int a_year) {
    if (empty()) {
        return false;
    }

    Book tmp_book; //new Book::obj to delete the same from list
    tmp_book.set_name(a_name);
    tmp_book.set_year(a_year);

    return delete_book(tmp_book);
} //@tested

bool Library::delete_book(int position) {
    if (empty()) {
        return false;
    }

    BookRecord* tmp_pntr;

    if (position >= 0 && position < m_number_of_records) {
        for (tmp_pntr = m_pntr_head; position--; tmp_pntr = tmp_pntr->next)
            ; //search upwards, 'cause position is positive (and valid)
    } else if (position < 0 && position > -m_number_of_records) {
        for (tmp_pntr = m_pntr_tail; ++position; tmp_pntr = tmp_pntr->previous)
            ; //search backwards, 'cause position is negative (and valid)
    } else {
        return false; //invalid position
    }

    delete_record(tmp_pntr);

    return true;
} //@tested

void Library::clear() {
    for (BookRecord* tmp_pntr = m_pntr_head->next; tmp_pntr->next != NULL; tmp_pntr = tmp_pntr->next) {
        delete tmp_pntr->previous->current_book;
        delete tmp_pntr->previous;
    }
    m_pntr_head = NULL;
    delete m_pntr_tail->current_book;
    delete m_pntr_tail;
    m_pntr_tail = NULL;
    m_number_of_records = 0;
} //@tested

//                  Private:
bool Library::delete_book(const Book& old_book) {
    if (empty()) {
        return false;
    }

    BookRecord* tmp_pntr = m_pntr_head;

    while (tmp_pntr != NULL && tmp_pntr->current_book->get_info() != old_book.get_info()) {
        tmp_pntr = tmp_pntr->next;
    } //search the same objects

    if (tmp_pntr == NULL) {
        return false; //there are no same objects
    } else {
        delete_record(tmp_pntr);
    }

    return true;
} //@tested

void Library::delete_record(BookRecord *old_record) {
    if (old_record == m_pntr_head && old_record == m_pntr_tail) {
        m_pntr_head = m_pntr_tail = NULL; //last element in list will be deleted
    } else if (old_record == m_pntr_head) {
        m_pntr_head = old_record->next;
        old_record->next->previous = NULL; //head of list will be deleted
    } else if (old_record == m_pntr_tail) {
        m_pntr_tail = old_record->previous;
        old_record->previous->next = NULL; //tail of list will be deleted
    } else {
        old_record->next->previous = old_record->previous;
        old_record->previous->next = old_record->next;
    }

    delete old_record->current_book; //delete data
    delete old_record; //delete control structure
    m_number_of_records--;
}

bool Library::add_book(Book* new_book) {
    if (empty()) {
        try {
            m_pntr_head = new BookRecord;
            if (m_pntr_head == NULL) {
                throw;
            }
        } catch (...) {
            return false;
        }
        m_pntr_head->current_book = new_book; //add book to the record
        m_pntr_tail = m_pntr_head; //link new object with list
    } else {
        BookRecord* tmp_pntr = m_pntr_head;

        while (tmp_pntr->next != NULL && tmp_pntr->current_book->get_info() < new_book->get_info()) {
            tmp_pntr = tmp_pntr->next;
        } //search where to add
        if (tmp_pntr->current_book->get_info() == new_book->get_info()) {
            return false; //this object already exists
        } else if (tmp_pntr->current_book->get_info() > new_book->get_info()) {
            BookRecord* new_record;
            try {
                new_record = new BookRecord;
                if (new_record == nullptr) {
                    throw;
                }
            } catch (...) {
                return false;
            }

            new_record->current_book = new_book;
            new_record->next = tmp_pntr;
            if (tmp_pntr->previous) {
                new_record->previous = tmp_pntr->previous;
                tmp_pntr->previous->next = new_record;
            } else {
                m_pntr_head = new_record; //object must be the first element in list
            }
            tmp_pntr->previous = new_record;
        } else {
            BookRecord* new_record;
            try {
                new_record = new BookRecord;
                if (new_record == nullptr) {
                    throw;
                }
            } catch (...) {
                return false;
            }

            new_record->current_book = new_book;
            new_record->previous = tmp_pntr;
            if (tmp_pntr->next != nullptr) {
                new_record->next = tmp_pntr->next;
                tmp_pntr->next->previous = new_record;
            } else {
                m_pntr_tail = new_record; //object must be the last element in list
            }
            tmp_pntr->next = new_record;
        }
    }

    m_number_of_records++;

    return true;
} //@tested

//                  Friend functions:
//std::ostream& operator<<(std::ostream& os, const Library& a_library) {
//    os << "Books in library:\n";
//
//    if (a_library.empty()) {
//        os << "Nothing.\n";
//    } else {
//        for (int i = 0; i < a_library.get_amount_of_books(); i++) {
//            os << i << " - " << a_library.get_book(i)->get_info() << "\n";
//        }
//    }
//
//    return os;
//}
