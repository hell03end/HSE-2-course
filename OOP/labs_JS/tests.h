// Класс содержащий тесты используемых классов.

#ifndef TESTS_H
#define TESTS_H

#include <QObject>
#include <QDebug> //для вывода сообщений о проведении тестирования
#include <iostream> //ввод-вывод с++
#include <string> //для работы со строками
#include "account.h" //подключение классов пользователя и дочернего ему - аккаунта (для тестов)
#include "stack.h" // подключение класса стэка

class Tests : public QObject {
    Q_OBJECT
public:
    explicit Tests(int t_n = 0, QObject *parent = 0): n(t_n) {} //инициализация номеров тестов
public slots:
    //метод, запускающих работу тестов
    void Run() {
        //тестирование классов пользователя, аккаунта и стэка
        if (n%10 != 0) /* Тесты для объектов класса User */ {
            qDebug() << QString("User tests started...\n");
            User* u2 = new User(QString::fromStdString("hseguest"), QString::fromStdString("hsepassword"), 10); //динамический экземпляр класса
            delete u2; //срабатывание деструктора
            u2 = new User(u1); //проверка конструктора копирования
            u1.setLogin(QString::fromStdString("hseguest")); //изменение имени пользователя
            u1.setPassword(QString::fromStdString("hsepassword")); //изменение пароля
            if (!(u1(u1("hello")) == QString::fromStdString("hello"))) {
                qDebug() << QString("Ошибка шифрования.\n");
                exit(1);
            }
            u1 = *u2;
            if ((u1.getLogin() != QString::fromStdString("login") || u1.getPassword(true) != QString::fromStdString("password"))) {
                qDebug() << QString("Ошибка оператора =.\n");
                exit(2);
            }
            if (!(u1 == *u2)) {
                qDebug() << QString("Ошибка оператора ==.\n");
                exit(3);
            }
        } //конец тестов класса User
        if ((n/10)%10 != 0) /* Тесты для объектов класса Account */ {
            qDebug() << QString("Account tests started...\n");
            Account* a2 = new Account(QString::fromStdString("hseguest"), QString::fromStdString("hsepassword"),
                                      QString::fromStdString("somemail@mail.ru")); //динамический экземпляр класса
            delete a2; //срабатывание деструктора
            a2 = new Account(a1); //проверка конструктора копирования
            a1.setLogin(QString::fromStdString("hseguest")); //изменение имени
            a1.setPassword(QString::fromStdString("hsepassword")); //изменение пароля
            a1.setEmail(QString::fromStdString("somemail@mail.ru")); //изменение email
            *a2 = a1;
            if (a2->getLogin() != QString::fromStdString("hseguest") ||
                a2->getPassword(true) != QString::fromStdString("hsepassword") ||
                a2->getEmail() != QString::fromStdString("somemail@mail.ru")) {
                qDebug() << QString("Ошибка оператора =.\n");
                exit(4);
            }
            if (!(a1 == *a2)) {
                qDebug() << QString("Ошибка оператора ==.\n");
                exit(5);
            }
            if (a1 + 1 != 1 || a1.getAge() != 1) {
                qDebug() << QString("Ошибка оператора +.\n");
                exit(6);
            }
            if (a1++ != 1 || a1.getAge() != 2) {
                qDebug() << QString("Ошибка оператора ++.\n");
                exit(7);
            }
            if (a1 ^ 4 != 16) {
                qDebug() << QString("Ошибка оператора ^.\n");
                exit(8);
            }
            u1 = (User) a1; //тестирование оператора приведение типов
        } //конец тестов класса Client
        if ((n/100)%10 != 0) /* Тесты для объектов класса Stack */ {
            qDebug() << QString("Stack tests started...\n");
            if (s1.show() != nullptr) /*стэк пуста*/ {
                qDebug() << QString("Ошибка при создании стэка.\n");
                exit(9);
            }
            s1.push(&u1); //добавление пользователя в стэк
            s1 + &a1; //добавление аккаунта в стэк
            if (s1.size() != 2) {
                qDebug() << QString("Ошибка при добавлении в стэк.\n");
                exit(10);
            }
            //проверка оператора индексации:
            if (*(s1[1]) != a1) {
                qDebug() << QString("Ошибка в операторе индексации.\n");
                exit(11);
            }
            User *a2 = *(s1.pop()); //сохранение полученного из стэка элемента (после получение элемент удаляется)
            delete a2; //удаление полученного элемента, для предотвращения утечки памяти
            a2 = *(s1--); //проверка постфиксного оператора декримента
            delete a2;
        } //конец тестов класса Stack
        qDebug() << QString("Tests completed!\n");
        emit(finished());
    }
signals:
    void finished(); //сигнал о успешном завершении тестов
private:
    int n; //номера тестов, которые нужно выполнить
    User u1; //статический экземпляр класса User
    Account a1; //статический экземпляр класса Account
    Stack<User*> s1; //создание экземпляра класса Stack (конструктор по умолчанию)
};

#endif // TESTS_H
