// Класс содержащий логику работы графического интерфейса пользователя.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <fstream>
#include <QDebug>
#include <typeinfo>
#include <QMutex>
#include "account.h" //данный класс также содержит "user.h"
#include "stack.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0); //конструктор по умолчанию
    ~MainWindow(); //деструктор

private:
    Ui::MainWindow *ui; //интерфейс окна
    Stack<User*> stack; //стэк указателей на базовый класс
    User *firstUser; //первый добавленный в стэк пользователь
    QMutex mutex; //для блокировки одновременно используемых ресурсов

signals:
    void StackSize(int n); //сигнал для индикации заполненности стека

private slots:
    void checkAddButton(); //проверяет, введены ли логин и пароль, затем делает кнопку доступной
    void checkEmailButton(); //проверяет, выбрано ли добавление аккаунта и делает поле ввода почты доступным
    void addUser(); //добавление пользователя наверх стека
    void deleteUser(); //удаление верхнего полозователя из стека
    void printInfo(); //вывод информации о стеке в окна вывода состояния
    void saveToFile(); //сохранение стека в файл
    void loadFromFile(); //загрузка стека из файла

public slots:
    void autoAdd(); //автоматическое добавление пользователей в стек, запущенное в отдельном потоке
    void autoDelete(); //автоматическое удаление пользователей из стека, запущенное в отдельном потоке
    void testsFinished(); //уведомление об успешном завершении тестов, запущенных в отдельном потоке
};

#endif // MAINWINDOW_H
