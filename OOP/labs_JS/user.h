// Класс представляющий пользователя, имеет поля логина и пароля, а так же методы работы с ними.
// Имеет метод (де)шифрования пароля с помощью исключающего или, а так же оператор для шифрование произвольного сообщения.
// Переопределены необходимые методы и операторы (в том числе сравнения)
// Виртуальные методы родительского класса могуть быть переопределены для объектов дочерних классов.
// Задание пароля и имени пользователя может быть переопределено в доченрних классах.
// В классе реализовано правило трех: деструктор, конструктор копирования и оператор присваивания)

#ifndef USER_H
#define USER_H

#include <QString>

using namespace std;

class User {
    //операторы ввода/вывода объявлены как дружественные => имеют доступ к приватным полям класса
    friend ostream& operator <<(ostream &out, const User &user); //оператор вывода
    friend istream& operator >>(istream &in, User &user); //оператор ввода
public:
    // конструкторы
    User(); //по умолчанию
    User(const QString &login, const QString &password, int age = 0); //инициализации
    virtual ~User(); //деструктор (удаление объекта), сделан виртуальным для обеспечения корректности удаления
    User(const User &user); //копирования
    // интеллектуальные модификаторы:
    virtual void setLogin(const QString &login); //задание имени пользователя
    virtual void setPassword(const QString &password); //задание пароля
    virtual void setAge(int age); //задание возраста
    // селекторы (не изменяют полей класса => константны):
    QString getLogin() const; //получение имени пользователя
    QString getPassword(bool decrypt = false) const; //получение зашифрованного/расшифрованного пароля
    int getAge() const; //получение возраста пользователя
    // перегрузка операторов:
    virtual User &operator =(const User &user); //оператор присваивания
    QString operator ()(const QString &str) const; //оператор, использующий объет класса как функцию (шифрует переданную строку)
    // арифметические операторы:
    virtual int operator +(int n); //увеличивает возраст пользователя на заданное количество лет
    virtual int operator -(int n); //уменьшает возраст пользователя на заданное количество лет
    virtual int operator *(int n); //увеличивает возраст пользователя в заданное количество раз
    virtual int operator /(int n); //делит возраст пользователя на заданное количество лет
    virtual int operator ^(int n); //возводит возраст пользователя в степень
    virtual int operator ++(); //увеличивает возраст пользователя на 1. День рождения=)
    virtual int operator ++(int); //увеличивает возраст пользователя на 1. Возвращает возраст до увеличения
    virtual int operator --(); //уменьшает возраст пользователя на 1.
    virtual int operator --(int); //уменьшает возраст пользователя на 1. Возвращает возраст до увеличения
    // логические операторы, методы константные, так как не требуется изменять аттрибутов:
    virtual bool operator ==(const User &user) const;
    virtual bool operator !=(const User &user) const;
    virtual bool operator >(const User &user) const;
    virtual bool operator >=(const User &user) const;
    virtual bool operator <(const User &user) const;
    virtual bool operator <=(const User &user) const;
protected: //модификатор доступа, позволяющий только классам наследникам иметь доступ к полям и методам
    QString login, password; //поля данных класса
    int age; //поле - возраст пользователя
    QString crypt(const QString &message) const; //метод класса, шифрующий/дешифрующий строку (для паролей) методом xor
};

#endif
