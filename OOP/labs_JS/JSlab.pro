QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = JSlab
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp \
        mainwindow.cpp \
        user.cpp \
        account.cpp

HEADERS  += mainwindow.h \
            user.h \
            account.h \
            stack.h \
            threads.h \
            tests.h

FORMS    += mainwindow.ui

