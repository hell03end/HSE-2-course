// Класс представляющий стэк для хранения данных. Данные хранятся в цепочке связанных структур.
// Класс шаблонный => можно хранить объекты любых типов.
// Логика работы методов доступа к элементам стэка обусловлена его устройством - добавление новых элементов в конец, удаление из конца (первым пришел, последним вышел).
// Переопределены необходимые методы и операторы (в том числе оператор индексации)
// В классе реализовано правило трех: деструктор, конструктор копирования и оператор присваивания)
// Класс защищен от одновременного редактирования из нескольких потоков с помощью мьютексов

#ifndef STACK_H
#define STACK_H

#include <iostream>
#include <QMutex>
#include <QDebug>

using namespace std;

// интерфейс класса:
template <class T>
class Stack {
public:
    // конструкторы:
    Stack(); //по умолчанию
    Stack(T array[]); //инициализации - заполняет стэк копиями элементов переданного массива
    ~Stack(); //деструктор (удаление объекта)
    Stack(const Stack& stack); //копирования
    int size(void) const; //получение размера стэка
    void push(const T &object); //добавление элемента наверх стэка
    T *pop(void); //возвращает и удаляет верхний элемент стэка
    T *show(void) const; //возвращает верхний элемент (копию)
    //перегрузка операторов:
    Stack &operator =(const Stack &stack); //оператор присваивания
    T &operator [](int n); //оператор доступа по индексу - может сгенерировать исключение
    inline void operator +(const T &object); //добавление объекта в стэк
    inline T *operator --(int); //удаление верхнего объекта стэка
private:
    typedef struct element {
        element *next = nullptr, *previous = nullptr; //указатели на следующий и предыдущий элементы
        T *data = nullptr; //указатель на хранимые данные
    } Element; //структура элемента очереди
    Element *begin, *end; //начало и конец стэка
    mutable int num; //число элементов в контейнере
    mutable QMutex mutex; //создание мьютекса. Объявлен mutable для возможности использования в константных методах
};

// реализация методов класса:
template <class T>
Stack<T>::Stack() {
    begin = end = nullptr; //в пустой очереди указатели на конец и начало = 0
    num = 0; //очередь пуста
}
template <class T>
Stack<T>::Stack(T array[]) : Stack() {
    for (auto i : array) {
        push(i); //добавляем каждый элемент переданного массива в контейнер
    }
}
template <class T>
Stack<T>::~Stack() {
    Element *current = begin; //текущий элемент равен первому
    while (current != nullptr) /*Проходимся по всем элементам*/ {
        begin = begin->next; //первый элемент смещается на следующий за ним
        delete current->data; //удаляем хранимые данные
        delete current; //удаляем текущий элемент
        current = begin; //смещаем текущий элемент
    }
    begin = end = nullptr; //обнуляем указатели
}
template <class T>
Stack<T>::Stack(const Stack &stack) {
    Stack tempStack; //создаем временный стэк, в который будут помещены все элементы исходной в обратном порядке
    for (int i = 0; i < stack.size(); i++) {
        try {
            tempStack.push(stack[i]); //помещаем каждый элемент из начала переданного стэка в конец временного
        } catch (string s) {
            qDebug() << QString::fromStdString(s);
        }
    }
    for (int i = 0; i < stack.size(); i++) {
        this->push(*(tempStack.pop())); //помещаем каждый элемент из начала временного стэка в новый
    }
}
template <class T>
int Stack<T>::size() const {
    QMutexLocker locker(&mutex); //блокируем мьютекс на время выполнения операции
    return num; //возвращаем количество элементов в очереди
}
template <class T>
void Stack<T>::push(const T &object) {
    QMutexLocker locker(&mutex); //блокируем мьютекс на время выполнения операции
    if (begin == nullptr) /*контейнер пуст*/ {
        begin = new Element; //создаем новый элемент
        end = begin; //в контейнере только один элемент, поэтому указатель на начало равен указателю на конец
    } else {
        begin->next = new Element; //создаем новый элемент в начале стэка
        begin->next->previous = begin; //для созданного элемента запоминаем предыдущий элемент
        begin = begin->next; //перемещаем начало стэка
    }
    begin->data = new T(object); //создаем новый объект и копируем в него переданные данные
    num++; //увеличиваем количество элементов
}
template <class T>
T *Stack<T>::pop() {
    QMutexLocker locker(&mutex); //блокируем мьютекс на время выполнения операции
    if (begin == nullptr) /*контейнер пуст*/ {
        return nullptr; //возвращает нулевой указатель при отсутствии элементов в стэке
    } else {
        T *result = begin->data; //временное хранилище содержимого элемента
        if (begin->previous == nullptr) /*элемент единственный*/ {
            delete begin; //удаляем элемент
            begin = end = nullptr; //присваиваем указателям на начало и конец стэка нули
        } else {
            begin = begin->previous; //смещаем указатель на начало стэка на предыдущий элемент
            delete begin->next; //удаляем первый элемент стэка
            begin->next = nullptr; //обнуляем указатель на певый элемент
        }
        num--; //уменьшаем количество элементов
        return result; //возвращаем первый элемент
    }
}
template <class T>
T *Stack<T>::show() const {
    QMutexLocker locker(&mutex); //блокируем мьютекс на время выполнения операции
    //возвращает нулевой указатель при отсутствии элементов в контейнере иначе возвращает копию первого элемента
    return (begin == nullptr) ? nullptr : new T(*(begin->data));
}
template <class T>
Stack<T> &Stack<T>::operator =(const Stack &stack) {
    mutex.lock(); //блокируем мьютекс на время выполнения операции
    if (this != &stack) /*проверяем, что очереди не один и тот же объект*/ {
        /* если адресса не равны, то объекты тоже, поэтому удаляем исходный объект и заменяем новым */
        delete this;
        this = new Stack();
        mutex.unlock(); //разблокируем мьютекс
        Stack tempStack; //создаем временный стэк, в который будут помещены все элементы исходного в обратном порядке
        for (int i = 0; i < stack.size(); i++) {
            try {
                tempStack.push(stack[i]); //помещаем каждый элемент переданного стэка в конец временного
            } catch (string s) {
                qDebug() << QString::fromStdString(s);
            }
        }
        for (int i = 0; i < stack.size(); i++) {
            this->push(*(tempStack.pop())); //помещаем каждый элемент из временного стэка в новый, в правильном порядке
        }
    }
    mutex.unlock(); //разблокируем мьютекс
    return *this; //возвращаем ссылку на полученный контейнер
}
template <class T>
T &Stack<T>::operator [](int i) {
    QMutexLocker locker(&mutex); //блокируем мьютекс на время выполнения операции
    if (i < 0 || i >= num) {
        throw static_cast<string>("Index out of range!");
    }
    Element *tmp = begin; //временный элемент, для перемещения
    for(; i > 0; i--) {
        tmp = tmp->previous; //продвигаемся по стэку
    }
    return *(tmp->data); //возвращаем ссылку на данные
}
template <class T>
void Stack<T>::operator +(const T &object) {
    this->push(object); //добавляем объект в контейнер
}
template <class T>
T *Stack<T>::operator --(int t) {
    return this->pop(); //возвращаем верхний элемент стэка, из которого элемент удаляется
}

#endif //STACK_H
