#include "mainwindow.h"
#include "ui_mainwindow.h"

ostream& operator <<(ostream &out, const User &user) {
    out << "#######" << endl;
    out << user.crypt(user.login).toStdString() << endl;
    out << user.password.toStdString() << endl;
    out << user.crypt(QString::fromStdString(std::to_string(user.age))).toStdString() << endl;
}
ostream& operator <<(ostream &out, const Account &account) {
    out << account.crypt(account.email).toStdString() << endl;
    out << account.crypt(account.login).toStdString() << endl;
    out << account.password.toStdString() << endl;
    out << account.crypt(QString::fromStdString(std::to_string(account.age))).toStdString() << endl;
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::MainWindow), //создаем новый объект интерфейса пользователя
    firstUser(nullptr) /*первый пользователь, изначально, пуст*/ {
    ui->setupUi(this);

    // проверка на то, что данные введены в форму:
    QRegExp text("[a-zA-Zа-яА-Я]{1}[a-zA-Zа-яА-Я0-9-]{2,30}"); //регулярное выражение, для осуществления проверки
    QRegExp email("([a-zA-Z\.-_]+)@([a-zA-Z\.-]+)(\.[a-zA-Z\.]+)"); //проверка, что в строке введен адрес электронной почты
    ui->lineEdit_login->setValidator(new QRegExpValidator(text, this)); //устанавливаем проверку в поле
    ui->lineEdit_password->setValidator(new QRegExpValidator(text, this)); //устанавливаем проверку в поле
    ui->lineEdit_email->setValidator(new QRegExpValidator(email, this));
    connect(ui->lineEdit_login, SIGNAL(textChanged(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием
    connect(ui->lineEdit_login, SIGNAL(textEdited(QString)), this, SLOT(checkAddButton()));
    connect(ui->lineEdit_password, SIGNAL(textChanged(QString)), this, SLOT(checkAddButton())); //связываем проверку с действием
    connect(ui->lineEdit_password, SIGNAL(textEdited(QString)), this, SLOT(checkAddButton()));
    connect(ui->lineEdit_email, SIGNAL(textChanged(QString)), this, SLOT(checkEmailButton()));
    connect(ui->lineEdit_email, SIGNAL(textEdited(QString)), this, SLOT(checkEmailButton()));
    connect(ui->checkBox, SIGNAL(clicked(bool)), this, SLOT(checkEmailButton()));

    //соединение кнопок с действиями:
    connect(ui->pushButton_add, SIGNAL(clicked(bool)), this, SLOT(addUser()));
    connect(ui->pushButton_delete, SIGNAL(clicked(bool)), this, SLOT(deleteUser()));
    connect(ui->pushButton_save, SIGNAL(clicked(bool)), this, SLOT(saveToFile()));
    connect(ui->pushButton_load, SIGNAL(clicked(bool)), this, SLOT(loadFromFile()));
    connect(this, SIGNAL(StackSize(int)), ui->progressBar, SLOT(setValue(int)));
}

MainWindow::~MainWindow() {
    if (firstUser != nullptr) {
        delete firstUser; //удаляем первого пользователя (если он не был удален прежде)
    }
    delete ui; //удаляем объект графического интерфейса
}

void MainWindow::checkAddButton() {
    // проверяет, введено ли что-то в поля ввода логина/пароля/почты,
    // устанавливает доступность кнопки "добавить пользоватлеля"
    ui->pushButton_add->setEnabled(ui->lineEdit_login->hasAcceptableInput() &&
                                   ui->lineEdit_password->hasAcceptableInput() &&
                                   ui->lineEdit_email->hasAcceptableInput());
    //устанавливает кнопку "добавить пользователя" выбранной по умолчанию
    ui->pushButton_add->setDefault(ui->lineEdit_login->hasAcceptableInput() &&
                                   ui->lineEdit_password->hasAcceptableInput() &&
                                   ui->lineEdit_email->hasAcceptableInput());
}

void MainWindow::checkEmailButton() {
    //делает поле для ввода email доступным, если поставленна галочка
    ui->lineEdit_email->setEnabled(ui->checkBox->isChecked());
    if (!ui->checkBox->isChecked()) {
        //когда пользователь снимает галочку, поле почты восстанавливает свое значение
        ui->lineEdit_email->setText(QString::fromStdString("somemail@mail.ru"));
    }
}

void MainWindow::addUser() {
    mutex.lock();
    if (stack.size() <= 100) {
        QString login = ui->lineEdit_login->text(); //получение информации, введенной в поле логина
        QString password = ui->lineEdit_password->text(); //получение информации, введенной в поле пароля
        QString email = ui->lineEdit_email->text(); //получение адреса электронной почты
        int age = ui->spinBox->value(); //получение возраста пользователя
        if (!ui->checkBox->isChecked()) {
            //создаем нового пользователя
            User *user = new User(login, password, age);
            if (firstUser == nullptr) {
                firstUser = user; //сохраняем первого пользователя
            }
            stack.push(user); //добавляем нового пользователя наверх стека
        } else {
            //создаем новый аккаунт
            Account *account = new Account(login, password, email, age);
            if (firstUser == nullptr) {
                firstUser = account; //сохраняем первого пользователя
            }
            stack.push(account); //добавляем новый аккаунт наверх стека
        }
        //очистка полей ввода информации о пользователе:
        ui->lineEdit_login->clear();
        ui->lineEdit_password->clear();
        ui->lineEdit_email->setText(QString::fromStdString("somemail@mail.ru"));
        ui->checkBox->setChecked(false);
        ui->lineEdit_email->setEnabled(false);
        mutex.unlock();
        printInfo(); //вывод текущего состояния стека
    } else {
        mutex.unlock();
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
        error.setText("<b>Стек переполнен!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Невозможно добавить пользователя в стек");
        error.exec(); //отображаем окно
    }
}

void MainWindow::deleteUser() {
    if (stack.size() != 0) {
        mutex.lock();
        User **user = stack.pop(); //сохранение удаляемого элемента и удаление из стека
        //освобождение памяти
        delete *user;
        delete user;
        if (stack.size() == 0) {
            if (firstUser != nullptr) {
                delete firstUser; //удаление первого добавленного пользователя
            }
            firstUser = nullptr;
        }
        mutex.unlock();
        printInfo();
    } else {
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
        error.setText("<b>Стек пуст!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Невозможно произвести удаление пользователя из пустого стека");
        error.exec(); //отображаем окно
    }
}

void MainWindow::printInfo() {
    QMutexLocker locker(&mutex); //предотвращаем одновременный доступ к полям вывода
    //очищаем поля для вывода
    ui->textBrowser_bottom->clear();
    ui->textBrowser_top->clear();
    int size = stack.size(); //получаем размер стека
    //вывод информации:
    if (size != 0) {
        if (firstUser != nullptr) {
            ui->textBrowser_bottom->append(QString::fromStdString("Логин: "+firstUser->getLogin().toStdString()+"\n"));
            ui->textBrowser_bottom->append(QString::fromStdString("Пароль: "+firstUser->getPassword(true).toStdString()+"\n"));
            ui->textBrowser_bottom->append(QString::fromStdString("Возраст: "+std::to_string(firstUser->getAge())+"\n"));
            if (typeid(*firstUser) == typeid(Account)) {
                ui->textBrowser_bottom->append(QString::fromStdString("Email: "+static_cast<Account*>(firstUser)->getEmail().toStdString()+"\n"));
            }
        }
        User **user = stack.show(); //получаем данные о верхнем пользователе стека (без его удаления)
        if (user != nullptr) /* если данные есть */ {
            ui->textBrowser_top->append(QString::fromStdString("Логин: "+(*user)->getLogin().toStdString()+"\n"));
            ui->textBrowser_top->append(QString::fromStdString("Пароль: "+(*user)->getPassword(true).toStdString()+"\n"));
            ui->textBrowser_top->append(QString::fromStdString("Возраст: "+std::to_string((*user)->getAge())+"\n"));
            if (typeid(**user) == typeid(Account)) {
                ui->textBrowser_top->append(QString::fromStdString("Email: "+static_cast<Account*>((*user))->getEmail().toStdString()+"\n"));
            }
        }
    }
    emit StackSize(size); //изменяем индикатор заполненности стека
}

void MainWindow::saveToFile() {
    mutex.lock();
    ofstream file("file.txt"); //открытие файла для записи
    if (file.is_open()) {
        User crypt; //создаем объект пользователя, для того, чтобы зашифровать данные
        //если файл был успешно открыт, записываем данные. Все записываемые данные шифруются:
        file << crypt(QString::fromStdString(to_string(stack.size()))).toStdString() << endl; //первой строкой записываем размер cтека
        for (int i = 0; i < stack.size(); i++) {
            //запись информации о пользователи в файл
            User* user = static_cast<User*>(stack[i]);
            if (typeid(*user) == typeid(Account)) {
                file << *(static_cast<Account*>(user));
            } else {
                file << *user;
            }
        }
        file.close(); //закрываем файл
        mutex.unlock();
        QMessageBox error; //создаем всплывающее окно
        error.setIcon(QMessageBox::Information); //добавляем значок
        error.setText("<b>Изменения успешно сохранены!<\b>"); //задаем сообщение окна
        error.setInformativeText("Файл с сохраненным стеком (file.txt) находится в одном каталоге с программой.");
        error.exec(); //отображаем окно
    } else {
        mutex.unlock();
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
        error.setText("<b>Не удается открыть файл для записи!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Возможно, создание файла в данном разделе запрещено или файл защищен.");
        error.exec(); //отображаем окно
    }
}

void MainWindow::loadFromFile() {
    mutex.lock();
    ifstream file("file.txt"); //открытие файла для записи
    if (file.is_open()) {
        //если файл был успешно открыт:
        User decrypt; //создаем объект пользователя, для того, чтобы расшифровать данные
        int n; //переменная для хранения размера считываемых данных
        string tmp; //переменная для временного хранения считанных данных
        //для обработки возможных ошибок при чтении воспользуемся обработкой исключений:
        try {
            //перед считыванием каждого поля расшифровывам его
            getline(file, tmp); //считываем первую строку файла
            n = stoi(decrypt(QString::fromStdString(tmp)).toStdString()); //запоминаем количество считываемых данных
            for (int i = 0; i < n; i++) {
                if (stack.size() <= 100) {
                    getline(file, tmp); //считываем следующую строку
                    if (tmp == "#######") {
                        User* user = new User();
                        getline(file, tmp); //считываем следующую строку
                        user->setLogin(decrypt(QString::fromStdString(tmp)));
                        getline(file, tmp); //считываем следующую строку
                        user->setPassword(decrypt(QString::fromStdString(tmp)));
                        getline(file, tmp); //считываем следующую строку
                        user->setAge(decrypt(QString::fromStdString(tmp)).toInt());
                        if (firstUser == nullptr) {
                            firstUser = user; //запоминаем первый элемент стэка
                        }
                        stack.push(user); //добавляем считанныйе данные в стэк
                    } else {
                        Account* account = new Account();
                        account->setEmail(decrypt(QString::fromStdString(tmp)));
                        getline(file, tmp); //считываем следующую строку
                        account->setLogin(decrypt(QString::fromStdString(tmp)));
                        getline(file, tmp); //считываем следующую строку
                        account->setPassword(decrypt(QString::fromStdString(tmp)));
                        getline(file, tmp); //считываем следующую строку
                        account->setAge(decrypt(QString::fromStdString(tmp)).toInt());
                        if (firstUser == nullptr) {
                            firstUser = account; //запоминаем первый элемент стэка
                        }
                        stack.push(account); //добавляем считанныйе данные в стэк
                    }
                } else {
                    qDebug() << QString::fromStdString("Stack overloaded!");
                }
                mutex.unlock();
                printInfo();
                mutex.lock();
            }
            mutex.unlock();
            QMessageBox error; //создаем всплывающее окно ошибки
            error.setIcon(QMessageBox::Information); //добавляем значок ошибки
            error.setText("<b>Изменения успешно загружены!<\b>"); //задаем сообщение окна ошибки
            error.exec(); //отображаем окно
        } catch(...) {
            mutex.unlock();
            QMessageBox error; //создаем всплывающее окно ошибки
            error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
            error.setText("<b>Во время чтения файла возникла ошибка!<\b>"); //задаем сообщение окна ошибки
            error.exec(); //отображаем окно
        }
        file.close(); //закрываем файл
    } else {
        mutex.unlock();
        QMessageBox error; //создаем всплывающее окно ошибки
        error.setIcon(QMessageBox::Critical); //добавляем значок ошибки
        error.setText("<b>Не удается открыть файл для чтения!<\b>"); //задаем сообщение окна ошибки
        error.setInformativeText("Возможно, файл не существует или защищен.");
        error.exec(); //отображаем окно
    }
}

void MainWindow::autoAdd() {
    //список возможных имен для автоматически добавляемых пользователей:
    QString names[] = { "Irina", "Lucya", "Skyline", "Andrew", "Cowboy",
        "Kitty", "Accont", "User", "Niki Minaj", "VinDisel" };
    //список возможных паролей для автоматически добавляемых пользователей:
    QString passwords[] = { "password", "12345", "sdkfj3k23", "ksdjf444", "qwerty" };
    //список возможных email для автоматически добавляемых пользователей:
    QString emails[] = { "Irina@mail.ru", "Lucya@mail.ru", "Skyline@mail.ru", "Cowboy@mail.ru",
                        "Kitty@hse.ru", "Accont@hse.ru", "User@hse.ru", "NikiMinaj@hse.ru", "VinDisel@hse.ru" };
    mutex.lock();
    if (stack.size() <= 100) {
        int n = rand(); //получаем случайное число
        if (n%2 == 0) /* если полученное число четное */ {
            //создаем нового пользователя
            User *user = new User(names[n%10], passwords[n%5], n%120);
            if (firstUser == nullptr) {
                firstUser = user; //сохраняем первого пользователя
            }
            stack.push(user); //добавляем нового пользователя наверх стека
            qDebug() << QString::fromStdString("New user added!");
        } else {
            //создаем новый аккаунт
            Account *account = new Account(names[n%10], passwords[n%5], emails[n%9], n%120);
            if (firstUser == nullptr) {
                firstUser = account; //сохраняем первого пользователя
            }
            stack.push(account); //добавляем новый аккаунт наверх стека
            qDebug() << QString::fromStdString("New account added!");
        }
    } else {
        qDebug() << QString::fromStdString("Stack overloaded!");
    }
    mutex.unlock();
    printInfo(); //вывод текущего состояния стека
}

void MainWindow::autoDelete() {
    mutex.lock();
    if (stack.size() != 0) {
        User **user = stack.pop(); //получение удаляемого элемента стэка и удаление из стэка
        if (stack.size() == 0 && firstUser != nullptr) {
            delete firstUser; //удаление первого добавленного пользователя
            firstUser = nullptr;
            qDebug() << QString::fromStdString("The last one.");
        } else {
            delete *user;
        }
        delete user;
        qDebug() << QString::fromStdString("User deleted!");
    } else {
        qDebug() << QString::fromStdString("Stack is empty!");
    }
    mutex.unlock();
    printInfo(); //выводим информацию, а не ошибку.
}

void MainWindow::testsFinished() {
    QMessageBox window; //создаем всплывающее окно
    window.setIcon(QMessageBox::Information); //добавляем значок
    window.setText("<b>Тесты завершены успешно!<\b>"); //задаем сообщение окна
    window.exec(); //отображаем окно
}
