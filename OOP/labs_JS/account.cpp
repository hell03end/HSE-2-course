#include "account.h"

Account::Account() : User() /* вызов конструктора по умолчанию для родительского класса */ {
    email = QString::fromStdString("example@mail.ru"); //задание значения электронной почты по умолчанию
}
Account::Account(const QString &login, const QString &password, const QString &email, int age) : User(login, password, age) {
    setEmail(email); //задание электронной почты с проверкой ввода
}
Account::~Account() {
    //все поля класса статичны, удалять нечего
    //перед вызовом данного деструктора вызывается деструктор базового класса
}
Account::Account(const Account &account) {
    //копируем все поля переданного объекта
    login = account.login; //копирование имени
    password = account.password; //копирование пароля
    age = account.age; //копирование возраста
    email = account.email; //копирование email-a
}
void Account::setEmail(const QString &email) {
    //проверка на корректность адресса электронной почты
    bool a = false; //наличие символа @
    bool a_unique = true; //символ @ встречается только один раз
    bool dot = false; //наличие точки
    for (auto c : email) {
        if (c == '@' && a_unique && !a) {
            a = true;
        } else if (c == '@' && a) {
            a_unique = false;
        } else if (c == '.') {
            dot = true;
        }
    }
    this->email = (a && a_unique && dot && email.size() > 5) ? email : QString::fromStdString("example@mail.ru");
}
QString Account::getEmail() const {
    return email;
}
void Account::setLogin(const QString &login) {
    if (login.size() < 2) /*Проверка ввода на минимальную длину*/ {
        this->login = QString::fromStdString("hidden login"); //стандартное имя пользователя
    } else {
        this->login = login; //заданное имя
    }
}
void Account::setPassword(const QString &password) {
    if (password.size() < 3) /*Проверка ввода на минимальную длину*/ {
        this->password = crypt(QString("PaS3w0rD")); //стандартный пароль (шифруется)
    } else {
        this->password = crypt(password); //заданный пароль (шифруется)
    }
}
Account &Account::operator=(const Account &account) {
    if (this != &account) /* проверка равенства адрессов текущего объекта и переданного */ {
        /* если адресса не равны, то объекты тоже, поэтому удаляем исходный объект и заменяем новым */
        age = account.age; //копируем возраст
        login = account.login; //копируем имя
        password = account.password; //копируем пароль
        email = account.email; //копируем адрес электронной почты
    }

    return *this; //возвращаем ссылку на получившийся объект (текущий)
}
Account::operator User() const {
    User user(login, password, age); //создаем объект родительского класса с текущими полями
    return user; //возвращаем его
}
