#ifndef THREADS_H
#define THREADS_H

#include <QThread> //подключаем библиотеку потоков Qt
#include <random> //для случайного интервала времени добавления
#include <ctime> //для задания семени генератора случайных чисел
#include <QDebug> //для вывода сообщений о действиях в потоке

using namespace std;

class Adder : public QThread {
    Q_OBJECT //для корректной работы потоков в Qt
public:
    void run()/* основной цикл потока */ {
        qDebug() << QString("Starting of thread of adding...\n");
        srand(time(0)); //задаем семя генератора случайных чисел
        for (int i = 0; i < 1000; i++) {
            int n = rand(); //получаем случайное число
            qDebug() << QString::fromStdString("Adder thread sleep to " + to_string(n%13) +  "s.\n");
            QThread::sleep(n%13); //останавливаем программу на какое-то время
            emit(addUser()); //посылаем сигнал о добавлении нового пользователя в стэк
            qDebug() << QString("Add new user...\n");
        }
    }
signals:
    void addUser(); //сигнал о добавлении нового пользователя в очередь
};

class Remover : public QThread {
    Q_OBJECT //для корректной работы потоков в Qt
public:
    void run() /* основной цикл потока */ {
        qDebug() << QString("Starting of thread of deleting...\n");
        srand(time(0)+100); //задаем семя генератора случайных чисел
        for (int i = 0; i < 1000; i++) {
            int n = rand(); //получаем случайное число
            qDebug() << QString::fromStdString("Remover thread sleep to " + to_string(n%21) +  "s.\n");
            sleep(n%21); //останавливаем программу на какое-то время
            emit(removeUser()); //посылаем сигнал о удалении пользователя
            qDebug() << QString("Delete user...\n");
        }
    }
signals:
    void removeUser(); //сигнал о удалении пользователя из стэка
};

#endif // THREADS_H
