#include "user.h"

User::User() {
    login = QString::fromStdString("login"); //стандартный логин
    password = crypt(QString::fromStdString("password")); //стандартный пароль (хранится в зашифрованном виде)
}
User::User(const QString &login, const QString &password, int age) {
    setLogin(login); //задание имени пользователя с проверкой ввода
    setPassword(password); //задание пароля с проверкой ввода
    setAge(age); //задание возраста пользователя с проверкой ввода
}
User::~User() {
    //все поля класса статичны, удалять нечего
}
User::User(const User &user) {
    //копируем все поля переданного объекта
    login = user.login; //копирование имени пользователя
    password = user.password; //копирование пароля
    age = user.age; //копирование возраста пользователя
}
void User::setLogin(const QString &login) {
    if (login.size() < 2) /*Проверка ввода на минимальную длину*/ {
        this->login = QString::fromStdString("login"); //стандартное имя пользователя
    } else {
        this->login = login; //заданное имя
    }
}
void User::setPassword(const QString &password) {
    if (password.size() < 3) /*Проверка ввода на минимальную длину*/ {
        this->password = crypt(QString("password")); //стандартный пароль (шифруется)
    } else {
        this->password = crypt(password); //заданный пароль (шифруется)
    }
}
void User::setAge(int age) {
    if (age < 0 || age > 115) /* проверка возраста на корректность */ {
        this->age = 0; //возраст пользователя не определен
    } else {
        this->age = age;
    }
}
QString User::getLogin() const {
    return login; //возвращает имя пользователя (не нуждается в проверках)
}
QString User::getPassword(bool decrypt) const {
    if (decrypt) {
        return crypt(password); //возвращает расшифрованный пароль
    }
    return password; //возвращает зашифрованный пароль
}
int User::getAge() const {
    return age; //возвращает возраст пользователя (не нуждается в проверках)
}
User &User::operator=(const User &user) {
    if (this != &user) /* проверка равенства адрессов текущего объекта и переданного */ {
        /* если адресса не равны, то объекты тоже, поэтому удаляем исходный объект и заменяем новым */
        login = user.login; //копируем имя клиента
        password = user.password; //копируем пароль
        age = user.age; //копируем возраст
    }

    return *this; //возвращаем ссылку на получившийся объект (текущий)
}
QString User::operator ()(const QString &str) const {
    return crypt(str);
}
int User::operator +(int n) {
    age += (n >= 0) ? n : -n; //модуль числа
    if (age > 115) /* возраст выходит за границы допустимого диапазона */ {
        age = 0; //возраст неопределен
    }
    return age;
}
int User::operator -(int n) {
    age -= (n >= 0) ? n : -n; //модуль числа
    if (age < 0) /* возраст выходит за границы допустимого диапазона */ {
        age = 0; //возраст неопределен
    }
    return age;
}
int User::operator *(int n) {
    age *= (n >= 0) ? n : -n; //модуль числа
    if (age > 115) /* возраст выходит за границы допустимого диапазона */ {
        age = 0; //возраст неопределен
    }
    return age;
}
int User::operator /(int n) {
    age /= (n >= 0) ? n : -n; //модуль числа
    if (age > 0) /* возраст выходит за границы допустимого диапазона */ {
        age = 0; //возраст неопределен
    }
    return age;
}
int User::operator ^(int n) {
    n = (n >= 0) ? n : -n; //модуль числа
    int t = age;
    //возводим в степень:
    for (int i = 0; i < n; i++) {
        age *= t;
    }
    if (age > 115 || age < 0) /* возраст выходит за границы допустимого диапазона */ {
        age = 0; //возраст неопределен
    }
    return age;
}
int User::operator ++() {
    age += 1;
    if (age > 115) /* возраст выходит за границы допустимого диапазона */ {
        age = 0; //возраст неопределен
    }
    return age;
}
int User::operator ++(int t) {
    t = age;
    age += 1;
    if (age > 115) /* возраст выходит за границы допустимого диапазона */ {
        age = 0; //возраст неопределен
    }
    return t;
}
int User::operator --() {
    age -= 1;
    if (age < 0) /* возраст выходит за границы допустимого диапазона */ {
        age = 0; //возраст неопределен
    }
    return age;
}
int User::operator --(int t) {
    age = t;
    age -= 1;
    if (age < 0) /* возраст выходит за границы допустимого диапазона */ {
        age = 0; //возраст неопределен
    }
    return t;
}
bool User::operator ==(const User &user) const {
    //проверка равенства полей
    return login == user.login && password == user.password && age == user.age;
}
bool User::operator !=(const User &user) const {
    //оператор, обратный оператору ==
    return !(*this == user);
}
bool User::operator >(const User &user) const {
    return login > user.login && password > user.password && age > user.age;
}
bool User::operator >=(const User &user) const {
    return login >= user.login && password >= user.password && age > user.age;
}
bool User::operator <(const User &user) const {
    return !(*this >= user);
}
bool User::operator <=(const User &user) const {
    return !(*this > user);
}
QString User::crypt(const QString &message) const {
    char key = '#'; //ключ шифрования
    string result = "";
    string _message = message.toStdString();
    for (auto c : _message) {
        result += c ^ key; //обрабатывая каждую букву исходного слова в цике, добавляем ее в результат
    }
    return QString::fromStdString(result); //возврат зашифрованной/дешифрованной строки
}
