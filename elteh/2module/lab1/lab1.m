Ube1 = load('1Ube.txt');
Ube1 = sort(Ube1(:));
Ib1 = load('1Ib.txt');
Ib1 = sort(Ib1(:));
Ik1 = load('1Ik.txt');
Ik1 = sort(Ik1(:));

Uke23 = load('2Ubk30.txt');
Uke23 = sort(Uke23(:));
Uke25 = load('2Ubk50.txt');
Uke25 = sort(Uke25(:));
Uke27 = load('2Ubk70.txt');
Uke27 = sort(Uke27(:));
Ik23 = load('2Ik30.txt');
Ik23 = sort(Ik23(:));
Ik25 = load('2Ik50.txt');
Ik25 = sort(Ik25(:));
Ik27 = load('2Ik70.txt');
Ik27 = sort(Ik27(:));

subplot(2,2,1);
plot(Ube1, Ib1, 'b', Ube1, Ik1, 'r');
xlabel('Ube, V');
ylabel('I, uA');
legend('Ib', 'Ik', 'Location', 'Best');
title('Ib = f(Ube)');
grid on;
subplot(2,2,2);
semilogy(Ube1, Ib1, 'b', Ube1, Ik1, 'r');
xlabel('Ube, V');
ylabel('I, uA');
legend('Ib', 'Ik', 'Location', 'Best');
title('Ib = log(f(Ube))');
grid on;

subplot(2,2,3);
plot(Uke23, Ik23, 'b', Uke25, Ik25, 'r', Uke27, Ik27, 'g');
xlabel('Uke, V');
ylabel('Ik, mA');
legend('Ib = 30mA', 'Ib = 50mA', 'Ib = 70mA', 'Location', 'Best');
title('Ik = f(Uke)');
grid on;
subplot(2,2,4);
semilogy(Uke23, Ik23, 'b', Uke25, Ik25, 'r', Uke27, Ik27, 'g');
xlabel('Uke, V');
ylabel('Ik, mA');
legend('Ib = 30mA', 'Ib = 50mA', 'Ib = 70mA', 'Location', 'Best');
title('Ik = log(f(Uke))');
grid on;


Ube3 = 0.39;
Ik3 = 37.3*10^-6;
Ib3 = 1.15*10^-6;
nf = 1.53174
Is = -0.999997*10^-6
rb = (Ube3 - 0.026*nf*log(Ik3/-Is + 1))/Ib3
bf = max(Ik1./Ib1)

dUke13 = 0.11 - 0.09;
dIk13 = 1.001 - 0.65;
dIk3 = 2.415;
dUke23 = 4.13 - 3.63;
dIk23 = 2.435 - dIk3;
Rc3 = dUke13/dIk13
Vaf3 = dUke23*dIk3/dIk23

dUke15 = 0.1 - 0.085;
dIk15 = 1.56 - 1.165;
dIk5 = 4.60;
dUke25 = 1.5 - 1;
dIk25 = 4.66 - dIk5;
Rc5 = dUke15/dIk15
Vaf5 = dUke25*dIk5/dIk25

dUke17 = 0.11 - 0.099;
dIk17 = 2.97 - 2.38;
dIk7 = 7.2;
dUke27 = 1.72 - 1.1;
dIk27 = 7.33 - dIk7;
Rc7 = dUke17/dIk17
Vaf7 = dUke27*dIk7/dIk27
