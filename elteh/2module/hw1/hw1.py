import re

with open("a.txt") as fl:
	data = fl.read()

lines = [line for line in data.split('\n')]
clr_lines = []
for line in lines:
	clr_lines.append(re.sub(r"([\s]+)", ", ", line))

while len(clr_lines) < 300:
	clr_lines.append("")

sep = "; "
with open("b.txt", "w") as fl:
	for i in range(60):
		for j in range(5):
			if j != 0:
				fl.write(sep)
			fl.write("{:>23}".format(clr_lines[i+60*j]))
		fl.write('\n')
