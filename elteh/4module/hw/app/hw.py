from argparse import ArgumentParser
import os
import logging
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re
import sys
from time import time

plt.rcParams['figure.figsize'] = (10, 6)
plt.rc('font', family='Arial')
np.random.seed(123)
logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s"
                           " - %(message)s", level=logging.ERROR)


def change_commas_to_dots(file_path: str) -> None:
    content = ""
    with open(file_path, 'r') as reader:
        for line in reader.readlines():
            content += re.sub(r",", r".", line)
    with open(file_path, 'w') as writer:
        _ = writer.write(content)


def load_data(file_path: str, sep='\n', columns=['sample'],
              dtype=float) -> pd.DataFrame():
    change_commas_to_dots(file_path)
    df = pd.read_csv(rf"{file_path}", sep=sep, names=columns, dtype=dtype)
    if df.size < 4:
        logging.error(f"Lack of samples ({df.size} < 4).")
        return pd.DataFrame()
    elif df.size > 15:
        logging.error(f"Too many samples ({df.size} > 15).")
        print(df)
        return pd.DataFrame()
    return df


def get_grabbs(q: int, n: int, file_path=r"rsc/grabbs.csv") -> float:
    grabbs_coeffs = pd.read_csv(file_path, index_col=0, dtype=float)
    Gt = None
    for i in range(10):
        try:
            Gt = grabbs_coeffs.loc[n + i, str(q)]
            break
        except KeyError:
            pass
    return Gt


def get_student(p: int, n: int, file_path=r"rsc/student.csv") -> float:
    student_coeffs = pd.read_csv(file_path, index_col=0, dtype=float)
    t = None
    for i in range(10):
        try:
            t = student_coeffs.loc[n + i, str(p)]
            break
        except KeyError:
            pass
    return t


def get_k_coeffs(file_path=r"rsc/k.csv") -> np.ndarray:
    return pd.read_csv(file_path, index_col=0, dtype=float).values


def remove_noise(y, y_mean: float, y_std: float, y_mean_std: float,
                 Gt: float) -> tuple:
    y_ = list(y.copy())
    G1 = abs(np.max(y_) - y_mean) / y_std
    G2 = abs(y_mean - np.min(y_)) / y_std

    while G1 > Gt or G2 > Gt:
        if G1 > Gt:
            y_.remove(np.max(y_))
        else:
            y_.remove(np.min(y_))
        y_mean = np.mean(y_)
        y_std = np.std(y_, ddof=1.5)
        y_mean_std = y_std / np.sqrt(len(y_))
        G1 = abs(np.max(y_) - y_mean) / y_std
        G2 = abs(y_mean - np.min(y_)) / y_std
    return np.array(y_), y_mean, y_std, y_mean_std


def get_file_path(file_path=None) -> str:
    while not file_path or not os.path.exists(file_path):
        file_path = input("Введите путь к измерениям (.txt, .csv):\t")
    return file_path


def print_results(y_mean: float, delta: float, p: int, y_mean_std: float=None,
                  n: int=None, const_error: float=None) -> int:
    r = 1  # accuracy
    if int(y_mean*10%10) <= 3:
        r = 2

    if not y_mean_std:
        print(f"Результаты:\t{y_mean:.{r}f} ± {delta:.{r}f}, {p}\n")
        return r
    print(f"Результаты:\t{y_mean:.{r}f}, {y_mean_std:.{r}f}, {n}, "
          f"{const_error:.{r}f}")
    return r


def plot_results(x, x_mean:float, delta:float) -> None:
    n = x.size
    _ = plt.plot(np.arange(0, n), x, 'o-')
    _ = plt.plot(np.arange(0, n), [x_mean] * n, '--')
    _ = plt.fill_between(np.arange(0, n), x_mean - delta,
                         x_mean + delta, alpha=0.1, color='r')
    _ = plt.legend(["Результаты измерений",
                    "Среднее значение измеряемой величины",
                    "Доверительные границы погрешности"])
    plt.show()


def parse_argv():
    parser = ArgumentParser(description="")
    parser.add_argument('--path', '-P', type=str, default="data.txt",
                        help="Путь к данным о измерениях (.txt, .csv)")
    parser.add_argument('-q', type=int, default=1, help="Уровень значимости q")
    parser.add_argument('-p', type=int, default=95,
                        help="Доверительная вероятность p")
    parser.add_argument('--systematic_error', default=[],
                        help="Систематические погрешности")
    parser.add_argument('--plot', '-g', type=bool, default=False,
                        help="Печать графика")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_argv()
    file_path = get_file_path(args.path)
    measurements = load_data(file_path)
    if not measurements.size:
        sys.exit(1)

    x = measurements.values
    x_mean = np.mean(x)
    x_std = np.std(x, ddof=1)

    Gt = get_grabbs(args.q, x.size)
    t = get_student(args.p, x.size)
    K = get_k_coeffs()

    systematic_errors = args.systematic_error
    a = np.sum(systematic_errors)  # где a - систематическая ошибка
    y = x - a  # внесение поправки
    y_mean = x_mean - a  # внесение поправки в оценку измеряемой величины
    y_std = np.std(x, ddof=1.5)  # смещение среднеквадратического отклонения

    x_mean_std = x_std / np.sqrt(x.size)
    y_mean_std = y_std / np.sqrt(y.size)

    y, y_mean, y_std, y_mean_std = \
        remove_noise(y, y_mean, y_std, y_mean_std, Gt)

    conf_limits = t * y_mean_std

    if len(systematic_errors) < 3:
        const_error = np.sum(np.abs(systematic_errors))
        systematic_bias = const_error / np.sqrt(3)
    else:
        if args.p == 95:
            k = 1.1
        elif len(systematic_errors) > 4:
            k = 1.4
        else:
            l = int(abs(np.max(systematic_errors) / np.min(systematic_errors)))
            k = float(K[l])  # приблизительное значение
        const_error = k * np.sqrt(np.sum(np.square(systematic_errors)))
        systematic_bias = const_error / (k * np.sqrt(3))

    y_sum_std = np.sqrt(np.square(systematic_bias) + np.square(y_mean_std))
    coeff_k = (conf_limits + const_error) / (y_mean_std + systematic_bias)
    delta = coeff_k * y_sum_std

    accuracy = print_results(y_mean, delta, args.p)
    if args.plot:
        plot_results(y, y_mean, delta)

    log = \
f'''Критическое значение критерия Граббса:          {Gt:.4f} 
Значение коэффициента Стьюдента t:              {t:.4f}

Математическое ожидание измерений:              {x_mean:.4f} 
Среднеквадратическое отклонение:                {x_std:.4f} 
Несмещенное математическое ожидание измерений:  {y_mean:.4f} ({abs(x_mean - y_mean):.4f}) 
Несмещенное среднеквадратическое отклонение:    {y_std:.4f} ({abs(x_std - y_std):.4f})
Среднеквадратическое отклонение оценки:         {x_mean_std:.4f} 
Несмещенное отклонение среднего значения:       {y_mean_std:.4f} ({abs(x_mean_std - y_mean_std):.4f})
Удалено элементов:                              {y.size - x.size}
Доверительные границы:                          {conf_limits:.4f}
Доверительные границы НСП:                      {const_error:.4f}
Среднеквадратическое НСП:                       {systematic_bias:.4f} 
Суммарное среднеквадратическое отклонение:      {y_sum_std:.4f} 
Коэффициент K:                                  {coeff_k:.4f}
Доверительные границы погрешности оценки:       {delta:.4f}

Результаты:
            {y_mean:.{accuracy}f} ± {delta:.{accuracy}f}, {args.p}
            {y_mean:.{accuracy}f}, {y_mean_std:.{accuracy}f}, {y.size}, {const_error:.{accuracy}f}
    '''


    with open(
        re.sub('\W', '', rf"LOG_{file_path}_{time()}")+".txt", 
        'w', 
        encoding='utf-8'
    ) as writer:
        writer.write(log)
