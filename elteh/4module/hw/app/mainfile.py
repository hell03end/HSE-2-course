from argparse import ArgumentParser
import os
import logging
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re
import sys
import time

plt.rcParams['figure.figsize'] = (10, 6)
plt.rc('font', family='Arial')
np.random.seed(123)
logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s"
                           " - %(message)s", level=logging.ERROR)


def change_commas_to_dots(file_path: str) -> None:
    content = ""
    with open(file_path, 'r') as reader:
        for line in reader.readlines():
            content += re.sub(r",", r".", line)
    with open(file_path, 'w') as writer:
        _ = writer.write(content)


def load_data(file_path: str, sep='\n', columns=['sample'],
              dtype=float) -> pd.DataFrame():
    change_commas_to_dots(file_path)
    df = pd.read_csv(rf"{file_path}", sep=sep, names=columns, dtype=dtype)
    if df.size < 4:
        logging.error(f"Lack of samples ({df.size} < 4).")
        return pd.DataFrame()
    elif df.size > 15:
        logging.error(f"Too many samples ({df.size} > 15).")
        print(df)
        return pd.DataFrame()
    return df


def get_grabbs(q: int, n: int, file_path=r"rsc/grabbs.csv"):
    if not (os.path.exists(file_path) and os.path.isfile(file_path)):
        logging.error(f"Path {file_path} isn't exist.\nCan't load grabbs.")
        return
    grabbs_coeffs = pd.read_csv(file_path, index_col=0, dtype=float)
    Gt = None
    for i in range(10):
        try:
            Gt = grabbs_coeffs.loc[n + i, str(q)]
            break
        except KeyError:
            pass
    return Gt


def get_student(p: int, n: int, file_path=r"rsc/student.csv"):
    if not (os.path.exists(file_path) and os.path.isfile(file_path)):
        logging.error(f"Path {file_path} isn't exist.\nCan't load student.")
        return
    student_coeffs = pd.read_csv(file_path, index_col=0, dtype=float)
    t = None
    for i in range(10):
        try:
            t = student_coeffs.loc[n + i, str(p)]
            break
        except KeyError:
            pass
    return t


def get_k_coeffs(file_path=r"rsc/k.csv") -> pd.DataFrame:
    if not (os.path.exists(file_path) and os.path.isfile(file_path)):
        logging.error(f"Path {file_path} isn't exist.\nCan't load K coeffs.")
        return pd.DataFrame()
    return pd.read_csv(file_path, index_col=0, dtype=float).values


def remove_noise(y, y_mean: float, y_std: float, y_mean_std: float,
                 Gt: float) -> tuple:
    y_ = list(y.copy())
    G1 = abs(np.max(y_) - y_mean) / y_std
    G2 = abs(y_mean - np.min(y_)) / y_std

    while G1 > Gt or G2 > Gt:
        if G1 > Gt:
            y_.remove(np.max(y_))
        else:
            y_.remove(np.min(y_))
        y_mean = np.mean(y_)
        y_std = np.std(y_, ddof=1.5)
        y_mean_std = y_std / np.sqrt(len(y_))
        G1 = abs(np.max(y_) - y_mean) / y_std
        G2 = abs(y_mean - np.min(y_)) / y_std
    return np.array(y_), y_mean, y_std, y_mean_std


def get_file_path(file_path=None) -> str:
    while not file_path or not os.path.exists(file_path):
        file_path = input("Введите путь к измерениям (.txt, .csv):\t")
    return file_path


def form_results(y_mean: float, delta: float, p: int, y_mean_std: float = None,
                 n: int = None, const_error: float = None) -> str:
    r = max(len(parse_digit(y_mean - int(y_mean), 2)) - 2,
            len(parse_digit(delta - int(delta), 2)) - 2)
    if not y_mean_std:
        return f"{y_mean:.{r}f} ± {delta:.{r}f}, P={p}\n"
    return f"{y_mean:.{r}f}, {y_mean_std:.{r}f}, {n}, {const_error:.{r}f}"


def plot_results(x, x_mean: float, delta: float) -> None:
    plt.figure()
    n = x.size
    _ = plt.plot(np.arange(0, n), x, 'o-')
    _ = plt.plot(np.arange(0, n), [x_mean] * n, '--')
    _ = plt.fill_between(np.arange(0, n), x_mean - delta,
                         x_mean + delta, alpha=0.1, color='r')
    _ = plt.legend(["Результаты измерений",
                    "Среднее значение измеряемой величины",
                    "Доверительные границы погрешности"])
    plt.show()


def calculations(data, q: float, p: float, errors, log: str) -> str:
    x = data.values
    n = x.size
    x_mean = np.mean(x)
    x_std = np.std(x, ddof=1)

    Gt = get_grabbs(q, n)
    if not Gt:
        raise RuntimeError("Can't load grabbs!")
    t = get_student(p, n)
    if not t:
        raise RuntimeError("Can't load student!")
    K = get_k_coeffs()
    if not K.size:
        raise RuntimeError("Can't load K coeffs!")

    systematic_errors = errors  # get_systematic_error
    a = np.sum(systematic_errors)  # где a - систематическая ошибка
    y = x - a  # внесение поправки
    y_mean = x_mean - a  # внесение поправки в оценку измеряемой величины
    y_std = np.std(x, ddof=1.5)  # смещение среднеквадратического отклонения

    x_mean_std = x_std / np.sqrt(x.size)
    y_mean_std = y_std / np.sqrt(y.size)

    y, y_mean, y_std, y_mean_std = remove_noise(y, y_mean, y_std, y_mean_std, Gt)

    conf_limits = t * y_mean_std

    if len(systematic_errors) < 3:
        const_error = np.sum(np.abs(systematic_errors))
        systematic_bias = const_error / np.sqrt(3)
    else:
        if p == 95:
            k = 1.1
        elif len(systematic_errors) > 4:
            k = 1.4
        else:
            l = int(abs(np.max(systematic_errors) / np.min(systematic_errors)))
            k = float(K[l])  # приблизительное значение
        const_error = k * np.sqrt(np.sum(np.square(systematic_errors)))
        systematic_bias = const_error / (k * np.sqrt(3))

    y_sum_std = np.sqrt(np.square(systematic_bias) + np.square(y_mean_std))
    coeff_k = (conf_limits + const_error) / (y_mean_std + systematic_bias)
    delta = coeff_k * y_sum_std

    log += \
        f"Критическое значение критерия Граббса:              {parse_digit(Gt)}\n" \
        f"Значение коэффициента Стьюдента t:                  {parse_digit(t)}\n" \
        f"Число измерений:                                    {parse_digit(n)}\n" \
        f"Оценка измерений:                                   {parse_digit(x_mean)}\n" \
        f"Среднеквадратическое отклонение измерений:          {parse_digit(x_std)}\n" \
        f"Несмещенная оценка измерений:                       {parse_digit(y_mean)}\n" \
        f"\tВеличина смещения:\t{parse_digit(abs(x_mean - y_mean))}\n" \
        f"Несмещенное среднеквадратическое отклонение:        {parse_digit(y_std)}\n" \
        f"\tВеличина смещения:\t{parse_digit(abs(x_std - y_std))}\n" \
        f"Среднеквадратическое отклонение оценки:             {parse_digit(x_mean_std)}\n" \
        f"Несмещенное среднеквадратическое отклонение оценки: {parse_digit(y_mean_std)}\n" \
        f"\tВеличина смещения:\t{parse_digit(abs(x_mean_std - y_mean_std))}\n" \
        f"Удалено элементов(как шум):                         {parse_digit(y.size - x.size)}\n" \
        f"Доверительные границы:                              {parse_digit(conf_limits)}\n" \
        f"Доверительные границы НСП:                          {parse_digit(const_error)}\n" \
        f"Среднеквадратическое отклонение НСП:                {parse_digit(systematic_bias)}\n" \
        f"Суммарное среднеквадратическое отклонение:          {parse_digit(y_sum_std)}\n" \
        f"Коэффициент K:                                      {parse_digit(coeff_k)}\n" \
        f"Доверительные границы погрешности оценки:           {parse_digit(delta)}\n"

    return y, y_mean, delta, y_mean_std, const_error, log


def save_log(log: str, file_name: str, file_path: str = None) -> None:
    if not file_path:
        file_path = re.sub('\W', '', rf"LOG_{file_name}_{time.ctime()}") + ".txt"
    with open(file_path, 'w', encoding='utf-8') as writer:
        writer.write(log)


def parse_argv():
    parser = ArgumentParser(description="")
    parser.add_argument('--path', '-P', type=str, default="data.txt",
                        help="Путь к данным о измерениях (.txt, .csv)")
    parser.add_argument('-q', type=int, default=1, help="Уровень значимости q")
    parser.add_argument('-p', type=int, default=95,
                        help="Доверительная вероятность p")
    parser.add_argument('--systematic_error', default=[],
                        help="Систематические погрешности")
    parser.add_argument('--plot', '-g', type=bool, default=False,
                        help="Печать графика")
    parser.add_argument('--log', '-l', type=bool, default=True,
                        help="Сохранение результатов в файл")
    return parser.parse_args()


def parse_digit(digit: float, precision: int=4) -> str:
    if int(digit) == digit or precision < 1:
        return str(digit)
    tens = 0
    buf = digit
    while buf:
        buf -= int(buf)
        buf *= 10
        tens += 1
        if int(buf):
            break
    if precision != 2 or int(buf) <= 3:
        tens += precision - 1
    try:
        return f"{digit:.{tens}f}"
    except TypeError:
        return str(digit)


if __name__ == "__main__":
    LOG = ""
    args = parse_argv()
    file_path = get_file_path(args.path)
    measurements = load_data(file_path)
    if not measurements.size:
        sys.exit(1)

    y, y_mean, delta, y_mean_std, const_error, LOG = \
        calculations(measurements, args.q, args.p, args.systematic_error, LOG)

    result = form_results(y_mean, delta, args.p)
    if args.plot:
        plot_results(y, y_mean, delta)

    LOG += f"\nРезультат:\t{result}"
    print(LOG)
    if args.log:
        save_log(LOG, file_path)
