import sys
from qtpy import QtWidgets
from app import mainwindow

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = mainwindow.Ui_MainWindow()
    sys.exit(app.exec_())
