;compare variables
;Pchelkin Dmitriy, BIV-151.
.386
	.model flat, stdcall
	option casemap :none
	include C:\masm32\include\windows.inc
	include C:\masm32\include\user32.inc
	include C:\masm32\include\kernel32.inc
	includelib C:\masm32\lib\user32.lib
	includelib C:\masm32\lib\kernel32.lib

.data
	VAR0 DD 7 ;pattern variable
	VAR1 DD 1
	VAR2 DD 2
	VAR3 DD 7

	s_title db "Pchelkin Dmitriy, BIV-151",0
	strV11 db "VAR1 == VAR0",0
	strV10 db "VAR1 != VAR0",0
	strV21 db "VAR2 == VAR0",0
	strV20 db "VAR2 != VAR0",0
	strV31 db "VAR3 == VAR0",0
	strV30 db "VAR3 != VAR0",0

.code
start:
	MOV EAX,VAR0
	CMP EAX,VAR1
	JE ELSE1
        invoke MessageBox, NULL, addr strV10, addr s_title, MB_OK
        JMP NEXT1
    ELSE1: 	
        invoke MessageBox, NULL, addr strV11, addr s_title, MB_OK
    NEXT1: 	
    MOV EAX,VAR0
	CMP EAX,VAR2
	JE ELSE2
        invoke MessageBox, NULL, addr strV20, addr s_title, MB_OK
	    JMP NEXT2
    ELSE2: 	
        invoke MessageBox, NULL, addr strV21, addr s_title, MB_OK
    NEXT2: 	
    MOV EAX,VAR0
	CMP VAR3,EAX
	JE ELSE3
    	invoke MessageBox, NULL, addr strV30, addr s_title, MB_OK
    	JMP NEXT3
    ELSE3: 
        invoke MessageBox, NULL, addr strV31, addr s_title, MB_OK
    NEXT3:
 	invoke ExitProcess, NULL
    end start
