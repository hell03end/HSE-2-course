#include <stdio.h>

int main(int argc, char const *argv[]) {
    float f = -0.1015625f;
    printf("Please, enter a float number:\t");
    scanf("%f", &f);
    unsigned char *p = (unsigned char *) &f;
    printf("%02x %02x %02x %02x\n", 
           (unsigned int) p[0],
           (unsigned int) p[1],
           (unsigned int) p[2],
           (unsigned int) p[3]);
    getchar(); getchar();
    return 0;
}