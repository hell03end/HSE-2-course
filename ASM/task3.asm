;floating numbers.
; A = -9112.31 
; -10001110011000.01001111010 = -1.000111001100001001111010*2^13 (13 = +1101)
; float:        13 + 127 = 140 = +10001100
;               ,00011100110000100111101
;               1.10001100.00011100110000100111101
;               C60E613D -> 3D610EC6
; double:       13 + 1023 = 1026 = +10000000010
;               ,0001110011000010011110100000000000000000000000000000
;               1.10000000010.0001110011000010011110100000000000000000000000000000
;               C021CC27A0000000 -> 000000A027CC21C0
; long double:  13 + 16383 = 16396 = +100000000001100
;               1,000111001100001001111010000000000000000000000000000000000000000
;               1.100000000001100.1000111001100001001111010000000000000000000000000000000000000000
;               C00C8E613D0000000000 -> 00000000003D618E0CC0
;indents for logical code blocks are like in Python.
;Pchelkin Dmitriy, BIV-151.
.386
 	.model flat, stdcall
 	option casemap :none ;case sensitive
 	include C:\masm32\include\windows.inc
 	include C:\masm32\include\user32.inc
    include C:\masm32\include\kernel32.inc
 	include C:\masm32\include\fpu.inc
 	includelib C:\masm32\lib\user32.lib
    includelib C:\masm32\lib\kernel32.lib
 	includelib C:\masm32\lib\fpu.lib

.data
	a DD +3.0
	b DD +4.0
    two DD +2.0
    ans DD 0

.code
start:
    FLD a ;a -> ST(0)
    FMUL b ;a*b -> ST(0)
    FLDPI ;ST(0) -> ST(1), pi -> ST(0)
    FDIV two;pi/2. = ST(0)/2. -> ST(0)
    FSIN ;sin(pi/2.) = sin(ST(0)) -> ST(0)
    FMUL ;a*b*sin(pi/2) = ST(1)*ST(0) -> ST(0)
    FDIV two ;1/2*a*b*sin(pi/2) = ST(0)*2. -> ST(0)
    FSTP ans ;ST(0) -> ans

    invoke ExitProcess, NULL
    end start
