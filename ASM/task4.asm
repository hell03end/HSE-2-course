;D = A*3 + B, A,B - packed BCD numbers with different lengths.
;Pchelkin Dmitriy, BIV-151.
.386
 	.model flat, stdcall
 	option casemap :none ;case sensitive
 	include C:\masm32\include\windows.inc
 	include C:\masm32\include\user32.inc
    include C:\masm32\include\kernel32.inc
 	includelib C:\masm32\lib\user32.lib
    includelib C:\masm32\lib\kernel32.lib

.data
    ;A db 99h, 99h
    A db 88h, 19h ;packed 1988
    len_A = $ - A
    unp_A db 4 dup(0)
    len_unp_A = $ - unp_A
    ;B db 99h, 99h, 99h
    B db 84h, 19h, 07h ;packed 71984
    len_B = $ - B
    unp_B db 6 dup(0)
    len_unp_B = $ - unp_B
    duo db 2
    tre db 3
    D db 7 dup(0) ;sum, len(sum) = max(len_A, len_B) + 1
    len_D = $ - D
    s_title db "Pchelkin Dmitriy, BIV-151",0
    s_in db "Given numbers:",10,"A = 1988, B = 71984",0
    s_out db "D = A*3 + B = 0000000",0

.code
unpuck:
    MOV AL,DL ;lower part of number
    MOV BL,AL ;higher part of number
    AND AL,00001111b ;remove higher part of number
    AND BL,11110000b ;remove lower part of number
    SHR BL,4 ;shift left (to lower part)
    ret

start:
    invoke MessageBox, NULL, addr s_in, addr s_title, MB_OK
    ;unpuck A
    MOV ECX,len_A
    for_A:
        MOV DL,len_A
        SUB DL,CL
        MOVZX EDX,DL
        MOV ESI,EDX ;get valid pointer to number
        MOV DL,A[ESI]
        CALL unpuck
        PUSH EAX ;remember AL in stack
        MOV AL,len_A
        SUB AL,CL
        MUL duo
        MOVZX EAX,AL
        MOV ESI,EAX ;get valid pointer to result
        POP EAX ;get AL from stack
        MOV unp_A[ESI+0],AL ;save result
        MOV unp_A[ESI+1],BL
        LOOP for_A
    ;unpuck B
    MOV ECX,len_B
    for_B:
        MOV DL,len_B
        SUB DL,CL
        MOVZX EDX,DL
        MOV ESI,EDX
        MOV DL,B[ESI]
        CALL unpuck
        PUSH EAX
        MOV AL,len_B
        SUB AL,CL
        MUL duo
        MOVZX EAX,AL
        MOV ESI,EAX
        POP EAX
        MOV unp_B[ESI+0],AL
        MOV unp_B[ESI+1],BL
        LOOP for_B
    ;A*3
    MOV CL,len_unp_A
    XOR EBX,EBX
    for_i:
        MOV DL,len_unp_A
        SUB DL,CL
        MOVZX EDX,DL
        MOV ESI,EDX
        MOV AL,unp_A[ESI]
        MUL tre ;multiply to 3
        AAM ;correction
        ;SETB AL
        ADD AL,BL ;add transfered decade
        MOV BL,AH ;save next transfered decade
        MOV D[ESI],AL ;save result
        LOOP for_i
    ;MOV D[ESI+1],BL ;add last transfered decade
    SETB D[ESI+1]
    ;A + B
    MOV CL,len_unp_B
    CLC ;clear CF
    PUSHF ;save flags
    for_j:
        MOV DL,len_unp_B
        SUB DL,CL
        MOVZX EDX,DL
        MOV ESI,EDX
        MOV AL,unp_B[ESI]
        POPF ;load flags
        ADC AL,D[ESI] ;addition with considering of transfered decade
        AAA ;correction
        PUSHF ;save flags to next operation
        MOV D[ESI],AL ;save results
        LOOP for_j
    MOV AL,D[ESI+1] ;add last transfered decade
    ADC AL,0
    MOV D[ESI+1],AL
    ;prepare output string and print it
    XOR ESI,ESI ;pointer to number
    MOV ECX,len_D ;pointer to string
    for_s_out:
        MOV AL,D[ESI]
        OR AL,00110000b ;add 30h -> convert to ASCII number
        MOV s_out[ECX+13],AL ;change string
        INC ESI
        LOOP for_s_out
    invoke MessageBox, NULL, addr s_out, addr s_title, MB_OK

    invoke ExitProcess, NULL
    end start
