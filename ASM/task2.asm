;program works with arrays like with stacks - iterates elements from end (FILO).
;indents for logical code blocks are like in Python.
;Pchelkin Dmitriy, BIV-151.
.386
 	.model flat, stdcall
 	option casemap :none ;case sensitive
 	include C:\masm32\include\windows.inc
 	include C:\masm32\include\user32.inc
 	include C:\masm32\include\kernel32.inc
 	includelib C:\masm32\lib\user32.lib
 	includelib C:\masm32\lib\kernel32.lib

.data
	A DD 0, 1, 2, 3, 1, 2, 3, 4 ; matrix 2x4
	n_i db 2 ;rows
	n_j db 4 ;columns
    n_b db 2 ;length of B-vectors
    el_size db 4 ;bytes per element ("ONLY-1!" for 1 byte only values)
	B1 db 0, 2 ;vector 1
	B2 db 2, 1 ;vector 2

	s_title db "Pchelkin Dmitriy, BIV-151",0
    fs_in db "Given matrix:",10,"%.li %.li %.li %.li",10,"%.li %.li %.li %.li",0
    fs_out db "Result:",10,"%.li %.li %.li %.li",10,"%.li %.li %.li %.li",0
	s_in byte 32 dup(?) ;space for formated string
	s_out byte 26 dup(?) ;space for formated string

.code
start:
    invoke wsprintf, addr s_in, addr fs_in, A[4*0], A[4*1], A[4*2], A[4*3], A[4*4], A[4*5], A[4*6], A[4*7] 
    invoke MessageBox, NULL, addr s_in, addr s_title, MB_OK

    MOV CL,n_b ;size of B to ECX(CL)    <-- ONLY-1!
    MOVZX ECX,CL ;1 -> 4 bytes (expand CL to ECX)   <-- ONLY-1!
    for_i:
        PUSH ECX ;save amount of elements left in stack
        MOV ESI,ECX ;save pointer to B-vectors
        SUB ESI,1 ;count from 0
        MOV CL,n_i ;n of rows of array  <-- ONLY-1!
        MOVZX ECX,CL
        for_j:
            MOV EAX,ECX ;pointer to rows
            SUB EAX,1
            MUL n_j ;count size of row in bytes
            MUL el_size ;resize rows pointer to counted size
            MOV EDX,EAX

            MOV AL,B1[ESI] ;get B1[i] address (columns)  <-- ONLY-1!
            MOVZX EAX,AL
            MUL el_size ;resize pointer to columns
            MOV EBX,EAX

            MOV EBP,A[EBX+EDX] ;get data from A[BL]
            PUSH EBX ;save element's address to stack
            
            MOV AL,B2[ESI]
            MOVZX EAX,AL
            MUL el_size
            MOV EBX,EAX
            
            XCHG EBP,A[EBX+EDX] ;swipe data
            POP EBX ;get element's address from stack
            XCHG EBP,A[EBX+EDX]
            
            LOOP for_j ;ECX(CL)--
        POP ECX ;get amount of elements left from stack
        LOOP for_i ;ECX(CL)--
    
    invoke wsprintf, ADDR s_out, ADDR fs_out, A[4*0], A[4*1], A[4*2], A[4*3], A[4*4], A[4*5], A[4*6], A[4*7] 
    invoke MessageBox, NULL, ADDR s_out, ADDR s_title, MB_OK

    invoke ExitProcess, NULL
    end start
