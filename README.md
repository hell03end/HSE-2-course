# 2 course of learning in [MIEM](https://miem.hse.ru/) [[HSE](https://www.hse.ru/)]
Study prosses and some results.

**[Explore this in NBViewer](http://nbviewer.jupyter.org/github/hell03end/HSE-2-course/tree/master/)**

Содержимое распространяется на условиях лицензии [MIT License](https://github.com/hell03end/HSE-2-course/blob/master/LICENSE). При использовании материалов обязательно упоминание автора работ. При наличии технической возможности необходимо также указать активную гиперссылку на [репозиторий автора](https://github.com/hell03end/) или на используемый раздел.

Please, note that *some things here* are **Windows-only**!

**Any questions?** Please, [**[create an issue](https://github.com/hell03end/HSE-2-course/issues/new)**].

[Course page](https://www.hse.ru/ba/isct/).
