### PC device
1. CPU (central processing unit)
2. Motherboard
3. GPU (graphical processing unit)
4. RAM (random access memory)
5. HDD/SSD (permanent memory)
6. Power Supply
7. NIC (network interface card)
8. Sound Card (DSP)
9. BIOS (basic input/output system) stored in CMOS
10. ROM
11. POST (power on self test)
12. Cooling system
13. Bus