Технология - совокупность процессов={моделей, методов, средств} для достижения каких-либо целей.

Информационная технология - совокупность {процессов} для обработки выбора и сбора информации.

Цель - конечное состояние, которые мы хотим достичь (S.M(easurable).A.R.T.).

Автоматизация - процесс замены человеческого труда машинным.
Системы: ручные, частично автоматизированные, автономные.
Компоненты: информационное, персональное, ...{?}

[Функционал](https://toster.ru/q/14928)

Главные метрики управления проектами: время и бюджет (к последнему - ресурсы).

Информационная система = реализация информационных технологий.
Анализ - метод разложения объекта на составляющие и выявление их взаимосвязей.