# !/bin/bash -x
# by hell03end

function show_help {
	echo "Login to MIEM HSE free-bsd server."
	echo
	echo "login_ssh.sh [-g|[-l]] [-u USERNAME=biv151_10]"
	echo
	echo "    -g - for global ip:        92.242.57.83"
	echo "    -l - for MIEM only ip:     172.27.6.10"
	echo
}

# try to find -h key in given parameters
for arg in $*; do
	if [ "$arg" = "-h" -o "$arg" = "-H" -o "$arg" = "--help" -o "$arg" = "/?" ]; then
		show_help
		exit
	fi	
done

host_ip="172.27.6.10"  # default value
# search for manual keys -g/-l (global/local)
for arg in $*; do
	if [ "$arg" = "-g" ]; then
		host_ip="92.242.57.83"
		break
	fi
	if [ "$arg" = "-l" ]; then
		host_ip="172.27.6.10"
		break
	fi
done

username="biv151_10"  # default value
# search for manual key -u
while [ "$1" != "-u" -a $# -ne 0 ]; do
	shift  # skip value
done
shift  # skip -u key
if [ $# -ne 0 ]; then
	username=$1
	shift  # skip used value
fi

ssh "$username@$host_ip" -p 8956
sleep 1
