# !/bin/bash -x
# by hell03end

function show_help {
	echo "Copy files to MIEM HSE free-bsd server."
	echo
	echo "scp_HSE.sh [-g|[-l]] [-f files...] [-p HOST_PATH=~] [-u USERNAME=biv151_10] [-P FILE_PATH]"
	echo
	echo "    -f - list of files"
	echo "    -g - for global ip:        92.242.57.83"
	echo "    -l - for MIEM only ip:     172.27.6.10"
	echo
}

# try to find -h key in given parameters
for arg in $*; do
	if [ "$arg" = "-h" -o "$arg" = "-H" -o "$arg" = "--help" -o "$arg" = "/?" ]; then
		show_help
		exit
	fi	
done

host_ip="172.27.6.10"  # default value
# search for manual keys -g/-l (global/local)
for arg in $*; do
	if [ "$arg" = "-g" ]; then
		host_ip="92.242.57.83"
		break
	fi
	if [ "$arg" = "-l" ]; then
		host_ip="172.27.6.10"
		break
	fi
done

# default values
username="biv151_10"
files_path=""
files=" "
# search for manual keys
while [ $# -ne 0 ]; do
	if [ "$1" = "-f" ]; then
		shift
		if [ $# -ne 0 ]; then
			files=$1
		fi
	fi
	if [ "$1" = "-p" ]; then
		shift
		if [ $# -ne 0 ]; then
			host_path=$1
		fi
	fi
	if [ "$1" = "-u" ]; then
		shift
		if [ $# -ne 0 ]; then
			username=$1
		fi
	fi
	if [ "$1" = "-P" ]; then
		shift
		if [ $# -ne 0 ]; then
			files_path=$1
		fi
	fi
	shift
done
# ask for filenames if not specified
if [ "$files" = " " ]; then
	echo Enter file to send:
	read files
fi

# check existence of files 
files_exists=" "
for file in $files; do
	full_file_path="$files_path$file"
	if [ -e $full_file_path -a -r $full_file_path ]; then
		files_exists="$files_exists $full_file_path"
	else
		echo Can\'t find \"$full_file_path\".
	fi
done

scp -P 8956 -r $files_exists "$username@$host_ip":$host_path
sleep 1.5
