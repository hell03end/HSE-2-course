#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include "task3.h"

int dprintf(int fd, const char *format, ...);

int main (int argc, char const *argv[]) {
    struct message msg;
    sprintf(msg.privfifo, "%d-fifo", getpid());
    if (mkfifo(msg.privfifo, S_IFIFO | 0666) == -1) {
        perror(msg.privfifo);
        return 1;
    } // создание приватного именованного канала
    printf("создан %s\n", msg.privfifo);
    
    int fdpub, fdpriv;
    if ((fdpub = open("public", O_WRONLY)) == -1) {   
        close(fdpriv);
        unlink(msg.privfifo);
        perror("public");
        return 1;
    } // открытие публичного канала
    write(fdpub, (char*) &msg, sizeof(msg)); // отправление сообщения на сервер
    printf("сообщение отправлено в публичный канал\n");
    if ((fdpriv = open(msg.privfifo, O_WRONLY)) == -1) {   
        perror(msg.privfifo);
        return 1;
    } // открытие приватного канала

    int buf;
    while((buf = getchar()) != EOF) {
        dprintf(fdpriv, "%c", buf);
    } // отправление серверу

    close(fdpriv); unlink(msg.privfifo); close(fdpub);
    return 0;
}
