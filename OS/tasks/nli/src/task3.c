#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "task3.h"

#define DATASIZE 1024

int main(int argc, char const *argv[]) {
    int fdpub, fdpriv;
    struct message msg;
    printf("сервер успешно запущен\n");

    while (1) {
        if ((fdpub = open("public", O_RDONLY)) == -1) {
            fprintf(stderr, "ошибка: public\n");
            return 1;
        } // открытие публичного канала

        while (read(fdpub, (char*) &msg, sizeof(msg)) > 0) {
            printf("открыт %s\n", msg.privfifo);
            if ((fdpriv = open(msg.privfifo, O_RDONLY)) == -1) {   
                fprintf(stderr, "ошибка: %s\n", msg.privfifo);
                continue;
            } // открытие приватного канала

            /*                        ЛИБО ЭТО: ПРОСТО ЭХО                    */
            int i;
            char data[DATASIZE];
            while (read(fdpriv, data, DATASIZE) > 0) {
                printf("%s", data);
                for (i = 0; i < DATASIZE; ++i) {
                    data[i] = '\0';
                }
            }
            /******************************************************************/

            /*                         ЛИБО ЭТО: ФИЛЬТРЫ                      */
            // int fd[2];
            // if (pipe(fd) == -1) {
            //     fprintf(stderr, "ошибка: pipe\n");
            //     close(fdpriv);
            //     continue;
            // }

            // pid_t pid = fork(); // клонирование текущего процесса
            // if (pid == -1) /* не удалось */ {
            //     fprintf(stderr, "ошибка: fork\n");
            //     continue;
            // } else if (!pid) /* для дочернего процесса */ {
            //     printf("дочерний процесс %d\n", pid);

                
            //     if (dup2(fd[0], fileno(stdin)) == -1) {
            //         fprintf(stderr, "ошибка: dup\n");
            //         return 1;
            //     }
            //     close(fd[0]);
            //     close(fd[1]);

            //     if (execl("task1", "task1", NULL) == -1) {
            //         fprintf(stderr, "ошибка: execl\n");
            //         return 1;
            //     } // подмена текущего процесса фильтрами
            // } else /* для родительского процесса */ {
            //     printf("родительский процесс %d\n", pid);

            //     FILE *file = fdopen(fd[1], "w");
            //     if (!file) {
            //         fprintf(stderr, "ошибка: fdopen\n");
            //         close(fdpriv);
            //         continue;
            //     }
            //     close(fd[0]);
            //     close(fd[1]);

            //     int i;
            //     char data[DATASIZE];
            //     while (read(fdpriv, data, DATASIZE) > 0) {
            //         fprintf(file, "%s", data);
            //         for (i = 0; i < DATASIZE; ++i) {
            //             data[i] = '\0';
            //         }
            //     }
            // }
            /******************************************************************/
            close (fdpriv);
        }
        close (fdpub);
    }
}
