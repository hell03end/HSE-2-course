#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>

int main(int argc, char const *argv[]) {
    int fd[2]; // для сохранения канала

    if (pipe(fd) == -1) {
        perror("pipe");
        return 1;
    } // создание канала

    pid_t pid1 = fork(); // клонирование текущего процесса
    if (pid1 == -1) /* не удалось */ {
        perror("fork №1");
        return 1;
    } else if (!pid1) /* для дочернего процесса */ {
        printf("дочерний процесс %d\n", pid1);

        pid_t pid2 = fork();
        if (pid2 == -1) {
            perror("fork №2");
            return 1;
        } else if (!pid2) {
            printf("дочерний процесс %d\n", pid2);
 
            if (dup2(fd[0], fileno(stdin)) == -1) {
                perror("dup №1, stdin");
                return 1;
            }
            close(fd[1]);
            close(fd[0]);

            if (execl("f1", "f1", NULL) == -1) {
                perror("f1");
                return 1;
            } // подмена текущего процесса фильтром 1
        } else {
            printf("родительский процесс %d\n", pid2);

            if (dup2(fd[1], fileno(stdout)) == -1) {
                perror("dup №2, stdout");
                return 1;
            }
            close(fd[0]);
            close(fd[1]);

            if (execl("f2", "f2", NULL) == -1) {
                perror("f2");
                return 1;
            }
        }
    } else /* для родительского процесса */ {
        printf("родительский процесс %d\n", pid1);
        
        int subprosses;
        if (waitpid(-1, &subprosses, 0) == -1) {
            perror("waitpid");
            return 1;
        } // ожидание дочерних процессов
    }
    
    return 0;
}
