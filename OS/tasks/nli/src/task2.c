#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>

int main(int argc, char const *argv[]) {
    int fd1[2], fd2[2]; // для сохранения каналов

    // создание каналов
    if (pipe(fd1) == -1 || pipe(fd2) == -1) {
        perror("pipe");
        return 1;
    }

    pid_t pid = fork(); // клонирование текущего процесса
    if (pid == -1) /* не удалось */ {
        perror("fork");
        return 1;
    } else if (!pid) /* для дочернего процесса */ {
        printf("дочерний процесс %d\n", pid);
        
        if (dup2(fd1[0], fileno(stdin)) == -1) {
            perror("dup №1, stdin");
            return 1;
        }
        if (dup2(fd2[1], fileno(stdout)) == -1) {
            perror("dup №1, stdout");
            return 1;
        }
        close(fd1[0]);
        close(fd1[1]);
        close(fd2[0]);
        close(fd2[1]);

        if (execl("task1", "task1", NULL) == -1) {
            perror("task1");
            return 1;
        } // подмена текущего процесса фильтрами
    } else /* для родительского процесса */ {
        printf("родительский процесс %d\n", pid);

        int buf;
        FILE *file = fdopen(fd1[1], "w");
        if (!file) {
            perror("fdopen");
            return 1;
        }
        while((buf = getchar()) != EOF) {
            putc(buf, file);
        }
        fclose(file);

        int subprosses;
        if (waitpid(pid, &subprosses, 0) == -1) {
            perror("waitpid");
            return 1;
        }

        if (dup2(fd2[0], fileno(stdin)) == -1) {
            perror("dup №2, stdin");
            return 1;
        }

        close(fd1[0]);
        close(fd2[0]);
        close(fd2[1]);

        while((buf = getchar()) != EOF) {
            putchar(buf);
        }
    }

    return 0;
}
