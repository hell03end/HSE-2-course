/*
Выполнил:   Щерба Иван Андреевич
Группа:     БИВ151
Вариант:    14
Задание:    Организовать работу через именованные каналы
Серверная часть
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>

#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "server.h"

int main(int argc, char const *argv[]) {
    // обработка ключа -h для вызова справки по программе
    char arg_key;
    while ((arg_key = getopt(argc, (char * const*) argv, "h")) != EOF) {
        if (arg_key == 'h') /* если ключ -h имеется среди аргументов */ {
            printf("Вызываемая программа запускает серверную часть приложения\n");
            printf("использование:\t./имя команды [-h]\n");
            printf("\t-h  —  показ данной помощи\n");
            printf("\tдополнительные аргументы не требуются\n");
            printf("ПРИМЕЧАНИЕ: до запуска обязательно требуется выполнить команду 'mkfifo public'\n");
            return 0; // нормальное завершение выполнения
        } else /* все другие аргументы неопознаны */ {
            fprintf(stderr, "Неправильное использование команды. Используйте '-h' для вывода помощи\n");
            return 1; // завершение выполнения с ошибкой
        }
    }
    // пропуск обработанных аргументов
    argc -= optind;
    argv += optind;

    int channel_pub, channel_priv;
    struct message msg;

    // обработка данных в бесконечном цикле
    while (1) {
        // открытие публичного канала на чтение
        if ((channel_pub = open("public", O_RDONLY)) == -1) {
            fprintf(stderr, "Не удается открыть публичный канал ('public')\n");
            return 1;
        }

        // чтение из публичного канала имени приватного канала
        while (read(channel_pub, (char*) &msg, sizeof(msg)) > 0) {
            // открытие приватного канала на чтение и ожидание клиентского ввода (может заблокировать сервер)
            if ((channel_priv = open(msg.privfifo, O_RDONLY)) == -1) {   
                fprintf(stderr, "невозможно открыть канал: %s\n", msg.privfifo);
                continue;
            } // невозможно открыть публичный канал

            unsigned int STRLEN = 1024; // ограничение на максимальную длину строки по умолчанию
            // отправка данных в дочерний процесс
            char line[STRLEN]; // строка данных
            unsigned int len; // длина вводимой строки
            for (len = 0; len < STRLEN; ++len) {
                line[len] = '\0';
            } // обнуление строки данных
            while (read(channel_priv, line, STRLEN) > 0) {
                len = 0;
                // считывание данных из клиентского процесса и их печать на экране
                while (len < STRLEN-1 && line[len] != '\0' && line[len] != '\n') {
                    len++;
                } // подсчет количества символовы
                printf("%c\n", ' ');
                if (line[len] != '\n') /* исправление ввода */ {
                    char buf[STRLEN]; // строка данных
                    unsigned int i; // итератор
                    for (i = 0; i < STRLEN; ++i) {
                        buf[i] = '\0';
                    } // обнуление строки данных
                    if (read(channel_priv, buf, STRLEN) <= 0) {
                        break;
                    } // если данных больше нет
                    i = len;
                    while (len < STRLEN-1 && buf[len-i] != '\0' && buf[len-i] != '\n') {
                        line[len] = buf[len-i]; // дозапись данных
                        len++;
                    } // подсчет количества символовы
                }
                line[len] = '\0'; // добавление признака окончания строки
                printf("%d: %s\n", len, line); // вывод данных на экран  
                for (len = 0; len < STRLEN; ++len) {
                    line[len] = '\0';
                } // обнуление строки данных
            }

            close (channel_priv); // закрытие приватного канала
        }
        // закрытие публичного канала
        close (channel_pub);
    }
}
