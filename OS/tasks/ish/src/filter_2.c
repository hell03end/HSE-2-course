/*
Выполнил:   Щерба Иван Андреевич
Группа:     БИВ151
Вариант:    14
Задание:    Разработать фильтр, заменяющий цифры текстовым вариантом
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 

int print_digit_str(const char* str, const int space) {
    // добавление пробела перед следующей цифрой, чтобы строки не слипались
    if (space) {
        putchar(' ');
    }
    printf("%s", str); // вывод строкового аналога цифры
    return 1;
}

int main(int argc, char const *argv[]) {
    // обработка ключа -h для вызова справки по программе
    char arg_key;
    while ((arg_key = getopt(argc, (char * const*) argv, "h")) != EOF) {
        if (arg_key == 'h') /* если ключ -h имеется среди аргументов */ {
            printf("Вызываемая программа заменяет все встреченные цифры (не числа!) их строковым аналогом\n");
            printf("использование:\t./имя команды [-h]\n");
            printf("\t-h  —  показ данной помощи\n");
            printf("\tдополнительные аргументы не требуются\n");
            return 0; // нормальное завершение выполнения
        } else /* все другие аргументы неопознаны */ {
            fprintf(stderr, "Неправильное использование команды. Используйте '-h' для вывода помощи\n");
            return 1; // завершение выполнения с ошибкой
        }
    }
    
    int c; // переменная для записи нового вводимого символа
    int is_digit = 0; // индикатор того, что введенный символ - цифра (для пробелов)
    while ((c = getchar()) != EOF) /* обработка всех вводимых символов */ {
        if (c == '0') {
            is_digit = print_digit_str("ноль", is_digit);
        } else if (c == '1') {
            is_digit = print_digit_str("один", is_digit);
        } else if (c == '2') {
            is_digit = print_digit_str("два", is_digit);
        } else if (c == '3') {
            is_digit = print_digit_str("три", is_digit);
        } else if (c == '4') {
            is_digit = print_digit_str("четыре", is_digit);
        } else if (c == '5') {
            is_digit = print_digit_str("пять", is_digit);
        } else if (c == '6') {
            is_digit = print_digit_str("шесть", is_digit);
        } else if (c == '7') {
            is_digit = print_digit_str("семь", is_digit);
        } else if (c == '8') {
            is_digit = print_digit_str("восемь", is_digit);
        } else if (c == '9') {
            is_digit = print_digit_str("девять", is_digit);
        } else /* если символ не является цифрой - он выводится без изменений */ {
            is_digit = 0;
            putchar(c);
        }
    }

    return 0;
}
