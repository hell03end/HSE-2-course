/*
Выполнил:   Щерба Иван Андреевич
Группа:     БИВ151
Вариант:    14
Задание:    Организовать работу через именованные каналы
Клиентская часть
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include "server.h"

int print_digit_str(const char* str, const int space, const int fd) {
    // добавление пробела перед следующей цифрой, чтобы строки не слипались
    if (space) {
        dprintf(fd, " %s", str); // отправка данных на сервер
    } else {
        dprintf(fd, "%s", str); // отправка данных на сервер
    }
    return 1;
}

int dprintf(int fd, const char *format, ...);
int main (int argc, char const *argv[]) {
    // обработка ключа -h для вызова справки по программе
    char arg_key;
    while ((arg_key = getopt(argc, (char * const*) argv, "h")) != EOF) {
        if (arg_key == 'h') /* если ключ -h имеется среди аргументов */ {
            printf("Вызываемая программа запускает клиент приложения\n");
            printf("использование:\t./имя команды [-h]\n");
            printf("\t-h  —  показ данной помощи\n");
            printf("\tдополнительные аргументы не требуются\n");
            printf("ПРИМЕЧАНИЕ: до запуска обязательно требуется выполнить команду 'mkfifo public'\n");
            return 0; // нормальное завершение выполнения
        } else /* все другие аргументы неопознаны */ {
            fprintf(stderr, "Неправильное использование команды. Используйте '-h' для вывода помощи\n");
            return 1; // завершение выполнения с ошибкой
        }
    }
    // пропуск обработанных аргументов
    argc -= optind;
    argv += optind;

    struct message msg; // данные, передаваемые серверу
    int fdpub, fdpriv;

    // создание частного именованного канала с правами доступа
    sprintf(msg.privfifo, "Fifo %d", getpid()); // создание имени канала и запись в структуру
    if (mkfifo(msg.privfifo, S_IFIFO | 0666) == -1) {
        perror(msg.privfifo);
        return 1;
    } // создание канала не удалось
    
    // открытие публичного канала с именем 'public' для записи и отправка в него сообщения
    if ((fdpub = open("public", O_WRONLY)) == -1) {   
        perror("public");
        return 1;
    } // не удалось открыть публичный канал
    write(fdpub, (char*) &msg, sizeof(msg)); 

    // открытие приватного именованного канала на запись 
    if ((fdpriv = open(msg.privfifo, O_WRONLY)) == -1) /* не удалось открыть приватный канал */ {   
        close(fdpub); // закрытие публичного канала
        perror(msg.privfifo);
        return 1;
    }

    // отправка данных на сервер через приватный канал
    int c; // для считывания данных посимвольно
    int is_digit = 0; // индикатор того, что введенный символ - цифра (для пробелов)
    while((c = getchar()) != EOF) {
        if (c == '0') {
            is_digit = print_digit_str("ноль", is_digit, fdpriv);
        } else if (c == '1') {
            is_digit = print_digit_str("один", is_digit, fdpriv);
        } else if (c == '2') {
            is_digit = print_digit_str("два", is_digit, fdpriv);
        } else if (c == '3') {
            is_digit = print_digit_str("три", is_digit, fdpriv);
        } else if (c == '4') {
            is_digit = print_digit_str("четыре", is_digit, fdpriv);
        } else if (c == '5') {
            is_digit = print_digit_str("пять", is_digit, fdpriv);
        } else if (c == '6') {
            is_digit = print_digit_str("шесть", is_digit, fdpriv);
        } else if (c == '7') {
            is_digit = print_digit_str("семь", is_digit, fdpriv);
        } else if (c == '8') {
            is_digit = print_digit_str("восемь", is_digit, fdpriv);
        } else if (c == '9') {
            is_digit = print_digit_str("девять", is_digit, fdpriv);
        } else /* если символ не является цифрой - он выводится без изменений */ {
            is_digit = 0;
            dprintf(fdpriv, "%c", c); // отправка данных на сервер
        }
    }

    close(fdpriv); // закрытие приватного канала
    unlink(msg.privfifo); // удаление приватного канала
    close(fdpub); // закрытие публичного канала

    return 0;
}
