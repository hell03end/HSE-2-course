/*
Выполнил:   Щерба Иван Андреевич
Группа:     БИВ151
Вариант:    14
Задание:    Разработать программу, которая осуществляет просмотр текущего каталога и 
выводит на экран имена находящихся в нём каталогов, упорядочив их по числу файлов и 
каталогов, содержащихся в отображаемом каталоге. Для каждого такого каталога указывается 
число содержащихся в нём файлов и каталогов. 
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

#ifndef STRLEN
#define STRLEN 1024
#endif

#ifndef COUNTDIR
#define COUNTDIR 100
#endif

struct dir_info_t {
    char name[STRLEN];
    int count_files;
};

// функция для сравнения директорий по количеству файлов в них
int compare_dir (const void * elem1, const void * elem2) {
    struct dir_info_t *f = (struct dir_info_t *) elem1;
    struct dir_info_t *s = (struct dir_info_t *) elem2;
    if (f->count_files > s->count_files) {
        return -1;
    }
    if (f->count_files < s->count_files) {
        return 1;
    }
    return 0;
}

int main(int argc, char const *argv[]) {
    // проверка корректности использования скрипта
    if (argc > 1) /* неверное число аргументов (> 0) */ {
        fprintf(stderr, "Неправильное использование команды:\tаргументы не требуются.\n");
        return 1; // завершение с кодом ошибки != 0
    }
    
    DIR *dir_ptr = opendir("."); // открытие директории
    // проверка корректности открытия
    if (!dir_ptr) {
        fprintf(stderr, "Невозможно открыть директорию!\n");
        return 1;
    }

    struct dir_info_t *directories[COUNTDIR]; // массив поддиректорий данной директории
    int idx = 0; // индекс директории в массиве

    struct dirent *file_ptr; // для сохранения информации о файлах открытой директории
    // просмотр открытой директории
    while ((file_ptr = readdir(dir_ptr))) {
        if (idx >= COUNTDIR) {
            fprintf(stderr, "Слишком много файлов в каталоге (> %d)!\n", COUNTDIR);
            break;
        }

        // выделение памяти для структуры содержащей информацию о каталоге:
        struct stat *stat_ptr = (struct stat*) malloc(sizeof(struct stat));
    
        // пропуск файлов, информация о которых недоступна
        if (lstat(file_ptr->d_name, stat_ptr)) {
            fprintf(stderr, "Невозможно получить информацию о файле: '%s'\n", 
                    file_ptr->d_name);
            free(stat_ptr);
            continue;
        }
        // пропуск файлов, не являющихся директорией
        if (!S_ISDIR(stat_ptr->st_mode)) {
            free(stat_ptr);
            continue;
        }
        
        // подсчет файлов, содержащихся в открытой поддиректории
        unsigned int count_files = 0; // счетчик файлов в поддиректории
        DIR *subdir_ptr = opendir(file_ptr->d_name); // открытие поддиректории
        if (!subdir_ptr) {
            fprintf(stderr, "Невозможно открыть поддиректорию: '%s'\n", file_ptr->d_name);
            free(stat_ptr);
            continue;
        }
        struct dirent *subdir_file_ptr;
        while ((subdir_file_ptr = readdir(subdir_ptr))) {
            count_files++;
        }
        free(subdir_ptr);
        free(subdir_file_ptr);

        // выделение памяти под структуру с данными о поддиректории
        directories[idx] = (struct dir_info_t *) malloc(sizeof(struct dir_info_t));
        // запись информации о поддиректории в структуру
        strcpy(directories[idx]->name, file_ptr->d_name); // сохранение имени директории
        directories[idx]->count_files = count_files; // сохранение количества файлов
        idx++; // увеличиваем индекс в массиве

        free(stat_ptr); // очистка памяти, занимаемой структурой
    }

    // массив поддиректорий, размером с количество считанных директорий
    struct dir_info_t dirs[idx];
    int i;
    for (i = 0; i < idx; ++i) {
        dirs[i] = *directories[i];
        free(directories[i]);
    }

    // сортировка поддиректорий по количеству файлов в них (функция qsort из stdlib.h)
    qsort(dirs, idx, sizeof(struct dir_info_t), compare_dir);
    // вывод результатов на экран
    for (i = 0; i < idx; ++i) {
        printf("%d\t%s\n", dirs[i].count_files, dirs[i].name);
    }

    return 0;
}
