// count characters
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {
    /* parent process should provide: input == p[R], output = q[W] */
    int count = 0;
    while (getchar() != EOF) {
        count++;
    }
    printf("%d\n", count);

    return EXIT_SUCCESS;
}
