// Textcount
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <string.h> // for strerror()
#include <errno.h> // for errno
#include <sys/wait.h>
#include <sys/types.h>

#define R 0
#define W 1

void exit_with_error(char const *message);

int main(int argc, char const *argv[]) {
    int p[2], q[2];

    if (pipe(p) == -1) /* create fifo channel */ {
        exit_with_error("Error while creating fifo channel #p");
    }
    if (pipe(q) == -1) {
        exit_with_error("Error while creating fifo channel #q");
    }

    pid_t pid = fork(); // clone process
    if (pid == -1) {
        exit_with_error("Error while fork()");
    } else if (!pid) /* if this process is a child */ {
        close(p[W]);
        close(q[R]);
        
        if (dup2(p[R], fileno(stdin)) == -1) /* reassign stdin */ {
            exit_with_error("Can't reassign stdin #1");
        }
        close(p[R]);

        if (dup2(q[W], fileno(stdout)) == -1) /* reassign stdout */ {
            exit_with_error("Can't reassign stdout #1");
        }
        close(q[W]);

        /* run count */
        if (execl("count", "count", NULL) == -1) {
            exit_with_error("Error while call 'count'");
        }
    } else /* if this process is a parent */ {
        FILE *fp = fdopen(p[W], "w");
        if (!fp) {
            exit_with_error("Error: Can't open file.");
        }

        close(p[R]); 
        close(q[W]);

        /* sent file to count */
        int c;
        int newline = 1;
        while((c = getchar()) != EOF) {
            switch(newline) {
                case 1:
                    if (c == '\n') {
                        putc(c, fp);
                    } else if (c == '.') {
                        while((c = getchar()) != EOF && c != '\n')
                            ; /* skip string */
                    } else {
                        putc(c, fp);
                        newline = 0;
                    }
                    break;
                default:
                    putc(c, fp);
                    if (c == '\n') {
                        newline = 1;
                    }
            }
        }
        fclose(fp); /* to add EOF */

        /* read input from q */
        if (dup2(q[R], fileno(stdin)) == -1) /* reassign stdin */ {
            exit_with_error("Can't reassign stdin #2");
        }
        close(q[R]);

        // wait for child process end it's job
        int pid_status;
        if (waitpid(pid, &pid_status, 0) == -1) {
            exit_with_error("Error while waiting child processes");
        }

        int total;
        if (!scanf("%d", &total)) {
            exit_with_error("Can't read data returned from 'count'");
        }
        printf("%d characters in common.\n", total);
    }

    return EXIT_SUCCESS;
}


void exit_with_error(char const *message) {
    if (errno) {
        fprintf(stderr, "%s: %s\n", message, strerror(errno));
    } else {
        fprintf(stderr, "%s\n", message);
    }
    exit(EXIT_FAILURE);
}
