// server.h
#define PUBLIC "public" /* public channel name (server) */
#define LINESIZE 512

typedef struct _message {
    char private_fifo[15]; /* private channel name (client) */
    char filename[100]; /* requested file name */
} Message;
