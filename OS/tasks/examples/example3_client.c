// client
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/io.h>
#include "example3_server.h"

int main (int argc, char const *argv[]) {
    Message msg;
    int fdpub, fdpriv;

    /* create private fifo channel and specify access rights */
    sprintf(msg.private_fifo, "Fifo %d", getpid()); // create channel name
    if (mkfifo(msg.private_fifo, S_IFIFO | 0666) == -1) {   
        perror (msg.private_fifo);
        return EXIT_FAILURE;
    }
    
    /* общедоступный именованный канал (создается перед запуском сервера %mkfifo public) открывается на запись, и в него записываются имена личного канала и требуемого файла */

    if ((fdpub = open(PUBLIC, O_WRONLY)) == -1) {   
        perror(PUBLIC);
        return EXIT_FAILURE;
    }
    strcpy(msg.filename, argv[1]);
    write(fdpub, (char*) &msg, sizeof(msg));

    /* open private named channel for read */
    if ((fdpriv = open(msg.private_fifo, O_RDONLY)) == -1) {   
        perror(msg.private_fifo);
        return EXIT_FAILURE;
    }

    /* print data received from private channel */
    int n;
    char line[LINESIZE];
    while((n = read(fdpriv, line, LINESIZE)) > 0) {
        write(1, line, n);
    }

    /* close and remove private fifo channel */
    close(fdpriv);
    unlink(msg.private_fifo);
    
    return EXIT_SUCCESS;
}
