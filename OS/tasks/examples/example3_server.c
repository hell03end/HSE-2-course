// server
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "example3_server.h"

int main(int argc, char const *argv[]) {
    int fdpub, fdpriv, fd;
    Message msg;

    while(1) {
        /* open public fifo channel for read only, lock open() */
        if ((fdpriv = open(msg.private_fifo, O_RDONLY)) == -1) {   
            perror(PUBLIC);
            return EXIT_FAILURE;
        }

        /* read requested file name and private channel name from public channel */
        while (read(fdpub, (char*) &msg, sizeof(msg)) > 0) {
            /* open requested file for read */
            if ((fd = open(msg.filename, O_RDONLY)) == -1) {   
                perror(msg.filename);
                break;
            }

            /* open private channel for write and wait for client (CAN BLOCK SERVER) */
            if ((fdpriv = open(msg.private_fifo, O_WRONLY)) == -1) {   
                perror(msg.private_fifo);
                break;
            }

            /* send file data to client throw it's private channel */
            int n;
            char line[LINESIZE];
            while((n = read(fd, line, LINESIZE)) > 0) {
                write(fdpriv, line, n);
            }

            close (fd);
            close (fdpriv);
        }

        /* close public channel to listen it (open()) */
        close (fdpub);
    }
}
