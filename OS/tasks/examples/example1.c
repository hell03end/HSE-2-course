// ls | ws
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <string.h> // for strerror()
#include <errno.h> // for errno
#include <sys/wait.h>
#include <sys/types.h>

#define R 0
#define W 1

void exit_with_error(char const *message);

int main(int argc, char const *argv[]) {
    int fd[2]; // files descriptors

    pid_t pid1 = fork(); // clone current process
    if (pid1 == -1) {
        exit_with_error("Error while fork() #1");
    } else if (!pid1) /* if this process is a child */ {
        if (pipe(fd) == -1) /* create fifo channel */ {
            exit_with_error("Error while creating fifo channel");
        }
        pid_t pid2 = fork();
        if (pid2 == -1) {
            exit_with_error("Error while fork() #2");
        } else if (!pid2) {
            close(fd[W]); // close fd[1]

            if (dup2(fd[R], fileno(stdin)) == -1) /* reassign stdin as fd[0] */ {
                exit_with_error("Can't reassign stdin");
            }
            close(fd[R]); // close original fd[0]

            if (execlp("wc", "wc", NULL) == -1) /* run wc instead this process, search in PATH */ {
                exit_with_error("Error while call 'wc'");
            }
        } else {
            close(fd[R]);

            if (dup2(fd[W], fileno(stdout)) == -1) {
                exit_with_error("Can't reassign stdout");
            }
            close(fd[W]);

            if (execlp("ls", "ls", "-a", NULL)) /* run ls instead this process */ {
                exit_with_error("Error while call 'ls'");
            }
        }
    } else /* if this process is a parent */ {
        /* wait while all child processes end their jobs */
        int pid_status;
        if (waitpid(-1, &pid_status, 0) == -1) {
            exit_with_error("Error while waiting child processes");
        }
    }
    
    return EXIT_SUCCESS;
}


void exit_with_error(char const *message) {
    if (errno) {
        fprintf(stderr, "%s: %s\n", message, strerror(errno));
    } else {
        fprintf(stderr, "%s\n", message);
    }
    exit(EXIT_FAILURE);
}
