// change all 'o' to '0' and 'i' 2 '1' or vice versa
// by hell03end

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include "commons.h" // for exit_with_error


void show_help(void);

int main(int argc, char const *argv[]) {
    char reverse = 0;

    /* handle named params */
    char arg_key;
    while ((arg_key = getopt(argc, (char * const*) argv, "hr")) != EOF) {
        if (arg_key == 'h') {
            show_help();
            return EXIT_SUCCESS;
        } else if (arg_key == 'r') {
            reverse = 1;
        } else {
            exit_with_error("Unknown key");
        }
    }
    /* skip used params */
    argc -= optind;
    argv += optind;
    
    int c;
    while ((c = getchar()) != EOF) {
        if (!reverse && (c == 'i' || c == 'I')) {
            putchar('1');
        } else if (!reverse && (c == 'o' || c == 'O')) {
            putchar('0');
        } else if (reverse && c == '0') {
            putchar('o');
        } else if (reverse && c == '1') {
            putchar('i');
        } else if (reverse && c == '2') {
            printf("to");
        } else if (reverse && c == '3') {
            printf("fri");
        } else if (reverse && c == '4') {
            printf("for");
        } else {
            putchar(c);
        }
    }

    return EXIT_SUCCESS;
}


void show_help() {
    printf("Change all 'o' to '0' and 'i' 2 '1' or vice versa.\n");
    printf("Usage:\t./filter_2 [-h] [-r]\n");
    printf("\t-h  —  showing help.\n");
    printf("\t-r  —  to change '0' to 'o', '1' to 'i', '2' to 'to', etc.\n");
}
