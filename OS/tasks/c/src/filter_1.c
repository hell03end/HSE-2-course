// add indent to input text and print it.
// by hell03end

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include "commons.h" // for exit_with_error


void show_help(void);

int main(int argc, char const *argv[]) {
    char indent_symbol = '*';
    unsigned int indent_size = 4;

    /* handle named params */
    char arg_key;
    while ((arg_key = getopt(argc, (char * const*) argv, "hs:")) != EOF) {
        if (arg_key == 'h') {
            show_help();
            return EXIT_SUCCESS;
        } else if (arg_key == 's') {
            // @FIXME: handle atoi exceptions + manual symbol selection isn't working
            indent_symbol = (char) atoi(optarg);
        } else {
            exit_with_error("Unknown key");
        }
    }
    /* skip used params */
    argc -= optind;
    argv += optind;

    if (!argc) {
        exit_with_error("WRONG NUMBER OF POSITIONAL ARGUMENTS!");
    }

    if ((indent_size = (unsigned int) atoi(argv[0])) > 20) {
        exit_with_error("Too big size of indent!");
    }

    int c;
    char newline = 1;
    while ((c = getchar()) != EOF) {
        if (newline) {
            unsigned int i;
            for (i = 1; i < indent_size; i++) {
                putchar(' ');
            }
            if (indent_size) {
                putchar(indent_symbol);
            }
            newline = 0;
        } 
        if (c == '\n') {
            newline = 1;
        }
        putchar(c);
    }

    return EXIT_SUCCESS;
}


void show_help() {
    printf("Add specified indent for given input.\n");
    printf("Usage:\t./filter_1 INDENT_SIZE [-h] [-s SYMBOL]\n");
    printf("\tINDENT_SIZE  —  should be > 0 and < 20.\n");
    printf("\t-h  —  showing help.\n");
    printf("\t-s  —  symbol to mark indent border.\n");
}
