// Var:     10
// Group:   BIV-151_1
// Task:    Разработать два фильтра и организовать их работу через конвейер
//          (через неименованный и именованный программные каналы).
//          Фильтр: вывод вводимого текста с заданным отступом от левого края 
//          (левая граница помечается звездочкой или указанным символом).
//          Фильтр: заменяет все символы 'o' на '0', а 'i' на '1' или наоборот.
//          Ввод осуществляется в клиентской части приложения, через именованный
//          канал поступает на сервер, где обрабатывается и выводится на экран. 
// by hell03end

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include "commons.h" // for exit_with_error


int dprintf(int fd, const char *format, ...); // function undeclared
void show_help(void);

int main (int argc, char const *argv[]) {
    /* handle named params */
    char arg_key;
    while ((arg_key = getopt(argc, (char * const*) argv, "h")) != EOF) {
        if (arg_key == 'h') {
            show_help();
            return EXIT_SUCCESS;
        } else {
            exit_with_error("Unknown key");
        }
    }
    /* skip used params */
    argc -= optind;
    argv += optind;

    if (!argc) {
        exit_with_error("WRONG NUMBER OF POSITIONAL ARGUMENTS!");
    }

    Message msg;
    int fdpub, fdpriv;

    /* create private fifo channel and specify access rights */
    sprintf(msg.private_fifo, "Fifo %d", getpid()); // create channel name
    if (mkfifo(msg.private_fifo, S_IFIFO | 0666) == -1) {  
        exit_with_perror(msg.private_fifo);
    }
    
    /* open public named channel (`%mkfifo public` before server launch) 
    to write and pass there msg structure */
    if ((fdpub = open(argv[0], O_WRONLY)) == -1) {   
        exit_with_perror(argv[0]);
    }
    int t = write(fdpub, (char*) &msg, sizeof(msg));

    /* open private named channel for write */
    if ((fdpriv = open(msg.private_fifo, O_WRONLY)) == -1) {   
        close(fdpub); // close public fifo channel
        exit_with_perror(msg.private_fifo);
    }

    /* send data to server throw private channel */
    int c;
    while((c = getchar()) != EOF) {
        t = dprintf(fdpriv, "%c", c); // sent file to filter
    }

    /* close and remove private fifo channel */
    close(fdpriv); /* to add EOF */
    unlink(msg.private_fifo);
    close(fdpub);

    return EXIT_SUCCESS;
}


void show_help() {
    printf("Apply 2 filters to input data.\n");
    printf("Usage:\t./conveer_3 PUBLIC_NAME [-h]\n");
    printf("\tPUBLIC_NAME  —  name of public fifo channel.\n");
    // printf("\tFILENAME  —  name of the file to save on the server.\n");
    printf("\t-h  —  showing help.\n");
}
