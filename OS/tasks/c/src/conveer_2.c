// Var:     10
// Group:   BIV-151_1
// Task:    Разработать два фильтра и организовать их работу через конвейер
//          (через неименованный и именованный программные каналы).
//          Фильтр: вывод вводимого текста с заданным отступом от левого края 
//          (левая граница помечается звездочкой или указанным символом).
//          Фильтр: заменяет все символы 'o' на '0', а 'i' на '1' или наоборот.
//          Ввод осуществляется в родительском процессе, через канал поступает 
//          в дочерний, где обрабатывается, а затем через другой канал поступает
//          обратно в родительский процесс откуда выводится на экран. 
// by hell03end

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include "commons.h" // for exit_with_error


void show_help(void);

int main(int argc, char const *argv[]) {
    /* handle named params */
    char arg_key;
    while ((arg_key = getopt(argc, (char * const*) argv, "h")) != EOF) {
        if (arg_key == 'h') {
            show_help();
            return EXIT_SUCCESS;
        } else {
            exit_with_error("Unknown key");
        }
    }
    /* skip used params */
    argc -= optind;
    argv += optind;

    int p[2], q[2]; // for fifo channels
    if (pipe(p) == -1) /* create fifo channel */ {
        exit_with_error("Error while creating fifo channel #p");
    }
    if (pipe(q) == -1) {
        exit_with_error("Error while creating fifo channel #q");
    }

    pid_t pid = fork(); // clone process
    if (pid == -1) {
        exit_with_error("Error while fork()");
    } else if (!pid) /* if this process is a child */ {
        close(p[W]);
        close(q[R]);
        
        if (dup2(p[R], fileno(stdin)) == -1) /* reassign stdin */ {
            exit_with_error("Can't reassign stdin #1");
        }
        close(p[R]);

        printf("I'm channel 1 with pid: %d\n", pid);

        if (dup2(q[W], fileno(stdout)) == -1) /* reassign stdout */ {
            exit_with_error("Can't reassign stdout #1");
        }
        close(q[W]);

        if (execl("conveer_1", "conveer_1", NULL) == -1) {
            exit_with_error("Error while call 'conveer_1'");
        }
    } else /* if this process is a parent */ {
        FILE *fp = fdopen(p[W], "w");
        if (!fp) {
            exit_with_error("Error: Can't open output file.");
        }
        close(p[R]);
        close(q[W]);

        printf("Parent process pid: %d\n", pid);

        int c;
        while((c = getchar()) != EOF) {
            putc(c, fp); // sent file to filter
        }
        fclose(fp); /* to add EOF */

        // wait for child process end it's job
        int pid_status;
        if (waitpid(pid, &pid_status, 0) == -1) {
            exit_with_error("Error while waiting child processes");
        }

        // read from child output
        if (dup2(q[R], fileno(stdin)) == -1) /* reassign stdin */ {
            exit_with_error("Can't reassign stdin #2");
        }
        close(q[R]);

        while((c = getchar()) != EOF) {
            putchar(c); // sent to output
        }
    }

    return EXIT_SUCCESS;
}


void show_help() {
    printf("Apply 2 filters to input data.\n");
    printf("Usage:\t./conveer_2 [-h]\n");
    printf("\t-h  —  showing help.\n");
}
