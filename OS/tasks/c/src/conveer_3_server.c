#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>

#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "commons.h" // for exit_with_error


void show_help(void);

int main(int argc, char const *argv[]) {
    /* handle named params */
    char arg_key;
    while ((arg_key = getopt(argc, (char * const*) argv, "h")) != EOF) {
        if (arg_key == 'h') {
            show_help();
            return EXIT_SUCCESS;
        } else {
            exit_with_error("Unknown key");
        }
    }
    /* skip used params */
    argc -= optind;
    argv += optind;

    if (!argc) {
        exit_with_error("WRONG NUMBER OF POSITIONAL ARGUMENTS!");
    }

    int fdpub, fdpriv, fd;
    Message msg;

    while (1) {
        /* open public fifo channel for read only, lock open() */
        if ((fdpub = open(argv[0], O_RDONLY)) == -1) {
            exit_with_perror(argv[0]);
        }

        /* read requested file name and private channel name from public channel */
        while (read(fdpub, (char*) &msg, sizeof(msg)) > 0) {
            /* open private channel for read and wait for client (CAN BLOCK SERVER) */
            if ((fdpriv = open(msg.private_fifo, O_RDONLY)) == -1) {   
                perror(msg.private_fifo);
                continue;
            }

            /* get data from client throw it's private channel */
            pid_t pid = fork(); // clone process
            int fd[2];
            if (pipe(fd) == -1) /* create fifo channel */ {
                perror("Error while creating fifo channel");
                continue;
            }

            if (pid == -1) {
                perror("Error while fork()");
                continue;
            } else if (!pid) /* if this process is a child */ {
                close(fd[W]);
                
                if (dup2(fd[R], fileno(stdin)) == -1) /* reassign stdin */ {
                    exit_with_error("Can't reassign stdin");
                }
                close(fd[R]);

                if (execl("conveer_1", "conveer_1", NULL) == -1) {
                    exit_with_error("Error while call 'conveer_1'");
                }
            } else /* if this process is a parent */ {
                close(fd[R]);
                // read from child output
                if (dup2(fd[W], fileno(stdout)) == -1) /* reassign stdin */ {
                    perror("Can't reassign stdout");
                    continue;
                }
                close(fd[W]);

                int n;
                char line[LINESIZE];
                while ((n = read(fdpriv, line, LINESIZE)) > 0) {
                    printf("%s", line);
                }
            }

            close (fdpriv);
        }
        /* close public channel to listen it (open()) */
        close (fdpub);
    }
}


void show_help() {
    printf("Start server, which apply 2 filters to data given from clients.\n");
    printf("Usage:\t./conveer_3_server PUBLIC_NAME [-h]\n");
    printf("\tPUBLIC_NAME  —  name of public fifo channel.\n");
    printf("\t-h  —  showing help.\n");
}
