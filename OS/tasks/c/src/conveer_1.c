// Var:     10
// Group:   BIV-151_1
// Task:    Разработать два фильтра и организовать их работу через конвейер
//          (через неименованный и именованный программные каналы).
//          Фильтр: вывод вводимого текста с заданным отступом от левого края 
//          (левая граница помечается звездочкой или указанным символом).
//          Фильтр: заменяет все символы 'o' на '0', а 'i' на '1' или наоборот.
//          Ввод осуществляется в первый фильтр, через канал поступает во 
//          второй, затем выводится на экран прямо из него. Первый фильтр 
//          запускается из родительского процесса, второй из первого. 
// by hell03end

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>
#include "commons.h" // for exit_with_error


void show_help(void);

int main(int argc, char const *argv[]) {
    unsigned int indent_size = 4;
    char reverse = 0;
    int fd[2]; // for fifo channel

    /* handle named params */
    char arg_key;
    while ((arg_key = getopt(argc, (char * const*) argv, "hrn:")) != EOF) {
        if (arg_key == 'h') {
            show_help();
            return EXIT_SUCCESS;
        } else if (arg_key == 'n') {
            indent_size = (unsigned int) atoi(optarg);
        } else if (arg_key == 'r') {
            reverse = 1;
        } else {
            exit_with_error("Unknown key");
        }
    }
    /* skip used params */
    argc -= optind;
    argv += optind;

    pid_t pid1 = fork(); // clone current process
    if (pid1 == -1) {
        exit_with_error("Error while fork() #1");
    } else if (!pid1) /* if this process is a child */ {
        if (pipe(fd) == -1) /* create fifo channel */ {
            exit_with_error("Error while creating fifo channel");
        }
        pid_t pid2 = fork();
        if (pid2 == -1) {
            exit_with_error("Error while fork() #2");
        } else if (!pid2) {
            close(fd[W]); // close fd[1]

            if (dup2(fd[R], fileno(stdin)) == -1) /* reassign stdin as fd[0] */ {
                exit_with_error("Can't reassign stdin");
            }
            close(fd[R]); // close original fd[0]

            /* run count */
            // @FIXME: pass all arguments to execl
            char params[32];
            sprintf(params, "%d", indent_size);

            if (execl("filter_1", "filter_1", params, NULL) == -1) /* run filter_1 instead this process */ {
                exit_with_error("Error while call 'filter_1'");
            }
        } else {
            close(fd[R]);

            if (dup2(fd[W], fileno(stdout)) == -1) {
                exit_with_error("Can't reassign stdout");
            }
            close(fd[W]);

            /* run count */
            // @FIXME: pass all arguments to execl
            char params[32];
            if (reverse) {
                sprintf(params, "-r");
            }

            if (execl("filter_2", "filter_2", params, NULL) == -1) /* run filter_2 instead this process */ {
                exit_with_error("Error while call 'filter_2'");
            }
        }
    } else /* if this process is a parent */ {
        /* wait while all child processes end their jobs */
        int pid_status;
        if (waitpid(-1, &pid_status, 0) == -1) {
            exit_with_error("Error while waiting child processes");
        }
    }
    
    return EXIT_SUCCESS;
}


void show_help() {
    printf("Apply 2 filters to input data.\n");
    printf("Usage:\t./conveer_1 [-hr] [-n INDENT_SIZE]\n");
    printf("\t-h  —  showing help.\n");
    printf("\t-n  —  indent size should be > 0 and < 20.\n");
    printf("\t-r  —  to change '0' to 'o', '1' to 'i', '2' to 'to', etc.\n");
    // printf("\t-s  —  symbol to mark indent border.\n");
}
