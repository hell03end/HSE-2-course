#include <stdio.h>
#include <errno.h> // for errno
#include <string.h> // for strerror()

#ifndef R
#define R 0
#endif
#ifndef W
#define W 1
#endif

#ifndef LINESIZE
#define LINESIZE 512
#endif

typedef struct _message {
    char private_fifo[15]; /* private channel name (client) */
    // char filename[100]; /* requested file name */
} Message;

void exit_with_error(char const *message) {
    if (errno) {
        fprintf(stderr, "%s: %s\n", message, strerror(errno));
    } else {
        fprintf(stderr, "%s\n", message);
    }
    exit(EXIT_FAILURE);
}

void exit_with_perror(char const *message) {
    perror(message);
    exit(EXIT_FAILURE);
}
