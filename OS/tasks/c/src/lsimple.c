// Var:     10
// Group:   BIV-151_1
// Task:    Разработать программу, которая просматривает текущий каталог и
//          выводит на экран имена всех встретившихся в нем обычных файлов.
// by hell03end

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

#ifndef DEFDIR
#define DEFDIR "." // default value: current dir
#endif

void show_help(void);

int main(int argc, char const *argv[]) {
    /* main working structures */
    struct dirent *file_ptr;
    struct stat *fstat_ptr = (struct stat*) malloc(sizeof(struct stat));
    DIR *dir_ptr = opendir(DEFDIR); // use current dir by default
    char only_simple = 1; // show only simple files

    /* handle named params */
    char arg_key;
    while ((arg_key = getopt(argc, (char * const*) argv, "hap:")) != EOF) {
        if (arg_key == 'h') {
            show_help();
            return(EXIT_SUCCESS);
        } else if (arg_key == 'a') {
            only_simple = 0;
        } else if (arg_key == 'p') {
            dir_ptr = opendir(optarg);
        } else {
            fprintf(stderr, "Unknown key: '%s'\n", optarg);
            return(EXIT_FAILURE);
        }
    }
    /* skip used params */
    argc -= optind;
    argv += optind;

    /* check all conditions for normal workflow satisfied */
    if (argc != 0 || !dir_ptr) {
        if (argc != 0) {
            fprintf(stderr, "WRONG USAGE!\n");
            show_help();
        } else {
            fprintf(stderr, "Can't open directory!\n");
        }
        return(EXIT_FAILURE);
    }

    /* showing all files in specified dir */
    while ((file_ptr = readdir(dir_ptr))) {
        if (only_simple) {
            /* skip unaccessible files */
            free(fstat_ptr);
            fstat_ptr = (struct stat*) malloc(sizeof(struct stat));
            if (stat(file_ptr->d_name, fstat_ptr)) {
                fprintf(stderr, "Can't get info by file: '%s'\n", file_ptr->d_name);
                continue;
            }
            /* skip non simple files */
            if ((fstat_ptr->st_mode & S_IFREG) != S_IFREG) {
                continue;
            }
        }

        if (file_ptr->d_ino) {
            /* for Unix-like OSes */
            printf("%10d\t%s\n", (int) file_ptr->d_ino, file_ptr->d_name);
        } else {
            /* for Windows */
            printf("%s\n", file_ptr->d_name);
        }
    }

    return EXIT_SUCCESS;
}


void show_help() {
    printf("Show simple files from directory (current by default).\n");
    printf("Usage:\t./lsimple [-h] [-a] [-f PATH]\n\n");
    printf("\t-h  —  showing this help.\n");
    printf("\t-a  —  showing all files instead of only simple files.\n");
    printf("\t-f  —  allow to specify manual PATH.\n");
}
