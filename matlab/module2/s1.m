x = 38:44;
m = [2 4 4 7 6 4 3];
h = [0.067 0.133 0.133 0.233 0.2 0.133 0.1];
ax1 = subplot(2, 1, 1);
bar(ax1, x, m);
ax2 = subplot(2, 1, 2);
plot(ax2, x, h);
clear x m h ax1 ax2;
fl = 'xrayl.txt';
d = load(fl);
d = sort(d(:));
fprintf('Average = %14.7f\n', m2_s1_mean(d))
d
plot(d)
fprintf('File %s\n', fl)
fprintf('Sample size = %d\n', length(d))
fprintf('min = %14.7f\n', d(1))
fprintf('max = %14.7f\n', d(length(d)))
