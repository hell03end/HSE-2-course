function [ res ] = Var( arr )
	s = 0;
	m = mean(arr);
	for i = 1:length(arr)
	    s = s + (arr(i)-m)^2;
	end
	res = s/(length(arr)-1);
end
