x = 0:1000;
y = log(x);
% figure
ax = subplot(2,2,1)
semilogx(ax, x, y)

x = 0:0.1:10;
y = exp(x);
% figure
ax = subplot(2,2,2)
semilogy(ax, x, y)

n = 100;
k = 1:n;
a = k.^(-2);
s = cumsum(a);
l = pi^2/6;
s(end)

% figure
subplot(2,2,3)
plot(k, s, 'b', [0 n], [l l], 'r');
grid;
xlabel('n');
ylabel('\Sigma_{k=1}^n {1}/{k^2}');

relerr = abs(s-l)/l;

% figure
subplot(2,2,4)
semilogy(relerr) 
xlabel('n'); 
ylabel('Relative error'); 
grid

k = 2:100; 
a = 1-1.*k.^(-2); 
p = cumprod(a);
p(end) 
figure
subplot(2,1,1)
plot(k, p); 
xlabel('n'); 
ylabel('\Pi_{k=1}^n (1-1/{k^2})'); 
grid;
relerr = abs(.5-p)./.5;
% figure
subplot(2,1,2) 
semilogy(k, relerr); 
xlabel('n'); 
ylabel('Relative error'); 
grid;

figure
n = 200;
m = 1:n; 
mf = cumprod(m);
semilogy(mf)
grid
sum(isinf(mf))
sum(isfinite(mf))
