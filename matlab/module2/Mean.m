function [ res ] = Mean( arr )
	sum = 0;
	for i = 1:length(arr)
	    sum = sum + arr(i);
	end
	res = sum/length(arr);
end
