x = randn(1000, 3);
hist(x);

x = randn(1000, 1);
nbins = 50;
hist(x, nbins);

x = randn(1000, 1);
subplot(3, 1, 1);
xbins1 = -4:4;
hist(x, xbins1)
subplot(3,1,2);
xbins2 = -2:2;
hist(x, xbins2)
subplot(3,1,3);
xbins3 = [-4 -2.5 0 0.5 1 3];
hist(x, xbins3)

x = randn(1000, 1);
ax1 = subplot(2, 1, 1);
hist(ax1, x, 50)
ax2 = subplot(2, 1, 2);
xbins = [-3, 0, 3];
hist(ax2, x, xbins)

x = randn(1000, 1);
[counts, centers] = hist(x);
bar(centers, counts)

fl = 'xrayl.txt';
d = load(fl);
d = sort(d(:));
fprintf('Average = %14.7f (%14.7f)\n', m2_s1_mean(d), mean(d))
fprintf('Dispercion = %14.7f (%14.7f)\n', m2_s1_var(d), var(d))
fprintf('Sqrd diff = %14.7f (%14.7f)\n', m2_s1_var(d)^(1/2), std(d))
fprintf('Variation = %14.7f (%14.7f)\n', sem2_2_std_mean(d), std(d)/mean(d))
