function [ res ] = m2_std_mean( arr )

m = mean(arr);
if m == 0
    print('Error!\n')
    res = 0;
else
    res = std(arr)/m;
end

end

