x = linspace(0, 4*pi, 50);
y = sin(x);
figure
stairs(y)

x = linspace(0, 4*pi, 50);
y = [0.5*cos(x), 2*cos(x)];
figure
% hold on
stairs(y) %plot function with stairs

x = linspace(0, 4*pi, 20);
figure
stairs(y, '-.or')

x = linspace(0, 4*pi, 40);
figure
s(1) = subplot(2,1,1);
s(2) = subplot(2,1,2);
y1 = 5*sin(x);
y2 = sin(5*x);
stairs(s(1), x, y1)
stairs(s(2), x, y2)

fl = 'xrayl.txt';
d = load(fl);
d = sort(d(:));
A = m2_s3_moments(d, 1)
fprintf('Несмещенная ассиметрия = %14.7f (%14.7f)\n', 0, skewness(d))
fprintf('Несмещенный эксцесс = %14.7f (%14.7f)\n', 0, kurtosis(d))
fprintf('Median = %14.7f (%14.7f)\n', 0, median(d))
fprintf('Размах выборки = %14.7f (%14.7f)\n', 0, range(d))

fprintf('Average = %14.7f (%14.7f)\n', m2_s1_mean(d), mean(d))
fprintf('Dispercion = %14.7f (%14.7f)\n', m2_s1_var(d), var(d))
fprintf('Sqrd diff = %14.7f (%14.7f)\n', m2_s1_var(d)^(1/2), std(d))
fprintf('Variation = %14.7f (%14.7f)\n', sem2_2_std_mean(d), std(d)/mean(d))

fl = 'pos4.txt';
X = load(fl);
X = sort(X(:));

m = mean(X)
S2 = var(X)
S = std(X)
V = std(X)/mean(X)
E = kurtosis(X)
M = median(X)
Sq = skewness(X)
