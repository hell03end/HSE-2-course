A = [
0 2 4 1 6
1 4 5 7 2
5 6 7 3 3
2 3 4 5 1
4 6 7 8 2
];

H = hilb(10);

fprintf('Spectral norm: %.3f\n', norm(A))
fprintf('Spectral norm (hilb): %.3f\n', norm(H))
fprintf('Inf norm: %.3f\n', norm(A, Inf))
fprintf('Inf norm (hilb): %.3f\n', norm(H, Inf))
fprintf('Frobenius norm: %.3f\n', norm(A, 'fro'))
fprintf('Frobenius norm (hilb): %.3f\n', norm(H, 'fro'))
fprintf('Conditional number: %.3f\n', cond(A))
fprintf('Conditional number (hilb): %.3f\n', cond(H))
fprintf('Conditional number (inf): %.3f\n', cond(A, Inf))
fprintf('Conditional number (hilb, inf): %.3f\n', cond(H, Inf))
fprintf('Conditional number (frobenius): %.3f\n', cond(A, 'fro'))
fprintf('Conditional number (hilb, frobenius): %.3f\n', cond(H, 'fro'))

fprintf('Det: %.3f\n', det(A))
fprintf('Det (hilb): %.3f\n', det(H))

b = [1;2;3;4;5];

fprintf('Answers:\n')
Solve(A, b)