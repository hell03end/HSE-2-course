function [ res ] = m2_moments( arr, l )
	n = length(arr);
	m = mean(arr);
	res = 0;
	for i = 1:n
		res = res + (arr(i) - m)^l;
	end
	res = res/n;
end
