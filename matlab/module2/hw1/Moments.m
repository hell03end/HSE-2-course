function [ al, bl ] = m2_moments( arr, l )
	n = length(arr);
	al = 0; %for starting coef-s
	for i = 1:n
		al = al + arr(i)^l;
	end
	al = al/n;

	m = mean(arr);
	bl = 0; %for central coef-s
	for i = 1:n
		bl = bl + (arr(i) - m)^l;
	end
	bl = bl/n;
end
