function [ res ] = Var( arr )
	res = max(arr) - min(arr);
end
