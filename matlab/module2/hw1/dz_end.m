function [ res ] = interval( arg )
    res = [];
    m = 0;
    dmin = min(arg);
    K = round(length(arg)^(1/2));
    R = range(arg);
    d = R/K;
    for i = 1:K
        for j = 1:length(arg)
            if arg(j) >= dmin
                if arg(j) < dmin+d
                    m = m + 1;
                end
            end
        end
        dmin = dmin + d;
        res = [res m];
        m = 0;
    end
end

