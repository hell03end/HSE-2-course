function [ res ] = Excess( arr, func )
	res = func(arr, 4)/(func(arr, 2))^2 - 3;
end
