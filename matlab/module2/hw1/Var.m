function [ res ] = Var( arr )
	res = 0;
	m = mean(arr); %get average value
	for i = 1:length(arr)
	    res = res + (arr(i)-m)^2;
	end
	res = res/(length(arr)-1);
end
