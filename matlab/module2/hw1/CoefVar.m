function [ res ] = CoefVar( arr )
	res = var(arr)^(1/2)/mean(arr);
end
