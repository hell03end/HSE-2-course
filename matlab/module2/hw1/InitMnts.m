function [ res ] = m2_moments( arr, l )
	n = length(arr);
	res = 0;
	for i = 1:n
		res = res + arr(i)^l;
	end
	res = res/n;
end
