function [ res ] = StDev( arr )
	res = var(arr)^(1/2);
end
