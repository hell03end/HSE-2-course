flpath = input('Enter file to open: ', 's');
data = load(flpath);
data = sort(data(:)); %sort all data, converting it to one column

X = mean(data);
S2 = var(data); %dispersion
S = std(data);
V = S./X;
Sk = skewness(data);
E = kurtosis(data);
M = median(data);

fprintf('\nAverage = %.7f (%.7f)\n', X, Avrg(data))
fprintf('Sample variance = %.7f (%.7f)\n', S2, Var(data))
fprintf('Standard deviation = %.7f (%.7f)\n', S, Var(data)^(1/2))
fprintf('The coefficient of variation = %.7f (%.7f)\n', V, Var(data)^(1/2)/Avrg(data))
fprintf('Sample initial and central moments of order l:\n')
fprintf('\tinitial moment (l=1) = (%.7f)\n', InitMnts(data, 1))
fprintf('\tinitial moment (l=2) = (%.7f)\n', InitMnts(data, 2))
fprintf('\tcentral moment (l=1) = %.7f (%.7f)\n', moment(data, 1), abs(CntrMnts(data, 1)))
fprintf('\tcentral moment (l=1) = %.7f (%.7f)\n', moment(data, 2), CntrMnts(data, 2))
fprintf('Estimate asymmetry coefficient = %.7f (%.7f)\n', Sk, AsymCoef(data, @CntrMnts))
fprintf('Assessment of excess = %.7f (%.7f)\n', E, Excess(data, @CntrMnts))
fprintf('Sample mode = %.7f\n', mode(data))
fprintf('Sample median = %.7f\n', M)
fprintf('Range = %.7f\n', range(data))
fprintf('Dispersion = %.7f\n', S2*mean(data)/(mean(data)-1))

k = round(length(data)^(1/2));
R = range(data);

d = [];
for i=1:k
	d(i) = R*(i/k);
end
Ip = [];
for i=1:k-1
	Ip(i) = Prsnt(data, d(i), d(i+1));
end

figure
hist(Ip, k)

% figure
% bar(data)

% figure
% ax = subplot(2,1,1);
% [counts, centers] = hist(data, 20);
% bar(ax, centers, counts)
% xlabel('Values')
% ylabel('Frequency')
% title('Using hist() + bar() function.')
% ax = subplot(2,1,2);
% histogram(ax, data, 20)
% xlabel('Values')
% ylabel('Frequency')
% title('Using histogram() function.')

if V < 0.33 && Sk < 0.5 && E-3 < 0.5 && abs(X-M) < 1 && Prsnt(data,X-S,X+S) <= 0.68 && ...
		Prsnt(data,X-2*S,X+2*S) <= 0.95 && Prsnt(data,X-3*S,X+3*S) <= 1
	fprintf('\nData is normal.\n');
end

fprintf('X +- S = %.7f\n', 100*Prsnt(data,X-S,X+S))
fprintf('X +- 2*S = %.7f\n', 100*Prsnt(data,X-2*S,X+2*S))
fprintf('X +- 3*S = %.7f\n', 100*Prsnt(data,X-3*S,X+3*S))


temp = dz_end(data)

plot(temp)
clear flpath data X S2 S Sk M V E k d Ip R counts centers ax;