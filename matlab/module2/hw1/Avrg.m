function [ res ] = Avrg( arr )
	res = 0;
	for i = 1:length(arr)
	    res = res + arr(i);
	end
	res = res/length(arr);
end
