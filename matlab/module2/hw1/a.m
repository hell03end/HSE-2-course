D=load(input('','s')); D=sort(D(:)); %sort all data, converting it to one column
X=mean(D);S2=var(D);S=std(D);V=S/X;Sk=skewness(D);E=kurtosis(D);M=median(D);
fprintf('Average %.7f(%.7f)\n',X,Avrg(D))
fprintf('Sample variance %.7f(%.7f)\n',S2,Var(D))
fprintf('Standard deviation %.7f(%.7f)\n',S,Var(D)^(1/2))
fprintf('Variation coef %.7f(%.7f)\n',V,Var(D)^(1/2)/Avrg(D))
fprintf('initial moment(l=1) (%.7f)\n',InitMnts(D,1))
fprintf('initial moment(l=2) (%.7f)\n',InitMnts(D,2))
fprintf('central moment(l=1) %.7f(%.7f)\n',moment(D,1),abs(CntrMnts(D,1)))
fprintf('central moment(l=1) %.7f(%.7f)\n',moment(D,2),CntrMnts(D,2))
fprintf('Asymmetry %.7f(%.7f)\n',Sk,AsymCoef(D,@CntrMnts))
fprintf('Assessment of excess %.7f(%.7f)\n',E,Excess(D,@CntrMnts))
fprintf('Sample mode %.7f(%.7f)\n',range(D),Mode(D))
fprintf('Sample median = %.7f\n',M)
subplot(2,1,1);[counts,centers]=hist(D,100);bar(centers,counts);subplot(2,1,2);hist(D,100);
if V<0.33&&Sk<0.5&&E-3<0.5&&abs(X-M)<1&&Prsnt(D,X-S,X+S)<=0.68&&Prsnt(D,X-2*S,X+2*S)<=0.95&&Prsnt(D,X-3*S,X+3*S)<=1
	fprintf('Normal data.\n');
end
