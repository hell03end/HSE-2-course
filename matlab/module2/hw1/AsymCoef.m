function [ res ] = AsymCoef( arr, func )
	res = func(arr, 3)/(func(arr, 2))^(3/2);
end
