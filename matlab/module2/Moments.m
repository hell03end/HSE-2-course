function [ res ] = m2_moments( arr, l )

n = length(arr);
m = mean(arr);
al = 0;
bl = 0;

for i = 1:n
	al = al + arr(i)^l;
end
al = al/n;

for i = 1:n
	bl = bl + (arr(i) - m)^l;
end
bl = bl/n;

res = [al, bl];

end
