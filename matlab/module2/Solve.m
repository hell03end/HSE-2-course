function [ ans ] = Solve( A, b )
	if det(A) ~= 0
		ans = A\b;
	else
		ans = NaN;
end
