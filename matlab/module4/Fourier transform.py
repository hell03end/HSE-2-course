import numpy as np
import matplotlib.pyplot as plt


DIR=r"rsc/"
plt.rcParams['figure.figsize'] = (20,7)
plt.rc('font', family='Arial')
np.random.seed(1)


def plot_ifft(input_signal, ifft_signal, ifft_gibs_signal):
    plt.step(range(1,len(input_signal)+1), input_signal, 'g')
    plt.plot(range(1,len(ifft_signal)+1), ifft_signal.real, 'b--')
    plt.plot(range(1,len(ifft_gibs_signal)+1), ifft_gibs_signal.real, 'r.--')
    plt.legend(["input signal", "discrete ifft signal", "discrete ifft with gibs effect"])
    plt.grid()
    plt.savefig(f"{DIR}ifft.png")
    plt.close()


def plot_fft_afr(fft_afr):
    plt.stem(fft_afr)
    plt.legend(["Amplitude Frequency Response"])
    plt.grid()
    plt.savefig(f"{DIR}fft_afr.png")
    plt.close()


def plot_fft_pfr(fft_pfr):
    plt.stem(fft_pfr)
    plt.legend(["Phase Frequency Response"])
    plt.grid()
    plt.savefig(f"{DIR}fft_pfr.png")
    plt.close()


def plot_fft(fft_signal, fft_gibs_signal):
    fig, ax = plt.subplots(1, 2)
    ax[0].stem(fft_signal.real)
    ax[0].legend(["fft"])
    ax[0].grid()
    ax[1].stem(fft_gibs_signal.real)
    ax[1].legend(["fft with gibs effect"])
    ax[1].grid()
    plt.savefig(f"{DIR}fft.png")
    plt.close()

def plot_ifft2(x, y, step, ifft, bad_ifft):
    plt.plot(x, y, x, ifft.real, 'or', x[::step], bad_ifft.real, '.k')
    plt.legend(["input signal", "discrete ifft signal", 
               "discrete ifft signal with low quality"])
    plt.grid()
    plt.savefig(f"{DIR}ifft2.png")
    plt.close()

def plot_fft2(x, step, fft, bad_fft):
    plt.plot(x, fft.real, x[::step], bad_fft.real)
    plt.legend(["discrete fft", r"ifft $\to$ fft"])
    plt.title("Spectrogram")
    plt.grid()
    plt.savefig(f"{DIR}fft2.png")
    plt.close()


if __name__ == "__main__":
    input_signal = np.array([[0]*8 + [1]*16 + [0]*8]*2).flatten()
    signal_len = len(input_signal)

    fft_signal = np.fft.fft(input_signal)  # discrete fourier transform
    signal_size = fft_signal.size

    fft_gibs_signal = fft_signal.copy()
    fft_gibs_signal[int(signal_len/6-1):int(signal_len/2-1)] = 0  # restrict harmonics

    ifft_signal = np.fft.ifft(fft_signal)  # inverse discrete fourier transform
    ifft_gibs_signal = np.fft.ifft(fft_gibs_signal)  

    fft_afr = abs(fft_signal)
    fft_pfr = np.angle(fft_signal)

    plot_ifft(input_signal, ifft_signal, ifft_gibs_signal)
    plot_fft_afr(fft_afr)
    plot_fft_pfr(fft_pfr)
    plot_fft(fft_signal, fft_gibs_signal)

    # Task 2:
    size=100
    step=int(size/20)
    x = np.linspace(0,size,size, dtype=int)
    y = np.sin(x) + 7*np.cos(x) + np.sin(7*x) + np.random.mtrand.uniform(size=size)  # input signal

    fft = np.fft.fft(y)  
    ifft = np.fft.ifft(fft)
    bad_ifft = np.fft.ifft(fft[::step])
    bad_fft = np.fft.fft(y[::step])

    plot_ifft2(x, y, step, ifft, bad_ifft)
    plot_fft2(x, step, fft, bad_fft)
