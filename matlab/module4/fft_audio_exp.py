import pyaudio
import numpy as np
from time import time
from argparse import ArgumentParser

import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
plt.rcParams['figure.figsize'] = (12,12)

LMODE_PARAMS = {
    'size': 12, 
    'rate': 8000,
    'chunk': 2**10
}


def soundplot(i, stream, path, chunk=2**10, size=12, borders=2**12):
    t1 = time()
    input_signal = np.fromstring(stream.read(chunk), dtype=np.int16)
    fft = np.fft.fft(input_signal)

    fig, ax = plt.subplots(1, 2)
    fig.set_size_inches(size, size)

    ax[0].plot(input_signal)
    ax[0].set_title(i)
    ax[0].grid()
    ax[0].axis([0, len(input_signal), -borders, borders])

    ax[1].plot(abs(fft[:int(len(fft)/2)]))
    ax[1].set_title(i)
    ax[1].grid()
    ax[1].axis([0, int(len(fft)/2), 0, borders*2])

    plt.savefig(path, dpi=50)
    plt.close('all')
    print(f"took {(time() - t1)*1000:02f} ms")


def parse_argv():
    parser = ArgumentParser(description="Setup audio-fft visualisation.")
    parser.add_argument('--path', '-P', type=str, default=r"fft_audio_exp.png",
                        help="Where to save buffer image.")
    parser.add_argument('--size', '-s', type=int, default=16,
                        help="Image quality.")
    parser.add_argument('--rate', '-r', type=int, default=44100,
                        help="Start server in debug mode.")
    parser.add_argument('--borders', '-b', type=int, default=2**12,
                        help="Max amplitude can be processed.")
    parser.add_argument('--chunk', '-C', type=int, default=2**12,
                        help="Amounts of bytes to get each time.")
    parser.add_argument('--mono', '-m', type=bool, default=True,
                        help="Toggle between mono and stereo recording.")
    parser.add_argument('--period', '-p', type=float, default=10,
                        help="Recording time.")
    parser.add_argument('--light', '--light-mode', type=bool, default=False,
                        help="Start in light mode for better performance.")
    return parser.parse_args()


if __name__=="__main__":
    args = parse_argv()
    lmode = args.light
    size = args.size if not lmode else LMODE_PARAMS['size'] 
    rate = args.rate if not lmode else LMODE_PARAMS['rate'] 
    chunk = args.chunk if not lmode else LMODE_PARAMS['chunk'] 

    p = pyaudio.PyAudio()
    stream = p.open(format=pyaudio.paInt16,
                    channels=1 if args.mono or lmode else 2,
                    rate=rate,
                    input=True, 
                    frames_per_buffer=chunk)
    for i in range(int(args.period*rate/chunk)):
        soundplot(i, stream, args.path, chunk, size, args.borders)
    stream.stop_stream()
    stream.close()
    p.terminate()
