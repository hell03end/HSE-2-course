% Dmitriy Pchelkin
% BIV-151
% variant: 10

diary diary_task_102 %write all actions

%task a:
a = 3.56
b = exp(0.316)
(log(b + 1.4)/log(a))^(-3/4) %ans

%task b:
a = 2
b = 2.1649*(10^-2)
(log(b + 1.4)/log(a))^(-3/4)

diary off
