% seminar 5:

x = -2*pi:pi/10:2*pi;

y1 = sin(x);
subplot(2, 2, 1)
plot(x, y1)

y2 = cos(x);
subplot(2, 2, 2)
plot(x, y2)

y3 = exp(-x);
subplot(2, 2, 3)
plot(x, y3)

y4 = x.^3;
subplot(2, 2, 4)
plot(x, y4)
