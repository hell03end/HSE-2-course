% Dmitriy Pchelkin
% BIV-151
% variant: 10

diary diary_task_401 %write all actions

disp('2x^2 - 2x - 8 = y');
a = 2;
b = -2;
c = -8;
disp('a = 2, b = -2, c = -8');

x = -10:0.1:10;
y = a*x.^2 + b*x + c;

disp('Roots is:')
[root1, root2] = SqrEquatSolve(a, b, c);
if root1
	root1
endif
if root2
	root2
endif

%plot
figure %creates new window to plot
plot(x, y); %plot of f(x)
xlabel('x')
ylabel('f(x)')
legend('2x^2 - 2x - 8 = y', 0)
title('Plot of 2x^2 - 2x - 8 = y')
grid on %show grid
%endplot

diary off
%delete local variables:
clear a b c x y root1 root2;
