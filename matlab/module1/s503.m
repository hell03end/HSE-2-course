% seminar 5:

%newstats
function [avrg, med] = sem52(vec)
	avrg = avrg(vec, length(vec));
	med = median(vec, length(vec));

function ans = avrg(v, n)
	%ans = sum(v)/n;
	sum = 0;
	for t=1:length(v)
		sum += v(t);
	end
	ans = sum / n;

function m = median (v, n)
	w = sort(v);
	if rem(n, 2) == 1 %n is even
		m = w((n+1)/2);
	else %n is odd
		m = (w(n/2) + w(n/2+1))/2;
	end
