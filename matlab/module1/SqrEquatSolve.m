% Dmitriy Pchelkin
% BIV-151
% variant: 10

function [x1, x2] = SqrEquatSolve (a, b, c)

	if nargin == 3
		if a == 0 && b == 0 && c == 0
			disp('infinite number of roots.');
			x1 = x2 = NaN;
			return;
		elseif a == 0 && b == 0
			disp('no roots.');
			x1 = x2 = NaN;
			return;
		elseif a == 0
			x1 = -c./b;
			x2 = NaN;
			return;
		elseif b == 0
			x1 = sqrt(-c./a);
			x2 = NaN;
		end

		D = b^2 - 4*a*c;
		if D == 0
			x1 = -b/(2*a);
			x2 = NaN;
			return;
		else
			x1 = (-b + sqrt(D))/(2*a);
			x2 = (-b - sqrt(D))/(2*a);
			return;
		end
	else
		error('Wrong number of args!');
	end
end