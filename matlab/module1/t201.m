% Dmitriy Pchelkin
% BIV-151
% variant: 10

diary diary_task_201 %write all actions

%f(x) on [a;b]
a = -0.1
b = 0.9
h = 0.1 %step

x = a:h:b; %linspace(_a:_h:_b);
y = sqrt(2 + 3*x).*log(1 + 3*x.^2)

figure %creates new window to plot
plot(x,y) %plot of f(x)
xlabel('x')
ylabel('f(x)')
legend('sqrt(2 + 3x)*ln(1 + 3x^2)', 0)
title('Plot of sqrt(2 + 3x)*ln(1 + 3x^2)')
grid on %show grid
axis equal

diary off
