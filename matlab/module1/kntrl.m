x = -10:0.01:10;
a = 2;
y = 7*a^3./(x.^2+4*a);

subplot(2, 3, 1);
%figure %creates new window to plot
plot(x,y) %plot of f(x)
xlabel('x')
ylabel('f(x)')
grid on %show grid
axis equal

t = 0:0.01:4*pi;
a = pi/10;
x = (3*a.*t)./(1 + t.^3);
y = (3*a.*t.^2)./(1 + t.^3);

subplot(2, 3, 2);
%figure %creates new window to plot
plot(x,y) %plot of f(x)
xlabel('x')
ylabel('f(x)')
grid on %show grid
%axis equal

t = 0:0.01:2*pi;
a = pi;
x = a.*(cos(t) + t.*sin(t));
y = a.*(sin(t) - t.*cos(t));

subplot(2, 3, 3);
%figure %creates new window to plot
plot(x,y) %plot of f(x)
xlabel('x')
ylabel('f(x)')
grid on %show grid
axis equal

t = -10:0.01:10;
a = 2;
b = 4;
x = ((a^2 - b^2).*cos(t).^3)./a;
y = ((a^2 - b^2).*sin(t).^3)./b;

subplot(2, 3, 4);
%figure %creates new window to plot
plot(x,y) %plot of f(x)
xlabel('x')
ylabel('f(x)')
grid on %show grid
axis equal

t = -10:0.01:10;
a = 4;
b = 2;
x = a.*t - b.*sin(t);
y = a - b.*cos(t);

subplot(2, 3, 5);
hold on;
%figure %creates new window to plot
plot(x,y) %plot of f(x)
xlabel('x')
ylabel('f(x)')
grid on %show grid
axis equal

t = -10:0.01:10;
a = 2;
b = 4;
x = a.*t - b.*sin(t);
y = a - b.*cos(t);

subplot(2, 3, 5);
%figure %creates new window to plot
plot(x,y) %plot of f(x)
hold off;
xlabel('x')
ylabel('f(x)')
grid on %show grid
axis equal

t = -10:0.01:10;
a = 2;
x = a.*(log(cot(0.5.*t)) - cos(t));
y = a.*sin(t);

subplot(2, 3, 6);
%figure %creates new window to plot
plot(x,y) %plot of f(x)
xlabel('x')
ylabel('f(x)')
grid on %show grid
axis equal
