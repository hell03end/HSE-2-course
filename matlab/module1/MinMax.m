function [Min, Max] = MinMax(matr)
	len = size (matr);
	Min = min(matr, len(1)*len(2));
	Max = max(matr, len(1)*len(2));

	function ans = min(m, n)
		ans = m(1);
		for t = 2:n
			if m(t) < ans
				ans = m(t);
			end
		end
	end

	function ans = max(m, n)
		ans = m(1);
		for t = 2:n
			if m(t) > ans
				ans = m(t);
			end
		end
	end
end
