% Dmitriy Pchelkin
% BIV-151
% variant: 10

diary diary_task_101 %write all actions

((10/3 + 2.5)/(2.5 - 4/3))*	...
((4.6 - 7/3)/(4.6 + 7/3))/	...
(0.05/(1/7 - 0.125) + 5.7)	%ans

diary off
