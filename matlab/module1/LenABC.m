% Dmitriy Pchelkin
% BIV-151
% variant: 10

function [lenAB, lenAC, lenBC] = LenABC (A, B, C)

	AB = B - A;
	AC = C - A;
	BC = C - B;

	lenAB = sqrt(AB*AB');
	lenAC = sqrt(AC*AC');
	lenBC = sqrt(BC*BC');

end