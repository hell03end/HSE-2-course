D = eye(100, 100);
C = logical(D);
BI2 = [1, 1, 1, 1; 1, 0, 0, 0; 1, 1, 1, 1; 1, 0, 0, 1; 1, 0, 0, 1; 1, 1, 1, 1];
BI = logical(BI2);
C(10:15, 2:5) = BI;
subplot(2,2,1); imshow(C)
subplot(2,2,2); imshow(flipdim(C, 1))
subplot(2,2,3); imshow(C)

% subplot(2,2,4); imshow()
