syms x
y = sin(x)/x;
limit(y)
clear

syms x
y = (2*x^3 + 3*x^2 + 1)/(x^2 - 2*x + 3);
vpa(limit(y, 2), 5)
clear

syms x
y = (1 + 1/x)^x;
vpa(limit(y, inf), 5)
clear

syms x
y = 1/x;
figure
ezplot(y);
grid on
limit(y, x, 0, 'left')
limit(y, x, 0, 'right')
clear

x = dsolve('Dx = -0.5*x', 'x(0) = 10')

[x1, x2] = dsolve('Dx1 = -0.5*x2', 'Dx2 = 3*x1', 'x1(0) = 0', 'x2(0) = 1')
ezplot(x1, [0,13]), hold on
ezplot(x2, [0,13]), hold off
grid on
clear

%TASK1
syms x
f = (x - 3)/(x^2 - 4*x + 3);
limit(f, 3)
clear

%TASK2
syms x
f = x^2 + 3*x + 1;
diff(f, x)
clear

%TASK3
syms x
f = x^2 - cos(x);
diff(f, x)
diff(diff(f, x), x)
clear

%TASK4
x = dsolve('Dx = t*cos(t) + x/t', 'x(1) = 0')
ezplot(x), grid on
x = dsolve('(1 + e^t)*x*Dx = e^t', 'x(0) = 1')
figure
ezplot(x), grid on % don't show this plot somewhy.
x = dsolve('Dx - x*tan(t) = 1/cos(t)', 'x(0) = 0')
figure
ezplot(x), grid on
clear

%TASK5


%TASK6
[x1, x2] = dsolve('Dx1 = 2*x1 - 1', 'Dx2 = 3*x1 + 2*x2 + 1', 'x1(0) = 1', 'x2(0) = 1')
figure
ezplot(x1), hold on
ezplot(x2), hold off
grid on
clear
