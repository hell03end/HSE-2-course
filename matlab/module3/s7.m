I = imread('bf3.jpg');
img_info = imfinfo('bf3.jpg');
fprintf('Original image sizes:\n\tWidth: %d\n\tHeight: %d\n', img_info.Height, img_info.Width)
subplot(2,2,1); imshow(I)
subplot(2,2,2); imshow(s7_f1('bf3.jpg', 2, 'picture_m1.jpg'))
subplot(2,2,3); imshow(I)
subplot(2,2,4); imshow(s7_f1('bf3.jpg', [200,100], 'picture_m3.jpg'))

% histograms
s7_f2('bf3.jpg');

%substraction
I_background = imopen(I, strel('disk',10));
figure
subplot(1,2,1); imshow(I_background)
I_sub = imsubtract(I, I_background);
subplot(1,2,2); imshow(I_sub)
