function [  ] = kr2_f( Img )
    filter_sobel = fspecial('sobel'); % создание фильтра sobel
    filter_prewitt = fspecial('prewitt'); % создание фильтра prewitt
    % применение фильтров
    Img_sobel = imfilter(Img, filter_sobel, 'replicate');
    Img_prewitt = imfilter(Img, filter_prewitt, 'replicate');
    % печать изображений
    subplot(1,3,1); imshow(Img)
    subplot(1,3,2); imshow(Img_sobel)
    subplot(1,3,3); imshow(Img_prewitt)

    % сравнение изображений 
    fprintf('Compared images sobel:\n\tCorr2: %.4f\n\tMean2 (difference): %.4f\n', ...
            corr2(Img(:), Img_sobel(:)), ...
            (mean2(Img(:)) - mean2(Img_sobel(:))))
    fprintf('Compared images prewitt:\n\tCorr2: %.4f\n\tMean2 (difference): %.4f\n', ...
            corr2(Img(:), Img_prewitt(:)), ...
            (mean2(Img(:)) - mean2(Img_prewitt(:))))
end