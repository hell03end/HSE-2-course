function [  ] = s7_f2( image_name, coeff, file_name )
    Img = imread(image_name);
    figure
    for i = 1:3
        subplot(1,3,i); imhist(Img(:,:,i))
    end
end
