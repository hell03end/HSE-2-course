I = imread('logo.jpg');

filter_image = @(Img, imgfilter) (imfilter(Img, imgfilter, 'replicate'));

special = fspecial('laplacian', .5);
I_filtered = filter_image(I, special);

subplot(1,2,1); imshow(I)
subplot(1,2,2); imshow(I_filtered)

figure
x = -4:.3:4;
subplot(1,3,1); fplot('sin(x)', [-4,4]); grid on
subplot(1,3,2); ezplot('x^2'); grid on
syms t
subplot(1,3,3); ezplot3(sin(t), cos(t), t, [0,6*pi])
