I = imread('bf1.jpg');
subplot(2,4,1); imshow(I)

I_croped = imcrop(I, [10 10 100 100]);
subplot(2,4,2); imshow(I_croped)

I_rotate = imrotate(I, 35, 'bicubic');
subplot(2,4,3); imshow(I_rotate)

impixel(I, 20,20)
mean_I = mean2(I)
corr2(I(:),I(:))

I_adjust = imadjust(I, [0.2 0.5], [0.8 0.2]);
subplot(2,4,4); imshow(I_adjust)

special = fspecial('laplacian', .5);
I_filtered = imfilter(I, special, 'replicate');
subplot(2,4,5); imshow(I_filtered)

special = fspecial('average', 5);
I_filtered = imfilter(I, special, 'replicate');
subplot(2,4,6); imshow(I_filtered)

corr2(I(:), I_filtered(:))
