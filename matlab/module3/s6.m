I3 = imread('bf3.jpg');
Info = imfinfo('bf3.jpg')

s6_f1('bf3.jpg', 1), figure

level = graythresh(I3);
bw = im2bw(I3,level);
subplot(1,2,1), imshow(bw)
[labeled, num_objects] = bwlabel(bw,4);
title(num_objects)

I3_inv = imadjust(I3, [0 1], [1 0]);
level_inv = graythresh(I3_inv);
bw_inv = im2bw(I3_inv,level_inv);
subplot(1,2,2), imshow(bw_inv)
[labeled_inv, num_objects_inv] = bwlabel(bw_inv,4);
title(num_objects_inv)

% figure, imshow(I3_inv), title('Inversion')
