fprintf('Task 1')
x = -3; b = 2;
a = @(x, b) (b*(cos(x)^3 + exp(b + 3))); % анонимная функция
a(x, b)
d = b*(cos(x)^3 + exp(b + 3)) % результат непосредственных вычислений

fprintf('Task 2')
x = -3:.7:3;
y = 3*abs(x - 1).^2; % значения функции 
fplot('3*abs(x - 1).^2', [-3,3]); grid on % график функции

fprintf('Task 3')
A = [2,1,1; 1,3,2; -2,4,-1];
B = [1,2,-2; 2,-1,0; 4,2,3];
inv(A) + 3*B

fprintf('Task 4')
figure
x = -4:.3:4;
fplot('sin(x.^2) - cos(x)', [-4,4], '-^'); hold on % график первой функции
fplot('x.^2 - 3', [-4,4], ':'); hold off % график второй функции
grid on; legend('f=sin(x^2) - cos(x)','y=x^2 - 3')
xlabel('x'); ylabel('f, y'); title('Grafiki')

fprintf('Task 5')
syms x
vpa(solve(abs(x + 1) + abs(x + 2) - 2, x), 5)

fprintf('Task 6')
A = [3, -8, 2, 2]; % коэффициенты полинома 3й степени
x = roots(A)

fprintf('Task 7')
A = [6,-1,-1; -1,6,-1; -1,-1,6]; % матрица коэффициентов
b = [11.33; 32; 42]; % свободный вектор
x = A\b

fprintf('Task 8')
syms x y
[x,y] = solve('x^3 - 2*y = 1', '3*x + 3*y^2 = 2'); % решение системы
vpa(x, 5)
vpa(y, 5)

fprintf('Task 9')
[x,y] = meshgrid(-3:.4:3, -3:.4:3); % получаем "сетку" из координат
figure('Color', 'w'); grid on
S = mesh(x, y, 3*x.^2 - sin(y).^2); % график (поверхность) функции
xlabel('x'); ylabel('y'); zlabel('z')

fprintf('Task 10')
syms x y
int(3*x^3, x) % нахождение интеграла
int(int(x^2 + 2*y*sin(x), x), y) % двойной интеграл

fprintf('Task 11')
syms x
limit((2*x^2 + 3*x + 4)/(x^3 + 2*x^2 + 3), 1)
limit((2^x + 1)/(x^2), inf)

fprintf('Task 12')
syms x
diff(2*x^3 + 1)
diff(exp(3*x) + x*x^(1/2))
diff(exp(3*x) + x*x^(1/2), 3) % производная 3-го порядка

fprintf('Task 13')
x = dsolve('Dx = 2*x + 3*t^2', 'x(0) = 1')
figure
ezplot(x); grid on
