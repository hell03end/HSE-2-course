%SYSTEMS OF EQUATIONS

%TASK1
syms x1 x2 x3 x4;
% initiate variables
A = [ 2 1 0 1;
	  1 -3 2 4;
      -5 0 -1 -7;
	  1 -6 2 6 ];
B = [ 8; 9; -5; 0 ];
% checking determenant is > 0
if det(A) == 0
	fprintf('Det == 0')
end
% matrix method
X = A^(-1)*B
% solve method
[x1, x2, x3, x4] = solve('2*x1 + x2 + x4 = 8', 'x1 - 3*x2 + 2*x3 + 4*x4 = 9', '-5*x1 - x3 - 7*x4 = -5', 'x1 - 6*x2 + 2*x3 + 6*x4 = 0');
vpa(x1, 5), vpa(x2, 5), vpa(x3, 5), vpa(x4, 5)
% Krammer
A1 = A; A1(:,1) = B; x1 = det(A1)/det(A)
A2 = A; A2(:,2) = B; x2 = det(A2)/det(A)
A3 = A; A3(:,3) = B; x3 = det(A3)/det(A)
A4 = A; A4(:,4) = B; x4 = det(A4)/det(A)
clear

%TASK2
syms x y
A = [2 -1; 3 2];
B = [4; 3];
% matrix method
X = A^(-1)*B
% solve method
[x1, y1] = solve('2*x - y = 4', '3*x + 2*y = 3');
vpa(x, 5), vpa(y, 5)
% Krammer
A1 = A; A1(:,1) = B; x1 = det(A1)/det(A)
A2 = A; A2(:,2) = B; x2 = det(A2)/det(A)
% graphic method
% fplot('2*x - y - 4', [-1,1,-1,1]), hold on
ezplot(2*x - y - 4), hold on
ezplot(3*x + 2*y - 3), hold off
grid on
xlim([x1-0.1,x1+0.1]) %, ylim([vpa(y1-1,5),vpa(y1+1,5)])
clear

%TASK3
syms x y
% graphic method
figure
ezplot(2*x*y - 16), hold on
ezplot(x^2 + y - 8), hold off
grid on, xlim([-7,7]), ylim([-7,7])
% solve
[x, y] = solve('2*x*y = 16', 'x^2 + y = 8');
vpa(x,5), vpa(y,5)
clear

%TASK4
syms x y
% solve
[x, y] = solve('x^3 - 2*y = 1', '3*x + 3*y^2 = 2');
vpa(x,5), vpa(y,5)
clear

%EQUATIONS

%TASK1(5)
syms x
% graphic method
figure
ezplot(abs(x + 5) - abs(2*x - 1) - 3)
grid on
% solve
x = solve('abs(x + 5) - abs(2*x - 1) = 3')
clear

%TASK2(6)
syms x
% graphic method
figure
ezplot('2*x^2-3*x-1')
grid on
% solve
x = solve('2*x^2 - 3*x = 1')
vpa(x(1),5), vpa(x(2),5)
% roots
x = roots([2 -3 -1])
clear

%INTEGRALS

%TASK1(7)
syms x
int(cos(x), x)
clear

%TASK2(8)
syms x
vpa(int(sin(x), x, 1, 3),5)
clear

%TASK3(9)
syms x y
f = 2*x + y^2/2;
int(int(f, x), y)
clear

%TASK4(10)
syms x y z
f = (x^2 - 2)*y + 3*z;
int(int(int(f, x), y), z)
int(int(int(f, x, -1, 1), y, 1, 2), z, 0, 3) 
clear
