I_background = imread('10.jpg');
I_1 = imread('1.png'); 
I_2 = imread('2.jpg'); 
I_3 = imread('3.jpg'); 
I_4 = imread('4.jpg'); 

I_1 = imresize(I_1, 0.5, 'bicubic'); % изменение размера: уменьшение в 2 раза
I_2 = imcrop(I_2, [10 10 100 100]); % обрезка изображения: вырезается кусок от (10,10) до (100,100)
I_3 = imrotate(I_3, 35, 'bicubic'); % поворот изображения на 35 градусов
I_3 = imresize(I_3, [100, 100], 'bicubic'); % изменение размера до 100х100 пикселей
I_4 = imadjust(I_4, [0 1], [1 0]); % инвертирование изображения

% вставка полученных изображений в фон. Т.к. изображение в RGB - имеет 3 измерения, вставляем в каждое отдельно
I_background(10:459, 10:459, :) = I_1;
% Далее аналогично
I_background(470:570, 10:110, :) = I_2;
I_background(10:109, 470:569, :) = I_3;
I_background(600:985, 600:1130, :) = I_4;

I_background = imcrop(I_background, [0 0 1200 1000]); % обрезка изображения

imshow(I_background) % отображение полученного результата

imwrite(I_background, 'result_100.jpg', 'Quality', 100) % сохранение изображения в формате jpg с качеством: 100
imwrite(I_background, 'result_50.jpg', 'Quality', 50) % сохранение изображения в формате jpg с качеством: 50
imwrite(I_background, 'result_1.jpg', 'Quality', 1) % сохранение изображения в формате jpg с качеством: 1
imwrite(I_background, 'result.png') % сохранение изображения в формате png
