function [ Img_m ] = s7_f1( image_name, coeff, file_name )
    Img = imread(image_name);
    Img_m = imresize(Img, coeff, 'bicubic');
    imwrite(Img_m, file_name)
    img_info = imfinfo(file_name);
    fprintf('Changed image sizes:\n\tWidth: %d\n\tHeight: %d\n', img_info.Height, img_info.Width)
end
