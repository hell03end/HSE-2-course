Img = logical(zeros(100,100));
D = logical([0 0 0 0 0 0; 0 0 0 1 1 0; 0 0 1 0 1 0; 
    0 0 1 0 1 0; 0 1 1 1 1 1; 0 1 0 0 0 1]);
M = logical([0 0 0 0 0 0; 0 1 0 0 0 1; 0 1 1 0 1 1;
    0 1 0 1 0 1; 0 1 0 0 0 1; 0 1 0 0 0 1]);
I = logical([0 0 0 0 0 0; 0 1 0 0 0 1; 0 1 0 0 1 1;
    0 1 0 1 0 1; 0 1 1 0 0 1; 0 1 0 0 0 1]);
T = logical([0 0 0 0 0 0; 0 1 1 1 1 1; 0 0 0 1 0 0;
    0 0 0 1 0 0; 0 0 0 1 0 0; 0 0 0 1 0 0]);
R = logical([0 0 0 0 0 0; 0 0 1 1 1 1; 0 0 1 0 0 1;
    0 0 1 1 1 1; 0 0 1 0 0 0; 0 0 1 0 0 0]);
Y = logical([0 0 1 1 1 0; 0 1 0 0 0 1; 0 1 0 0 1 1;
    0 1 0 1 0 1; 0 1 1 0 0 1; 0 1 0 0 0 1]);
P = logical([0 0 0 0 0 0; 0 0 1 1 1 1; 0 0 1 0 0 1;
	0 0 1 0 0 1; 0 0 1 0 0 1; 0 0 1 0 0 1]);
CH = logical([0 0 0 0 0 0; 0 0 1 0 0 1; 0 0 1 0 0 1;
	0 0 1 1 1 1; 0 0 0 0 0 1; 0 0 0 0 0 1]);
E = logical([0 0 1 0 1 0; 0 0 1 1 1 1; 0 0 1 0 0 0; 
	0 0 1 1 1 1; 0 0 1 0 0 0; 0 0 1 1 1 1]);
L = logical([0 0 0 0 0 0; 0 0 0 1 1 1; 0 0 1 0 0 1;
	0 0 1 0 0 1; 0 0 1 0 0 1; 0 1 1 0 0 1]);
K = logical([0 0 0 0 0 0; 0 0 1 0 0 1; 0 0 1 0 1 0;
	0 0 1 1 0 0; 0 0 1 0 1 0; 0 0 1 0 0 1]);
N = logical([0 0 0 0 0 0; 0 0 1 0 0 1; 0 0 1 0 0 1;
	0 0 1 1 1 1; 0 0 1 0 0 1; 0 0 1 0 0 1]);

Img(10:15, 10:15) = D;
Img(10:15, 20:25) = M;
Img(10:15, 30:35) = I;
Img(10:15, 40:45) = T;
Img(10:15, 50:55) = R;
Img(10:15, 60:65) = I;
Img(10:15, 70:75) = Y;

Img(20:25, 10:15) = P;
Img(20:25, 20:25) = CH;
Img(20:25, 30:35) = E;
Img(20:25, 40:45) = L;
Img(20:25, 50:55) = K;
Img(20:25, 60:65) = I;
Img(20:25, 70:75) = N;

imshow(Img)
