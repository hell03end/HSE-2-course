function [  ] = s6_f1( file_name, quality )
% different quality, write to files, compare with origin, show pictures

    Img = imread(file_name);
    imwrite(Img, 'picture_q.jpg', 'Quality', quality)
    Img_q = imread('picture_q.jpg');

    corr_q = corr2(Img(:), Img_q(:));
    mean_q = mean2(Img(:)) - mean2(Img_q(:))

    subplot(1,2,1); imshow(Img), title('Original')
    subplot(1,2,2); imshow(Img_q), title(corr_q)

    Info = imfinfo(file_name);
    Info_q = imfinfo('picture_q.jpg');

end

